package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.Database;
import com.hbm.dao.HotelDao;
import com.hbm.dao.HotelFacilityDao;
import com.hbm.dao.HotelWiseFacilityDao;
import com.hbm.dao.RoomWiseFacilityDao;

public class HotelWiseFacilityServlet extends HttpServlet{
	  HotelWiseFacilityDao hotelWiseFacilityDao;
	  HotelDao hotelDao;
	  HotelFacilityDao hotelFacilityDao;
	  
	  public void init(ServletConfig config) throws ServletException{
		   super.init(config);
		   Database db = (Database)getServletContext().getAttribute("db");
		   hotelWiseFacilityDao = new HotelWiseFacilityDao(db);
		   hotelDao = new HotelDao(db);
		   hotelFacilityDao = new HotelFacilityDao(db);
	  }
	  
	  protected void doGet(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		  	String id = request.getParameter("id");
		  	String action = request.getParameter("action");
		  	
		  	if(id == null && action == null) {
		  		  List<HotelWiseFacilityData> ul = hotelWiseFacilityDao.getAllHotelWiseFacility();
		  		  request.setAttribute("data", ul);
		  		  request.getRequestDispatcher("/hotelWiseFacility.jsp").forward(request, response);
		  	}
		  	else if(action.equals("createHotelWiseFacility")){
		  		 List<HotelData> ul1 = hotelDao.getHotelInfo();
		  		 request.setAttribute("data1", ul1);
		  		 List<HotelFacilityData> ul2 = hotelFacilityDao.getAllHotelFacility();
		  		 request.setAttribute("data2", ul2);
		  		 request.getRequestDispatcher("/hotelWiseFacility.jsp").forward(request, response);
		  	}
		  	else if(action.equals("delete")){
		  		int Id = Integer.parseInt(id);
		  		hotelWiseFacilityDao.deleteHotelWiseFacility(Id);
		  		response.sendRedirect("/HBM/hotelWiseFacility");
		  	}
		  	else if(action.equals("edit")) {
		  		 int Id = Integer.parseInt(id);
		  		 HotelWiseFacilityData hotelWiseFacilityData = hotelWiseFacilityDao.getHotelWiseFacility(Id);
		  		 request.setAttribute("data", hotelWiseFacilityData); 
		  		 List<HotelData> ul1 = hotelDao.getHotelInfo();
		  		 request.setAttribute("data1", ul1);
		  		 List<HotelFacilityData> ul2 = hotelFacilityDao.getAllHotelFacility();
		  		 request.setAttribute("data2", ul2);
		  		 request.getRequestDispatcher("/hotelWiseFacility.jsp").forward(request, response);
		  	}
	  }

	  
	  protected void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		    String HotelId = request.getParameter("hotelId");
		  	String HotelFacilityId = request.getParameter("hotelFacilityId");
		  	String action = request.getParameter("action");
		  	String id = request.getParameter("id");
		  	
		  	if(action.equals("create")) {
		  		 int hotelId = Integer.parseInt(HotelId);
		  		 int hotelFacilityId = Integer.parseInt(HotelFacilityId);
		  		 hotelWiseFacilityDao.createHotelWiseFacility(hotelId,hotelFacilityId);
		  		 response.sendRedirect("/HBM/hotelWiseFacility");
		  	}
		  	else if(action.equals("edit")) {
		  		 int Id = Integer.parseInt(id);
		  		 int hotelId = Integer.parseInt(HotelId);
		  		 int hotelFacilityId = Integer.parseInt(HotelFacilityId);
		  		 hotelWiseFacilityDao.updateHotelWiseFacility(Id,hotelId,hotelFacilityId);
		  		 response.sendRedirect("/HBM/hotelWiseFacility");
		  	}
	  }
}
