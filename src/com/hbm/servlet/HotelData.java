package com.hbm.servlet;

import java.util.List;

public class HotelData {
	 private int id,starRating,cityId,countryId;
	 private String hotelName,email,address,phoneNo,checkIn,checkOut,country,city,image1,image2,image3,image4;

	 public void setId(int id) {
		 this.id = id;
	 }
	 public int getId() {
		 return id;
	 }
	 
	 public void setCity(String city) {
		 this.city = city;
	 }
	 public String getCity() {
		 return city;
	 }
	 public void setCountry(String country) {
		 this.country = country;
	 }
	 public String getCountry() {
		 return country;
	 }
	 
	 public void setStarRating(int starRating) {
		 this.starRating = starRating;
	 }
	 public int getStarRating() {
		 return starRating;
	 }
	 
	 public void setEmail(String email) {
		 this.email = email;
	 }
	 public String getEmail() {
		 return email;
	 }
	 
	 public void setPhoneNo(String phoneNo) {
		 this.phoneNo = phoneNo;
	 }
	 public String getPhoneNo() {
		 return phoneNo;
	 }
	 
	 public void setAddress(String address) {
		 this.address = address;
	 }
	 public String getAddress() {
		 return address;
	 }
	 
	 public void setHotelName(String hotelName) {
		 this.hotelName = hotelName;
	 }
	 public String getHotelName() {
		 return hotelName;
	 }
	 
	 public void setCheckIn(String checkIn) {
		 this.checkIn = checkIn;
	 }
	 public String getCheckIn() {
		 return checkIn;
	 }
	 
	 public void setCheckOut(String checkOut) {
		 this.checkOut = checkOut;
	 }
	 public String getCheckOut() {
		 return checkOut;
	 }
	 
	 public void setCityId(int cityId) {
		 this.cityId = cityId;
	 }
	 public int getCityId() {
		 return cityId;
	 }
	 public void setCountryId(int countryId) {
		 this.countryId = countryId;
	 }
	 public int getCountryId() {
		 return countryId;
	 }
	public String getImage1() {
		return image1;
	}
	public void setImage1(String image1) {
		this.image1 = image1;
	}
	public String getImage2() {
		return image2;
	}
	public void setImage2(String image2) {
		this.image2 = image2;
	}
	public String getImage3() {
		return image3;
	}
	public void setImage3(String image3) {
		this.image3 = image3;
	}
	public String getImage4() {
		return image4;
	}
	public void setImage4(String image4) {
		this.image4 = image4;
	}
	 
}
