package com.hbm.servlet;

public class HotelWiseFacilityData {
	private int id,hotelFacilityId,hotelId;
	private String hotelFacility,hotelName;
	
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}
	public int getHotelId() {
		return hotelId;
	}
	
	public void setHotelFacilityId(int hotelFacilityId) {
		this.hotelFacilityId = hotelFacilityId;
	}
	public int getHotelFacilityId() {
		return hotelFacilityId;
	}
	
	public void setHotelFacility(String hotelFacility) {
		this.hotelFacility = hotelFacility;
	}
	public String getHotelFacility() {
		return hotelFacility;
	}
	
	
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelName() {
		return hotelName;
	}
}
