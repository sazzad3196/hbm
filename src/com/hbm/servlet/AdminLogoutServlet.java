package com.hbm.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AdminLogoutServlet extends HttpServlet{

	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException{
		try {
			HttpSession session=request.getSession();
			session.setAttribute("USER_DATA",null);
			response.sendRedirect("adminLogin.jsp");
		}catch(Exception e) {
			e.printStackTrace();
		}	
	}

}
