package com.hbm.servlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hbm.dao.CityDao;
import com.hbm.dao.CountryDao;
import com.hbm.dao.Database;
import com.hbm.dao.HotelDao;
import com.hbm.dao.HotelFacilityDao;
import com.hbm.dao.HotelSearchPageDao;
import com.hbm.dao.HotelWiseFacilityDao;
import com.hbm.dao.RoomFacilityDao;

public class HotelSearchPageServlet extends HttpServlet{
	  CityDao cityDao;
	  HotelDao hotelDao;
	  HotelFacilityDao hotelFacilityDao;
	  RoomFacilityDao roomFacilityDao;
	  HotelSearchPageDao hotelSearchPageDao;
	  HotelWiseFacilityDao hotelWiseFacilityDao;
	  public void init(ServletConfig config) throws ServletException {
		  super.init(config);
		  Database db = (Database)getServletContext().getAttribute("db");
		  cityDao = new CityDao(db);
		  hotelSearchPageDao = new HotelSearchPageDao(db);
		  hotelFacilityDao = new HotelFacilityDao(db);
		  roomFacilityDao = new RoomFacilityDao(db);
		  hotelDao = new HotelDao(db);
		  hotelWiseFacilityDao = new HotelWiseFacilityDao(db);
		  
	  }
	  
	  public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		  	int adults = Integer.parseInt(request.getParameter("adults"));
		  	int kids = Integer.parseInt(request.getParameter("kids"));
		  	int cityId = Integer.parseInt(request.getParameter("cityId"));
		    String checkIn = request.getParameter("checkIn");
		    String checkOut = request.getParameter("checkOut");
		    SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd");
		    
		  	List<CityData> ul1 = cityDao.getAllCitys();
		  	request.setAttribute("data1", ul1);
		  	HotelSearchPageData page = hotelSearchPageDao.setHotelSearchInfo(checkIn,checkOut,cityId,adults,kids);
		  	request.setAttribute("data2", page);
		  	List<HotelFacilityData> ul3 = hotelFacilityDao.getAllHotelFacility();
		  	request.setAttribute("data3", ul3);
		  	List<RoomFacilityData> ul4 = roomFacilityDao.getAllRoomFacility();
		  	request.setAttribute("data4", ul4);
		  	List<HotelData> ul5 = hotelDao.getAllHotels();
		  	request.setAttribute("data5", ul5);
		  	List<HotelWiseFacilityData> ul6 = hotelWiseFacilityDao.getAllHotelWiseFacility();
		  	request.setAttribute("data6", ul6);
		  	try {
			  	HttpSession session = request.getSession();
			    session.setAttribute("search_data", page);
		  	}catch (Exception e) {
				// TODO: handle exception
		  		e.printStackTrace();
			}
		  	
		  	request.getRequestDispatcher("/hotelSearchPage.jsp").forward(request, response);
	  }
}
