package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.Database;
import com.hbm.dao.HotelDao;
import com.hbm.dao.RoomDao;
import com.hbm.dao.RoomFacilityDao;
import com.hbm.dao.RoomWiseFacilityDao;

public class RoomWiseFacilityServlet extends HttpServlet{
	  RoomWiseFacilityDao roomWiseFacilityDao;
	  RoomDao roomDao;
	  HotelDao hotelDao;
	  RoomFacilityDao roomFacilityDao;
	  
	  public void init(ServletConfig config) throws ServletException{
		   super.init(config);
		   Database db = (Database)getServletContext().getAttribute("db");
		   roomWiseFacilityDao = new RoomWiseFacilityDao(db);
		   roomDao = new RoomDao(db);
		   hotelDao = new HotelDao(db);
		   roomFacilityDao = new RoomFacilityDao(db);
	  }
	  
	  protected void doGet(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		  	String id = request.getParameter("id");
		  	String action = request.getParameter("action");
		  	
		  	if(id == null && action == null) {
		  		  List<RoomWiseFacilityData> ul = roomWiseFacilityDao.getAllRoomWiseFacility();
		  		  request.setAttribute("data", ul);
		  		  request.getRequestDispatcher("/roomWiseFacility.jsp").forward(request, response);
		  	}
		  	else if(action.equals("createRoomWiseFacility")){
		  		 List<HotelData> ul1 = hotelDao.getHotelInfo();
		  		 request.setAttribute("data1", ul1);
		  		 
		  		 List<RoomData> ul2 = roomDao.getRoomInfo();
		  		 request.setAttribute("data2", ul2);
		  		 
		  		 List<RoomFacilityData> ul3 = roomFacilityDao.getAllRoomFacility();
		  		 request.setAttribute("data3", ul3);
		  		 request.getRequestDispatcher("/roomWiseFacility.jsp").forward(request, response);
		  	}
		  	else if(action.equals("delete")) {
		  		 int Id = Integer.parseInt(id);
		  		 roomWiseFacilityDao.deleteRoomWiseFacility(Id);
		  		 response.sendRedirect("/HBM/roomWiseFacility");
		  	}
		  	else if(action.equals("edit")) {
		  		 int Id = Integer.parseInt(id);
		  		 RoomWiseFacilityData roomWiseFacilityData = roomWiseFacilityDao.getRoomWiseFacility(Id);
		  		 request.setAttribute("data", roomWiseFacilityData);
		  		 
		  		 List<HotelData> ul1 = hotelDao.getHotelInfo();
		  		 request.setAttribute("data1", ul1);
		  		 
		  		 List<RoomData> ul2 = roomDao.getRoomInfo();
		  		 request.setAttribute("data2", ul2);
		  		 
		  		 List<RoomFacilityData> ul3 = roomFacilityDao.getAllRoomFacility();
		  		 request.setAttribute("data3", ul3);
		  		 request.getRequestDispatcher("/roomWiseFacility.jsp").forward(request, response);
		  	}
	  }
	  
	  protected void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		  	String HotelId = request.getParameter("hotelId");
		  	String RoomId = request.getParameter("roomId");
		  	String RoomFacilityId = request.getParameter("roomFacilityId");
		  	String action = request.getParameter("action");
		  	String id = request.getParameter("id");
		  	if(action.equals("create")) {
		  		 int hotelId = Integer.parseInt(HotelId);
		  		 int roomId = Integer.parseInt(RoomId);
		  		 int roomFacilityId = Integer.parseInt(RoomFacilityId);
		  		 roomWiseFacilityDao.createRoomWiseFacility(hotelId,roomId,roomFacilityId);
		  		 response.sendRedirect("/HBM/roomWiseFacility");
		  	}
		  	else if(action.equals("edit")) {
		  		 int Id = Integer.parseInt(id);
		  		 int hotelId = Integer.parseInt(HotelId);
		  		 int roomId = Integer.parseInt(RoomId);
		  		 int roomFacilityId = Integer.parseInt(RoomFacilityId);
		  		 roomWiseFacilityDao.updateRoomWiseFacility(Id,hotelId,roomId,roomFacilityId);
		  		 response.sendRedirect("/HBM/roomWiseFacility");
		  	}
	  }
}
