package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.Database;
import com.hbm.dao.HotelDao;
import com.hbm.dao.RoomDao;
import com.hbm.dao.RoomTypeDao;

public class RoomServlet extends HttpServlet{
	  RoomDao roomDao;
	  RoomTypeDao roomTypeDao;
	  HotelDao hotelDao;
	  
	  public void init(ServletConfig config)throws ServletException{
		   super.init(config);
		   Database db = (Database)getServletContext().getAttribute("db");
		   roomDao = new RoomDao(db);
		   roomTypeDao = new RoomTypeDao(db);
		   hotelDao = new HotelDao(db);
	  }
	  
	  protected void doGet(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		    String action = request.getParameter("action");
		    String id = request.getParameter("id");
		    if(id == null && action == null) {
		    	 List<RoomData> ul = roomDao.getAllRoomInfo();
		    	 request.setAttribute("data", ul);
		    	 request.getRequestDispatcher("/room.jsp").forward(request, response);
		    }
		    else if(action.equals("createRoom")) {
		    	  List<HotelData> ul = hotelDao.getHotelInfo();
				  request.setAttribute("data1", ul);
		    	  List<RoomTypeData> roomTypeData = roomTypeDao.getAllRoomType();
		    	  request.setAttribute("data", roomTypeData);
		    	  request.getRequestDispatcher("/room.jsp").forward(request,response);
		    }
		    else if(action.equals("delete")) {
		    	int Id = Integer.parseInt(id);
		    	roomDao.deleteRoom(Id);
		    	response.sendRedirect("/HBM/room");
		    }
		    else if(action.equals("edit")) {
		    	  int Id = Integer.parseInt(id);
		    	  List<RoomTypeData> roomTypeData = roomTypeDao.getAllRoomType();
		    	  request.setAttribute("data", roomTypeData); 
		    	  RoomData roomData = roomDao.getRoom(Id);
		    	  request.setAttribute("data2", roomData);
		    	  List<HotelData> ul = hotelDao.getHotelInfo();
				  request.setAttribute("data1", ul);
		    	  request.getRequestDispatcher("/room.jsp").forward(request,response);
		    }
	  }
	  
	  
	  protected void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		    String action = request.getParameter("action");
		    String roomNo =  request.getParameter("roomNo");
		    String image1 =  request.getParameter("image1");
		    String image2 =  request.getParameter("image2");
		    String image3 =  request.getParameter("image3");
		    String image4 =  request.getParameter("image4");
		    double rent = Double.parseDouble(request.getParameter("rent"));
		    int roomTypeId = Integer.parseInt(request.getParameter("roomTypeId"));
		    int hotelId = Integer.parseInt(request.getParameter("hotelId"));
		    
		    if(action.equals("create")) {
		    	  roomDao.createRoom(roomNo,rent,roomTypeId,hotelId,image1,image2,image3,image4);
		    	  response.sendRedirect("/HBM/room");
		    }
		    else if(action.equals("edit")) {
		    	  int id = Integer.parseInt(request.getParameter("id"));
		    	  roomDao.updateRoom(id,roomNo,rent,roomTypeId,hotelId,image1,image2,image3,image4);
		    	  response.sendRedirect("/HBM/room");
		    }
		    
	  }
}
