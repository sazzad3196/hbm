package com.hbm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.http.HTTPException;

import com.hbm.dao.CityDao;
import com.hbm.dao.Database;
import com.hbm.dao.UserDao;

public class CityUserServlet extends HttpServlet{
	
	CityDao cityDao;
	
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		cityDao = new CityDao(db);
	}
	
	protected void doGet(HttpServletRequest request , HttpServletResponse response) throws ServletException,IOException{
		String id = request.getParameter("id");
		String action = request.getParameter("action");
		
		if(id == null && action == null) {
			 List<CityData> ul = cityDao.getAllCitys();
			 request.setAttribute("data", ul);
			 request.getRequestDispatcher("/cityUser.jsp").forward(request, response);
		}
		else if(action.equals("edit")) {
			CityData cityData = new CityData();
			cityData = cityDao.getCity(id);
			request.setAttribute("data1", cityData);
			List<CountryData> ul = cityDao.getCountryName();
			request.setAttribute("data", ul);
			request.getRequestDispatcher("/cityUser.jsp").forward(request, response);
		}
		else if(action.equals("delete")) {
			  int Id = Integer.parseInt(id);
			  cityDao.cityDelete(Id);
			  response.sendRedirect("/HBM/cityUser");
		  }
		else if(action.equals("createCity")) {
			List<CountryData> ul = cityDao.getCountryName();
			request.setAttribute("data", ul);
			request.getRequestDispatcher("/cityUser.jsp").forward(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request , HttpServletResponse response) throws ServletException,IOException {
		  response.setContentType("text/html");
		  PrintWriter out = response.getWriter();
		  String action = request.getParameter("action");
		  String id = request.getParameter("id");
		  if(action.equals("edit")) {
			   int countryId = Integer.parseInt(request.getParameter("countryId"));
			   String name = request.getParameter("name");
			   cityDao.updateCity(id, name, countryId);
			   response.sendRedirect("/HBM/cityUser");
		  }
		  else if(action.equals("create")) {
			   String name = request.getParameter("name");
			   int countryId = Integer.parseInt(request.getParameter("countryId"));
			   cityDao.createCity(name,countryId);
			   response.sendRedirect("/HBM/cityUser");
		  }
		  
	}
	

}
