package com.hbm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.Finishings;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.Database;
import com.hbm.dao.UserDao;

public class AdminUserServlet extends HttpServlet{
	
	UserDao userDao;
	
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		Database db = (Database) getServletContext().getAttribute("db");
		userDao = new UserDao(db);
	}

	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		response.setContentType("text/html");
		String id = request.getParameter("id");
		String action = request.getParameter("action");
		
		if(id == null && action == null) {  /* from adminHome.jsp for Click Admin */
			List<UserData> ul = userDao.getAllUsers();
			request.setAttribute("data", ul);
			request.getRequestDispatcher("/adminUser.jsp").forward(request, response);
		} else if(action.equals("edit")) {
			UserData userData=new UserData();
			userData=userDao.getUser(id);
			request.setAttribute("data", userData);
			request.getRequestDispatcher("/adminUser.jsp").forward(request, response);
		} else if(action.equals("delete")) {
			userDao.deleteUser(id);
			response.sendRedirect("/HBM/adminUser"); 
		}else if(action.equals("createAdmin")) {
			 request.getRequestDispatcher("/adminUser.jsp").forward(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		/*
		 * edit from edit.jsp and create from createadmin.jsp for submit
		 */
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String action=request.getParameter("action");
		if(action.equals("edit")) {
			String Id=request.getParameter("id");
			String name=request.getParameter("name");
			String email=request.getParameter("email");
			String password=request.getParameter("password");
			String phoneNo=request.getParameter("phoneNo");
			int id=Integer.parseInt(Id);
			userDao.updateUser(id , name , email , password , phoneNo);
			response.sendRedirect("/HBM/adminUser");
		}else if(action.equals("create")) {
			String name=request.getParameter("name");
			 String email=request.getParameter("email");
			 String password=request.getParameter("password");
			 String phoneNo=request.getParameter("phoneNo");
			 userDao.createUser(name , email , password , phoneNo);
			 response.sendRedirect("/HBM/adminUser");
		}
		
	}	
	
		
	
	
}
