package com.hbm.servlet;

public class RoomWiseFacilityData {
	private int id,roomId,roomFacilityId,hotelId;
	private String roomFacility,roomNumber,hotelName;
	
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public int getRoomId() {
		return roomId;
	}
	
	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}
	public int getHotelId() {
		return hotelId;
	}
	
	public void setRoomFacilityId(int roomFacilityId) {
		this.roomFacilityId = roomFacilityId;
	}
	public int getRoomFacilityId() {
		return roomFacilityId;
	}
	
	public void setRoomFacility(String roomFacility) {
		this.roomFacility = roomFacility;
	}
	public String getRoomFacility() {
		return roomFacility;
	}
	
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	public String getRoomNumber() {
		return roomNumber;
	}
	
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelName() {
		return hotelName;
	}
}
