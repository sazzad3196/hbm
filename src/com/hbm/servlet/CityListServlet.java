package com.hbm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.hbm.dao.CityDao;
import com.hbm.dao.Database;


public class CityListServlet extends HttpServlet{
	
	CityDao cityDao;
	
	public void init(ServletConfig config)throws ServletException {
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		cityDao = new CityDao(db);
	}
	
	protected void doGet(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException {
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		String countryIdStr = request.getParameter("countryId");
		int countryId = Integer.parseInt(countryIdStr);
		int id = countryId;
		List<CityData> ul = cityDao.getCityList(id);
		Gson gson = new Gson();
		String json = gson.toJson(ul);
		out.print(json);
		out.flush();
	}
}
