package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.Database;
import com.hbm.dao.RoomDao;
import com.hbm.dao.RoomWiseFacilityDao;

public class RoomDetailsServlet extends HttpServlet{
	
	RoomDao roomDao;
	RoomWiseFacilityDao roomWiseFacilityDao;
	
	public void init(ServletConfig config) throws ServletException {
		  super.init(config);
		  Database db = (Database)getServletContext().getAttribute("db");
		  roomDao = new RoomDao(db);
		  roomWiseFacilityDao = new RoomWiseFacilityDao(db);
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		 int roomId = Integer.parseInt(request.getParameter("roomId"));
		 RoomData roomData = roomDao.getRoom(roomId);
		 request.setAttribute("data1", roomData);
		 List<RoomWiseFacilityData> ul2 = roomWiseFacilityDao.getAllRoomWiseFacility();
		 request.setAttribute("data2", ul2);
		 request.getRequestDispatcher("/roomDetails.jsp").forward(request, response);
	}

}
