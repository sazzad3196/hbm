package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.BookingDao;
import com.hbm.dao.CheckoutDao;
import com.hbm.dao.Database;

public class ViewMoreBookingServlet extends HttpServlet{
	
	BookingDao bookingDao;
	CheckoutDao checkoutDao;
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		bookingDao = new BookingDao(db);
		checkoutDao = new CheckoutDao(db);
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		  int bookingId = Integer.parseInt(request.getParameter("bookingId"));
		  List<CheckoutData> ul = checkoutDao.getCheckoutInfo(bookingId);
		  request.setAttribute("data", ul);
		  request.getRequestDispatcher("/viewMoreBooking.jsp").forward(request, response);
	}
}
