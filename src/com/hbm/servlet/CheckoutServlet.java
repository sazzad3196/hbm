package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.CheckoutDao;
import com.hbm.dao.Database;

public class CheckoutServlet extends HttpServlet{
	
	CheckoutDao checkoutDao;
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		checkoutDao = new CheckoutDao(db);
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		  int clientId = Integer.parseInt(request.getParameter("clientId"));
		  List<CheckoutData> ul = checkoutDao.getAllCheckoutInfo(clientId);
		  request.setAttribute("data", ul);
		  request.getRequestDispatcher("/checkout.jsp").forward(request, response);
		  
	}
}
