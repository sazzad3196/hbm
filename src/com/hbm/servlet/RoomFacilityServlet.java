package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.Database;
import com.hbm.dao.RoomFacilityDao;

public class RoomFacilityServlet extends HttpServlet{
	   RoomFacilityDao roomFacilityDao;
	   
	   public void init(ServletConfig config) throws ServletException{
		   	super.init(config);
		   	Database db = (Database)getServletContext().getAttribute("db");
		   	roomFacilityDao = new RoomFacilityDao(db);
	   }
	   
	   public void doGet(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		     String action = request.getParameter("action");
		     String id = request.getParameter("id");
		     
		     if(id == null && action == null) {
		    	  List<RoomFacilityData> ul = roomFacilityDao.getAllRoomFacility();
		    	  request.setAttribute("data", ul);
		    	  request.getRequestDispatcher("/roomFacility.jsp").forward(request, response);
		     }
		     else if(action.equals("createRoom")) {
		    	  request.getRequestDispatcher("/roomFacility.jsp").forward(request, response);
		     }
		     else if(action.equals("delete")) {
		    	  int Id = Integer.parseInt(id);
		    	  roomFacilityDao.deleteRoomFacility(Id);
		    	  response.sendRedirect("/HBM/roomFacility");
		     }
		     else if(action.equals("edit")) {
		    	  int Id = Integer.parseInt(id);
		    	  RoomFacilityData ul = roomFacilityDao.getRoomFacility(Id);
		    	  request.setAttribute("data", ul);
		    	  request.getRequestDispatcher("/roomFacility.jsp").forward(request, response);
		     }
	   }
	   
	   public void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		     String action = request.getParameter("action");
		     String roomFacility = request.getParameter("roomFacility");
		     if(action.equals("create")) {
		    	   roomFacilityDao.createRoomFacility(roomFacility);
		    	   response.sendRedirect("/HBM/roomFacility");
		     }
		     else if(action.equals("edit")) {
		    	   int id = Integer.parseInt(request.getParameter("id"));
		    	   roomFacilityDao.updateRoomFacility(id,roomFacility);
		    	   response.sendRedirect("/HBM/roomFacility");
		     }
		     
	   }
}
