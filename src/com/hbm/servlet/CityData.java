package com.hbm.servlet;

import javax.servlet.http.HttpServlet;

public class CityData extends HttpServlet{
	private int id;
	private int countryId;
	private String name;
	private String countryName = null;
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryName() {
		return countryName;
	}

}
