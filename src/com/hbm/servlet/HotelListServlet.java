package com.hbm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.hbm.dao.CityDao;
import com.hbm.dao.Database;
import com.hbm.dao.HotelDao;

public class HotelListServlet extends HttpServlet{
	 HotelDao hotelDao;
	 
	 public void init(ServletConfig config)throws ServletException {
			super.init(config);
			Database db = (Database)getServletContext().getAttribute("db");
			hotelDao = new HotelDao(db);
	}
		
	 protected void doGet(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException {
			response.setContentType("application/json");
			PrintWriter out = response.getWriter();
			int countryId = Integer.parseInt(request.getParameter("countryId"));
			int cityId = Integer.parseInt(request.getParameter("cityId"));
			List<HotelData> ul = hotelDao.selectHotelName(countryId,cityId);
			Gson gson = new Gson();
			String json = gson.toJson(ul);
			out.print(json);
			out.flush();
		} 
	 
}
