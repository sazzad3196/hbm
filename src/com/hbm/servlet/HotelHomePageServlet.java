package com.hbm.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.CityDao;
import com.hbm.dao.CountryDao;
import com.hbm.dao.Database;

public class HotelHomePageServlet extends HttpServlet{
	  CountryDao countryDao;
	  CityDao cityDao;
	  public void init(ServletConfig config) throws ServletException {
		  super.init(config);
		  Database db = (Database)getServletContext().getAttribute("db");
		  countryDao = new CountryDao(db);
		  cityDao = new CityDao(db);
		  
	  }
	  
	  public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		  	List<CityData> ul1 = cityDao.getAllCitys();
		  	request.setAttribute("data1", ul1);
		  	request.getRequestDispatcher("/hotelHomePage.jsp").forward(request, response);
	  }
}
