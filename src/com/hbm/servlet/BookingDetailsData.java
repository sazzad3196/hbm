  package com.hbm.servlet;

import java.util.List;

public class BookingDetailsData {
	    int id,roomId,bookingId;
	    double rent;
	    RoomData roomData;
	    
		public RoomData getRoomData() {
			return roomData;
		}
		public void setRoomData(RoomData roomData) {
			this.roomData = roomData;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getRoomId() {
			return roomId;
		}
		public void setRoomId(int roomId) {
			this.roomId = roomId;
		}
		public int getBookingId() {
			return bookingId;
		}
		public void setBookingId(int bookingId) {
			this.bookingId = bookingId;
		}
		public double getRent() {
			return rent;
		}
		public void setRent(double rent) {
			this.rent = rent;
		}
 }
