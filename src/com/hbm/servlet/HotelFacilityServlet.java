package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.Database;
import com.hbm.dao.HotelFacilityDao;
import com.hbm.dao.RoomFacilityDao;

public class HotelFacilityServlet extends HttpServlet{
	
	 HotelFacilityDao hotelFacilityDao;
	 public void init(ServletConfig config) throws ServletException{
		   	super.init(config);
		   	Database db = (Database)getServletContext().getAttribute("db");
		   	hotelFacilityDao = new HotelFacilityDao(db);
	 }
	 
	 
	 public void doGet(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
	     String action = request.getParameter("action");
	     String id = request.getParameter("id");
	     
	     if(id == null && action == null) {
	    	  List<HotelFacilityData> ul = hotelFacilityDao.getAllHotelFacility();
	    	  request.setAttribute("data", ul);
	    	  request.getRequestDispatcher("/hotelFacility.jsp").forward(request, response);
	     }
	     else if(action.equals("createHotel")) {
	    	  request.getRequestDispatcher("/hotelFacility.jsp").forward(request, response);
	     }
	     else if(action.equals("delete")) {
	    	  int Id = Integer.parseInt(id);
	    	  hotelFacilityDao.deleteHotelFacility(Id);
	    	  response.sendRedirect("/HBM/hotelFacility");
	     }
	     else if(action.equals("edit")) {
	    	  int Id = Integer.parseInt(id);
	    	  HotelFacilityData ul = hotelFacilityDao.getHotelFacility(Id);
	    	  request.setAttribute("data", ul);
	    	  request.getRequestDispatcher("/hotelFacility.jsp").forward(request, response);
	     }
	 }
	 
	 
	 public void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
	     String action = request.getParameter("action");
	     String hotelFacility = request.getParameter("hotelFacility");
	     if(action.equals("create")) {
	    	   hotelFacilityDao.createHotelFacility(hotelFacility);
	    	   response.sendRedirect("/HBM/hotelFacility");
	     }
	     else if(action.equals("edit")) {
	    	   int id = Integer.parseInt(request.getParameter("id"));
	    	   hotelFacilityDao.updateHotelFacility(id,hotelFacility);
	    	   response.sendRedirect("/HBM/hotelFacility");
	     }
	     
   }

}
