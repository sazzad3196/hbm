package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.Database;
import com.hbm.dao.HotelDao;
import com.hbm.dao.HotelFacilityDao;
import com.hbm.dao.HotelWiseFacilityDao;
import com.hbm.dao.RoomDao;
import com.hbm.dao.RoomWiseFacilityDao;

public class HotelDetailsServlet extends HttpServlet{
	
	HotelDao hotelDao;
	RoomDao roomDao;
	HotelFacilityDao hotelFacilityDao;
	HotelWiseFacilityDao hotelWiseFacilityDao;
	RoomWiseFacilityDao roomWiseFacilityDao;
		
	public void init(ServletConfig config) throws ServletException {
		  super.init(config);
		  Database db = (Database)getServletContext().getAttribute("db");
		  hotelDao = new HotelDao(db);
		  roomDao = new RoomDao(db);
		  hotelFacilityDao = new HotelFacilityDao(db);
		  hotelWiseFacilityDao = new HotelWiseFacilityDao(db);
		  roomWiseFacilityDao = new RoomWiseFacilityDao(db);
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		  int hotelId = Integer.parseInt(request.getParameter("hotelId"));
		  HotelData hotelData = hotelDao.getHotel(hotelId);
		  request.setAttribute("data1", hotelData);
		  List<RoomData> ul2 = roomDao.selectRoom(hotelId);
		  request.setAttribute("data2", ul2);
		  List<HotelWiseFacilityData> ul3 = hotelWiseFacilityDao.HotelWiseFacility(hotelId);
		  request.setAttribute("data3", ul3);
		  List<HotelFacilityData> ul4 = hotelFacilityDao.getAllHotelFacility();
		  request.setAttribute("data4", ul4);
		  List<RoomWiseFacilityData> ul5 = roomWiseFacilityDao.getAllRoomWiseFacility();
		  request.setAttribute("data5", ul5);
		  request.getRequestDispatcher("/hotelDetails.jsp").forward(request, response);
		  
	}
}
