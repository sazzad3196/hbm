package com.hbm.servlet;

public class RoomData {
	private int id,roomTypeId,hotelId;
	private String roomType,hotelName,roomNo,image1,image2,image3,image4;
	private double rent;
	
	
	public String getImage1() {
		return image1;
	}
	public void setImage1(String image1) {
		this.image1 = image1;
	}
	public String getImage2() {
		return image2;
	}
	public void setImage2(String image2) {
		this.image2 = image2;
	}
	public String getImage3() {
		return image3;
	}
	public void setImage3(String image3) {
		this.image3 = image3;
	}
	public String getImage4() {
		return image4;
	}
	public void setImage4(String image4) {
		this.image4 = image4;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	public String getRoomNo() {
		return roomNo;
	}
	
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getRoomType() {
		return roomType;
	}
	
	public void setRoomTypeId(int roomTypeId) {
		this.roomTypeId = roomTypeId;
	}
	public int getRoomTypeId() {
		return roomTypeId;
	}
	
	public void setRent(double rent) {
		this.rent = rent;
	}
	public double getRent() {
		return rent;
	}
	
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getHotelName() {
		return hotelName;
	}
	
	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}
	public int getHotelId() {
		return hotelId;
	}
}
