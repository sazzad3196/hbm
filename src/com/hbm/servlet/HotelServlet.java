package com.hbm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.http.HTTPException;

import com.hbm.dao.CityDao;
import com.hbm.dao.Database;
import com.hbm.dao.HotelDao;
import com.hbm.dao.UserDao;

public class HotelServlet extends HttpServlet{
	
	HotelDao hotelDao;
	
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		hotelDao = new HotelDao(db);
	}
	
	protected void doGet(HttpServletRequest request , HttpServletResponse response) throws ServletException,IOException{
		String id = request.getParameter("id");
		String action = request.getParameter("action");
		
		if(id == null && action == null) {
			 List<HotelData> ul = hotelDao.getAllHotels();
			 request.setAttribute("data", ul);
			 request.getRequestDispatcher("/hotel.jsp").forward(request, response);
		}
		else if(action.equals("createHotel")) {
			 List<CityData> ul = hotelDao.getCity(0);
			 request.setAttribute("data", ul);
			 List<CountryData> ul1 = hotelDao.getCountry();
			 request.setAttribute("data1", ul1);
			 request.getRequestDispatcher("/hotel.jsp").forward(request, response);
		}
		else if(action.equals("delete")) {
			 int Id = Integer.parseInt(id);
			 hotelDao.hotelDelete(Id);
			 response.sendRedirect("/HBM/hotel");
		}
		else if(action.equals("edit")) {
			 int Id =  Integer.parseInt(id);
			 HotelData hotelData = hotelDao.getHotel(Id);
			 request.setAttribute("data2", hotelData);
			 
			 int countryId = hotelData.getCountryId();
			 List<CountryData> ul2 = hotelDao.getCountry();
			 request.setAttribute("data1", ul2);
			 
			 List<CityData> ul = hotelDao.getCity(countryId);
			 request.setAttribute("data", ul);
			 request.getRequestDispatcher("/hotel.jsp").forward(request, response);
		}
		
	}
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException{
		  String action = request.getParameter("action");
		  if(action.equals("create")) {
			   int countryId = Integer.parseInt(request.getParameter("countryId"));
			   int cityId = Integer.parseInt(request.getParameter("cityId"));
			   int starRating = Integer.parseInt(request.getParameter("starRating"));
			   String hotelName = request.getParameter("hotelName");
			   String email = request.getParameter("email");
			   String phoneNo = request.getParameter("phoneNo");
			   String address = request.getParameter("address");
			   String checkIn = request.getParameter("checkIn");
			   String checkOut = request.getParameter("checkOut");
			   String image1 =  request.getParameter("image1");
			   String image2 =  request.getParameter("image2");
			   String image3 =  request.getParameter("image3");
			   String image4 =  request.getParameter("image4");
			   
//			    System.out.println("CheckOut: "+checkOut);
//				System.out.println("CheckIn: "+checkIn);
//				System.out.println("HotelName: "+hotelName);
//				System.out.println("email: "+email);
//				System.out.println("phoneNo: "+phoneNo);
//				System.out.println("addrss: "+address);
//				System.out.println("city: "+cityId);
//				System.out.println("StarRating: "+starRating);
				
				
			   hotelDao.createHotel(countryId,cityId,starRating,hotelName,email,phoneNo,address,checkIn,checkOut,image1,image2,image3,image4);
			   response.sendRedirect("/HBM/hotel");
		  }
		  else if(action.equals("edit")) {
			   int id = Integer.parseInt(request.getParameter("id"));
			   int cityId = Integer.parseInt(request.getParameter("cityId"));
			   int countryId = Integer.parseInt(request.getParameter("countryId"));
			   int starRating = Integer.parseInt(request.getParameter("starRating"));
			   String hotelName = request.getParameter("hotelName");
			   String email = request.getParameter("email");
			   String phoneNo = request.getParameter("phoneNo");
			   String address = request.getParameter("address");
			   String checkIn = request.getParameter("checkIn");
			   String checkOut = request.getParameter("checkOut");
			   String image1 =  request.getParameter("image1");
			   String image2 =  request.getParameter("image2");
			   String image3 =  request.getParameter("image3");
			   String image4 =  request.getParameter("image4");
			   hotelDao.updateHotel(id,countryId,cityId,starRating,hotelName,email,phoneNo,address,checkIn,checkOut,image1,image2,image3,image4);
			   response.sendRedirect("/HBM/hotel");
		  }
	}
	
	
	

}
