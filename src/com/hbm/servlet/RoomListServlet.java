package com.hbm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.hbm.dao.CityDao;
import com.hbm.dao.Database;
import com.hbm.dao.RoomDao;

public class RoomListServlet extends HttpServlet{
    RoomDao roomDao;
	
	public void init(ServletConfig config)throws ServletException {
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		roomDao = new RoomDao(db);
	}
	
	protected void doGet(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException {
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		int hotelId = Integer.parseInt(request.getParameter("hotelId"));
		List<RoomData> ul = roomDao.selectRoom(hotelId); 
		Gson gson = new Gson();
		String json = gson.toJson(ul);
		out.print(json);
		out.flush();
	}
}
