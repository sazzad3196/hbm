package com.hbm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import com.google.gson.Gson;
import com.hbm.dao.BookingDao;
import com.hbm.dao.ClientLoginDao;
import com.hbm.dao.Database;

public class BookingServlet extends HttpServlet{
	ClientLoginDao clientLoginDao;
	BookingDao bookingDao;
	
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		clientLoginDao = new ClientLoginDao(db);
		bookingDao = new BookingDao(db);
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		
		 response.setContentType("application/json");
		 PrintWriter out = response.getWriter();
		 HttpSession session = request.getSession();
		 HotelSearchPageData hotelSearchPage = (HotelSearchPageData)session.getAttribute("search_data");
		 
		 int bookedBy = 0;
		 int adults = hotelSearchPage.getAdults();
		 int kids = hotelSearchPage.getKids();
		 String action = request.getParameter("action");
		 int hotelId = Integer.parseInt(request.getParameter("hotelId"));
		 int roomId = Integer.parseInt(request.getParameter("roomId"));
		 double rent = Double.parseDouble(request.getParameter("rent"));
		 String checkIn = hotelSearchPage.getCheckInData();
		 String checkOut = hotelSearchPage.getCheckOutData();
		 if(action.equals("login")) {
			 String key = null;
			 String userName = request.getParameter("userName");
			 String password = request.getParameter("password");
			 ClientData clientData = clientLoginDao.doSingIn(userName , password);
			 if(clientData != null) {
				   bookedBy = clientData.getId();
				   session.setAttribute("client_data", clientData);
				   BookingData bookingData = bookingDao.checkBooking(hotelId,bookedBy,hotelSearchPage.getCheckInData(),hotelSearchPage.getCheckOutData()); // bookedBy means clientData.getId();
				   if(bookingData == null) {
					   bookingDao.createBooking(hotelId,bookedBy,adults,kids,checkIn,checkOut);
					   BookingData bookingData1 = bookingDao.checkBooking(hotelId,bookedBy,hotelSearchPage.getCheckInData(),hotelSearchPage.getCheckOutData());
					   int bookingId = bookingData1.getId();
					   bookingDao.createBookingDetails(roomId,bookingId,rent);
					   key = "Successfully Login and Successfully you have done booking this room";
				   }
				   else {
					    BookingDetailsData bookingDetailsData = bookingDao.checkBookingDetails(bookingData.getId(), roomId);
						if(bookingDetailsData == null) {
							  bookingDao.newUpdateStatusBookingInfo(bookingData.getId());
							  bookingDao.createBookingDetails(roomId,bookingData.getId(),rent);
							  key = "Successfully Login but you have done booking this room";
						}
						else {
					          key = "Successfully Login but Already you had done booking this room";
						}
					   
				   }
				   
			 }
			 else {
				  key = "Please enter correct Username and Password";
			 }
			 List<BookingData> ul = bookingDao.getAllBooking();
			 session.setAttribute("booking_data", ul);
			 Gson gson = new Gson();
			 String value = gson.toJson(key);
			 out.print(value);
			 out.flush();
		 }
		 else {
			 String key = null;
			 ClientData client = (ClientData)session.getAttribute("client_data");
			 bookedBy = client.getId();
			 BookingData bookingData = bookingDao.checkBooking(hotelId,bookedBy,hotelSearchPage.getCheckInData(),hotelSearchPage.getCheckOutData()); // bookedBy means client.getId();
			 if(bookingData == null) {
				  bookingDao.createBooking(hotelId,bookedBy,adults,kids,checkIn,checkOut);
				  BookingData bookingData1 = bookingDao.checkBooking(hotelId,bookedBy,hotelSearchPage.getCheckInData(),hotelSearchPage.getCheckOutData());
				  int bookingId = bookingData1.getId();
				  bookingDao.createBookingDetails(roomId,bookingId,rent);
				  key = "You have done successfully booking this room.";
			 }
			 else {
				  BookingDetailsData bookingDetailsData = bookingDao.checkBookingDetails(bookingData.getId(), roomId);
				  if(bookingDetailsData == null) {
					  bookingDao.newUpdateStatusBookingInfo(bookingData.getId());
					  bookingDao.createBookingDetails(roomId,bookingData.getId(),rent);
					  key = "Now you have done booking this room.";
				  }
				  else {
				      key = "Already you had done booking this room.";
				  }
			 }
			 List<BookingData> ul = bookingDao.getAllBooking();
			 session.setAttribute("booking_data", ul);
			 Gson gson = new Gson();
			 String value = gson.toJson(key);
			 out.print(value);
			 out.flush();
		 }

	}

}
