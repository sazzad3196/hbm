package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.BookingDao;
import com.hbm.dao.Database;

public class ViewBookingServlet extends HttpServlet{
	BookingDao bookingDao;	
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		bookingDao = new BookingDao(db);
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		  List<BookingData> ul = bookingDao.getBooking();
		  request.setAttribute("data", ul);
		  request.getRequestDispatcher("/viewBooking.jsp").forward(request,response);
	}
}
