package com.hbm.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.ClientLoginDao;
import com.hbm.dao.Database;

public class ClientRegisterServlet extends HttpServlet{
    ClientLoginDao clientLoginDao;
	
	public void init(ServletConfig config) throws ServletException {
		  super.init(config);
		  Database db = (Database)getServletContext().getAttribute("db");
		  clientLoginDao = new ClientLoginDao(db);
	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		String userName = request.getParameter("userName");
	    String password = request.getParameter("password");
	    String phoneNo = request.getParameter("phoneNo");
	    String email = request.getParameter("email");
	    String gender = request.getParameter("gender");
	    String action = request.getParameter("action");
	    int age = Integer.parseInt(request.getParameter("age"));
	    
	    if(action.equals("clientRegister")) {
		    clientLoginDao.doRegister(userName,password,phoneNo,email,gender,age);
		    response.sendRedirect("/HBM/clientLogin.jsp");
	    }
	}
}
