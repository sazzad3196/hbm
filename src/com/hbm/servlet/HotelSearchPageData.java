package com.hbm.servlet;

import java.util.Date;

public class HotelSearchPageData {
	
    int adults,kids,cityId;
    String checkInData,checkOutData;
     
	public int getAdults() {
		return adults;
	}
	public void setAdults(int adults) {
		this.adults = adults;
	}
	public int getKids() {
		return kids;
	}
	public void setKids(int kids) {
		this.kids = kids;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public String getCheckInData() {
		return checkInData;
	}
	public void setCheckInData(String checkInData) {
		this.checkInData = checkInData;
	}
	public String getCheckOutData() {
		return checkOutData;
	}
	public void setCheckOutData(String checkOutData) {
		this.checkOutData = checkOutData;
	}
}
