package com.hbm.servlet;

public class RoomFacilityData {
	private int id;
	private String facilityName;
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}
	public String getFacilityName() {
		return facilityName;
	}
}
