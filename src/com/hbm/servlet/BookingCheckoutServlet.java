package com.hbm.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hbm.dao.BookingDao;
import com.hbm.dao.BookingDetailsDao;
import com.hbm.dao.CheckoutDao;
import com.hbm.dao.Database;

public class BookingCheckoutServlet extends HttpServlet{
	
    BookingDetailsDao bookingDetailsDao;
    BookingDao bookingDao;
    CheckoutDao checkoutDao;
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		bookingDao = new BookingDao(db);
		checkoutDao = new CheckoutDao(db);
		bookingDetailsDao = new BookingDetailsDao(db);
	}
	
	public void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		 String action = request.getParameter("action");
		 HttpSession session = request.getSession();
		 if(action.equals("delete")) {
			  int bookingDetailsId = Integer.parseInt(request.getParameter("bookingDetailsId"));
			  int clientId = Integer.parseInt(request.getParameter("clientId"));
			  int bookingId = Integer.parseInt(request.getParameter("bookingId"));
			  bookingDetailsDao.deleteBookingDetails(bookingDetailsId);
			  List<BookingDetailsData> ul = bookingDetailsDao.getBookingDetails(bookingId);
			  if(ul == null) {
				  bookingDao.deleteBooking(bookingId);
				  List<BookingData> ul1 = bookingDao.getAllBooking();
				  session.setAttribute("booking_data", ul1);
				  response.sendRedirect("/HBM/hotelHomePage");
			  }
			  else {
				  List<BookingData> ul1 = bookingDao.getAllBooking();
				  session.setAttribute("booking_data", ul1);
				  response.sendRedirect("/HBM/checkout?clientId=" + clientId);
			  }
			  
		 }
		 else {
			 Date date = new Date();  
			 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			 String paymentDate = formatter.format(date);
			 int bookingId = Integer.parseInt(request.getParameter("bookingId"));
			 int clientId = Integer.parseInt(request.getParameter("clientId"));
			 bookingDao.updateStatusBookingInfo(bookingId,paymentDate);
			 List<BookingData> ul = bookingDao.getAllBooking();
			 session.setAttribute("booking_data", ul);
			 List<CheckoutData> ul1 = checkoutDao.getCheckoutInfo(bookingId);
			 request.setAttribute("data", ul1);
			 request.getRequestDispatcher("/confirmBooking.jsp").forward(request, response);
		 }
	}
}
