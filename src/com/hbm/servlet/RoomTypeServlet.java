package com.hbm.servlet;

import java.awt.Desktop.Action;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.Database;
import com.hbm.dao.HotelDao;
import com.hbm.dao.RoomTypeDao;

public class RoomTypeServlet extends HttpServlet{
	 RoomTypeDao roomTypeDao;
	 
	 public void init(ServletConfig config) throws ServletException{
		 super.init(config);
		 Database db = (Database)getServletContext().getAttribute("db");
		 roomTypeDao = new RoomTypeDao(db);
		 
	 }
	 
	 protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException{
		  String id = request.getParameter("id");
		  String action = request.getParameter("action");
		  
		  if(id == null && action == null) {
			  	List<RoomTypeData> ul = roomTypeDao.getAllRoomType();
			  	request.setAttribute("data", ul);
			  	request.getRequestDispatcher("/roomType.jsp").forward(request, response);
		  }
		  else if(action.equals("createRoomType")) {
			   request.getRequestDispatcher("/roomType.jsp").forward(request, response);
		  }
		  else if(action.equals("delete")) {
			  int Id = Integer.parseInt(id);
			  roomTypeDao.deleteRoomType(Id);
			  response.sendRedirect("/HBM/roomType");
		  }
		  else if(action.equals("edit")) {
			   int Id = Integer.parseInt(id);
			   List<RoomTypeData> ul = roomTypeDao.getAllRoomType();
			   request.setAttribute("data", ul);
			   
			   RoomTypeData roomTypeData = roomTypeDao.getRoomType(Id);
			   request.setAttribute("data1", roomTypeData);
			   request.getRequestDispatcher("/roomType.jsp").forward(request,response);
		  }
	 }
	 
	 protected void doPost(HttpServletRequest request,HttpServletResponse response)throws IOException,ServletException{
		 String action = request.getParameter("action");
		 if(action.equals("create")) {
			  String roomType = request.getParameter("roomType");
			  System.out.println("RoomType: " + roomType);
			  roomTypeDao.createRoomType(roomType);
			  response.sendRedirect("/HBM/roomType");
		 }else if(action.equals("edit")) {
			  int id = Integer.parseInt(request.getParameter("id"));
			  String roomType = request.getParameter("roomType");
			  roomTypeDao.updateRoomType(id,roomType);
			  response.sendRedirect("/HBM/roomType");
		 }
	 }
}
