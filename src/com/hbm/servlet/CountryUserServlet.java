package com.hbm.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hbm.dao.CountryDao;
import com.hbm.dao.Database;

public class CountryUserServlet extends HttpServlet{
	CountryDao countryDao;
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		Database db = (Database)getServletContext().getAttribute("db");
		countryDao = new CountryDao(db);
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String action = request.getParameter("action");
		  
		if(id == null && action == null) {
			  List <CountryData> ul = countryDao.getAllCountrys();
			  request.setAttribute("data", ul);
		      request.getRequestDispatcher("/countryUser.jsp").forward(request, response);
		}
		else if(action.equals("createCountry")) {
			 request.getRequestDispatcher("/countryUser.jsp").forward(request, response);
		}
		else if(action.equals("edit")) {
			CountryData countryData = countryDao.getCountry(id);
			request.setAttribute("data", countryData);
			request.getRequestDispatcher("/countryUser.jsp").forward(request, response);
		}
		else if(action.equals("delete")) {
			int Id = Integer.parseInt(id);
			countryDao.countryDelete(Id);
			response.sendRedirect("/HBM/countryUser");
		}
	}
	
	
    protected void doPost(HttpServletRequest request , HttpServletResponse response) throws ServletException,IOException {
    	String action = request.getParameter("action");
		if(action.equals("edit")) {
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			countryDao.updateCountry(id,name);
			response.sendRedirect("/HBM/countryUser");
		}
		else if(action.equals("create")) {
			String name = request.getParameter("name");
			System.out.println(name);
			countryDao.createCountry(name);
			response.sendRedirect("/HBM/countryUser");
		}
    	
	}


}
