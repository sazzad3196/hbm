package com.hbm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hbm.dao.BookingDao;
import com.hbm.dao.Database;
import com.hbm.dao.UserDao;



public class AdminLoginServlet extends HttpServlet{
	
    UserDao userDao;
    BookingDao bookingDao;
	
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
		Database db = (Database) getServletContext().getAttribute("db");
		userDao = new UserDao(db);
		bookingDao = new BookingDao(db);
	}
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		
		UserData userData = userDao.adminLogin(userName , password);
		if(userData == null) {
	    	 response.sendRedirect("adminLogin.jsp");
		}else {
			HttpSession session = request.getSession();
			List<BookingData> ul = bookingDao.getAllBooking();
			session.setAttribute("booking_data", ul);
	    	session.setAttribute("USER_DATA", userData);
	        response.sendRedirect("adminHome.jsp");
		}
		
	}

}
