package com.hbm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.hbm.dao.BookingDao;
import com.hbm.dao.ClientLoginDao;
import com.hbm.dao.Database;

public class ClientLoginServlet extends HttpServlet{
	
	ClientLoginDao clientLoginDao;
	BookingDao bookingDao;
	
	public void init(ServletConfig config) throws ServletException {
		  super.init(config);
		  Database db = (Database)getServletContext().getAttribute("db");
		  clientLoginDao = new ClientLoginDao(db);
		  bookingDao = new BookingDao(db);
	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		  PrintWriter out = response.getWriter();
		  String userName = request.getParameter("userName");
		  String password = request.getParameter("password");
		  String action = request.getParameter("action");
		  ClientData clientData = clientLoginDao.doSingIn(userName , password);
		  
		  if(action.equals("clientLogin")) {
			  if(clientData == null) {
				  response.sendRedirect("/HBM/clientLogin.jsp");
			  }
			  else {
				   List<BookingData> ul = bookingDao.getAllBooking();
				   HttpSession session = request.getSession();
				   session.setAttribute("client_data", clientData);
				   session.setAttribute("booking_data", ul);
				   response.sendRedirect("/HBM/hotelHomePage");
			  }
		  }
	}
}
