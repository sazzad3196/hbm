package com.hbm.servlet;

public class HotelFacilityData {
	private int id;
	private String hotelFacility;
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	
	public void setHotelFacility(String hotelFacility) {
		this.hotelFacility = hotelFacility;
	}
	public String getHotelFacility() {
		return hotelFacility;
	}
}
