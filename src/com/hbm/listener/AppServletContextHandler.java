package com.hbm.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.hbm.dao.Database;

public class AppServletContextHandler implements ServletContextListener{
	
	public void contextInitialized(ServletContextEvent event) {
		ServletContext sc=event.getServletContext();
		String databaseDriver = sc.getInitParameter("databaseDriver");
		String databaseName = sc.getInitParameter("databaseName");
		String username = sc.getInitParameter("username");
		String password = sc.getInitParameter("password");
		Database db = new Database(databaseDriver, databaseName, username, password);
		db.open();
		sc.setAttribute("db", db);
	}
	
	public void contextDestroyed(ServletContextEvent event) {
		ServletContext sc=event.getServletContext();
		Database db = (Database) sc.getAttribute("db");
		db.close();
    }

}
