package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.RoomData;
import com.hbm.servlet.RoomFacilityData;

public class RoomFacilityDao {
	 Database database;
	 public RoomFacilityDao(Database db) {
		 database = db;
	 }
	 
	 public List<RoomFacilityData> getAllRoomFacility(){
		  List<RoomFacilityData> ul = new ArrayList<RoomFacilityData>();
		  String sql = null;
		  Statement statement = null;
		  ResultSet RS = null;
		  try {
			 statement = database.getConnection().createStatement();
			 sql = " select * from roomfacilityinfo ";
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 RoomFacilityData roomData = new RoomFacilityData();
				 roomData.setId(Integer.parseInt(RS.getString(1)));
				 roomData.setFacilityName(RS.getString(2));
				 ul.add(roomData);
			 }
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		  }
		  return ul;
	 }
	 
	 public void createRoomFacility(String roomFacility) {
		 String sql = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " insert into roomfacilityinfo(roomFacility) values ('" + roomFacility + "')";
			 statement.executeUpdate(sql);
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		  }
	 }
	 
	 public void deleteRoomFacility(int id) {
		 String sql = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " delete from roomfacilityinfo where id= " + id;
			 statement.executeUpdate(sql);
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		  }
	 }
	 
	 public RoomFacilityData getRoomFacility(int id) {
		  String sql = null;
		  Statement statement = null;
		  ResultSet RS = null;
		  RoomFacilityData roomData = new RoomFacilityData();
		  try {
			 statement = database.getConnection().createStatement();
			 sql = " select * from roomfacilityinfo where id = " + id;
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 roomData.setId(Integer.parseInt(RS.getString(1)));
				 roomData.setFacilityName(RS.getString(2));
			 }
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		  }
		  
		  return roomData;
	 }
	 
	 public void updateRoomFacility(int id,String roomFacility) {
		 String sql = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " update roomfacilityinfo set roomFacility='" + roomFacility + "' where id= " + id;
			 statement.executeUpdate(sql);
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		  }
	 }
}
