package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.CityData;
import com.hbm.servlet.CountryData;

public class CityDao {
	Database database = null;
	
	public CityDao(Database db) {
		database = db;
	}
	
	public List<CityData> getAllCitys() {
		 List<CityData> ul = new ArrayList<CityData>();
		 String sql = null;
		 Statement statement = null;
		 ResultSet RS = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select cityinfo.*, countryinfo.name from cityinfo left outer join countryinfo on cityinfo.CountryId = countryinfo.id";
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 int Id = Integer.parseInt(RS.getString(1));
				 CityData cityData = new CityData();
				 cityData.setId(Id);
				 cityData.setName(RS.getString(2));
				 int CountryId = Integer.parseInt(RS.getString(3));
				 cityData.setCountryId(CountryId);
				 String countryName = RS.getString(4);
				 cityData.setCountryName(countryName);
				 ul.add(cityData);
			 }
		  }catch(Exception e) {
				 e.printStackTrace();
			}finally {
					 try {
						if(RS != null) RS.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						try {
							if(statement != null) statement.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
			 }
		     
		 return ul;
	}
	
	public CityData getCity(String id) {
		CityData cityData = new CityData();
		int Id = Integer.parseInt(id);
		String sql = "select Name,CountryId from cityinfo where Id = "+ Id;
		Statement statement = null;
		ResultSet resultRS = null;
		try {
			statement = database.getConnection().createStatement();
			resultRS = statement.executeQuery(sql);
			if(resultRS.next()) {
			     cityData.setId(Id);
			     cityData.setName(resultRS.getString(1));
			     cityData.setCountryId(resultRS.getInt(2));
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			try {
				if(statement != null) {
					statement.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				if(resultRS != null) {
					resultRS.close();
				}
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		return cityData;
	}
	
	public void updateCity(String id , String name , int countryId) {
		Statement statement = null;
		String sql = null;
		int Id = Integer.parseInt(id);
		System.out.println("Id1: "+Id);
		System.out.println("Name2: "+name);
		System.out.println("CountryId3: "+countryId);
		try {
			statement = database.getConnection().createStatement();
			sql = "update cityinfo set Name = '" + name + "', CountryId = " + countryId + " where Id = " + Id;
			statement.executeUpdate(sql);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			try {
				if(statement != null) {
					statement.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	public void cityDelete(int id) {
		Statement statement = null;
		String sql = null;
		try {
			statement = database.getConnection().createStatement();
			sql = "delete from cityinfo where Id = " + id;
			statement.executeUpdate(sql);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(statement != null) {
					statement.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	public void createCity(String name,int countryId) {
		String sql = null;
		Statement statement = null;
		ResultSet RS = null;
		try {
			statement = database.getConnection().createStatement();
			sql = "insert into cityinfo(Name,CountryId) values ('" + name + "','" + countryId + "')";
			statement.executeUpdate(sql);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(statement != null) {
					statement.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	public List<CountryData> getCountryName() {
		List<CountryData> ul = new ArrayList<CountryData>();
		Statement statement = null;
		String sql = null;
		ResultSet RS = null;
		try {
			statement = database.getConnection().createStatement();
			sql = "select id,name from countryinfo";
			RS = statement.executeQuery(sql);
			while(RS.next()) {
				CountryData countryData = new CountryData();
				countryData.setId(Integer.parseInt(RS.getString(1)));
				countryData.setName(RS.getString(2));
				ul.add(countryData);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			try {
				if(statement != null) {
					statement.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				if(RS != null) {
					RS.close();
				}
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		return ul;
	}
	
	public List<CityData> getCityList(int countryId){
		 List<CityData> ul = new ArrayList<CityData>();
		 Statement statement = null;
		 String sql = null;
		 ResultSet RS = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select Id,Name from cityinfo where CountryId = " + countryId;
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 CityData cityData = new CityData();
				 cityData.setId(Integer.parseInt(RS.getString(1)));
				 cityData.setName(RS.getString(2));
				 ul.add(cityData);
			 }
		 }catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			} finally {
				try {
					if(statement != null) {
						statement.close();
					}
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(RS != null) {
						RS.close();
					}
				}
				catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		 
		 return ul;
	}
}
