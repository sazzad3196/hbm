package com.hbm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Random;

import com.hbm.servlet.BookingData;
import com.hbm.servlet.BookingDetailsData;
import com.hbm.servlet.HotelSearchPageData;


public class BookingDao {
	Database database;
	
	public BookingDao(Database db) {
		database = db;
	}
	
	public void createBooking(int hotelId,int bookedBy,int adults,int kids,String checkIn,String checkOut) {
		  Statement statement = null;
		  String sql = null;
		  Random random = new Random();
		  int invoiceNo = random.nextInt(10000);
		  Date date = new Date();  
		  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		  String bookingDate = formatter.format(date);
		  
		  try {
			   statement = database.getConnection().createStatement();
			   sql = "insert into bookinginfo(hotelId,clientId,adults,kids,status,invoiceNo,checkIn,checkOut,bookingDate,paymentDate) values (" + hotelId + "," + bookedBy + "," + adults + "," + kids + "," + 1 + "," + invoiceNo + ",'" + checkIn + "','" + checkOut + "','" + bookingDate + "','" + null + "')";
			   statement.executeUpdate(sql);
		  }catch (Exception e) {
			// TODO: handle exception
			  e.printStackTrace();
		  }finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		  }  
	}
	
	public BookingData checkBooking(int hotelId,int bookedBy,String checkIn,String checkOut) {
		   BookingData bookingData = null;
		   ResultSet RS = null;
		   Statement statement = null;
		   String sql = null;
		   try {
			    statement = database.getConnection().createStatement();
			    sql = "select * from bookinginfo where hotelId=" + hotelId + " and clientId=" + bookedBy + " and checkIn='" + checkIn + "' and checkOut='" + checkOut + "'";
			    RS = statement.executeQuery(sql);
			    while(RS.next()) {
			    	 bookingData = new BookingData();
			    	 bookingData.setId(Integer.parseInt(RS.getString(1)));
			    	 bookingData.setHotelId(Integer.parseInt(RS.getString(2)));
			    	 bookingData.setBookedBy(Integer.parseInt(RS.getString(3)));
			    	 bookingData.setCheckIn(RS.getString(4));
			    	 bookingData.setCheckOut(RS.getString(5));
			    	 bookingData.setAdults(Integer.parseInt(RS.getString(6)));
			    	 bookingData.setKids(Integer.parseInt(RS.getString(7)));
			    	 bookingData.setInvoiceNo(Integer.parseInt(RS.getString(8)));
			    	 bookingData.setStatus(Integer.parseInt(RS.getString(9)));
			    	 bookingData.setBookingDate(RS.getString(10));
			    	 bookingData.setPaymentDate(RS.getString(11));
			    	 bookingData.setCancelDate(RS.getString(12));
			    }
		   }catch (Exception e) {
			   // TODO: handle exception
			   e.printStackTrace();
		   }finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
	       }  
		   
		   return bookingData;
	}
	
	public void createBookingDetails(int roomId,int bookingId,double rent) {
		   Statement statement = null;
		   String sql = null;
		   try {
			   statement = database.getConnection().createStatement();
			   sql = "insert into bookingdetailsinfo(roomId,bookingId,rent) values(" + roomId + "," + bookingId + "," + rent + ")";
			   statement.executeUpdate(sql);
		   }catch (Exception e) {
			// TODO: handle exception
			   e.printStackTrace();
		   }finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		   }
	}
	
	public BookingDetailsData checkBookingDetails(int bookingId,int roomId) {
		 BookingDetailsData data = null;
		 ResultSet RS = null;
		 Statement statement = null;
		 String sql = null;
		 try {
			  statement = database.getConnection().createStatement();
			  sql = "select * from bookingdetailsinfo where roomId=" + roomId + " and bookingId=" + bookingId;
			  RS = statement.executeQuery(sql);
			  while(RS.next()) {
				  data = new BookingDetailsData();
				  data.setId(Integer.parseInt(RS.getString(1)));
				  data.setRoomId(Integer.parseInt(RS.getString(2)));
				  data.setBookingId(Integer.parseInt(RS.getString(3)));
				  data.setRent(Double.parseDouble(RS.getString(4)));
			  }
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}finally {
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				if(RS != null) RS.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
       } 
		 
	   return data;
	}
	
	public List<BookingData> getAllBooking(){
		 List<BookingData> ul = new ArrayList<BookingData>();
		 ResultSet RS = null;
		 Statement statement = null;
		 String sql = null;
		 try {
			    statement = database.getConnection().createStatement();
			    sql = "select * from bookinginfo";
			    RS = statement.executeQuery(sql);
			    while(RS.next()) {
			    	 BookingData bookingData = new BookingData();
			    	 bookingData.setId(Integer.parseInt(RS.getString(1)));
			    	 bookingData.setHotelId(Integer.parseInt(RS.getString(2)));
			    	 bookingData.setBookedBy(Integer.parseInt(RS.getString(3)));
			    	 bookingData.setCheckIn(RS.getString(4));
			    	 bookingData.setCheckOut(RS.getString(5));
			    	 bookingData.setAdults(Integer.parseInt(RS.getString(6)));
			    	 bookingData.setKids(Integer.parseInt(RS.getString(7)));
			    	 bookingData.setInvoiceNo(Integer.parseInt(RS.getString(8)));
			    	 bookingData.setStatus(Integer.parseInt(RS.getString(9)));
			    	 bookingData.setBookingDate(RS.getString(10));
			    	 bookingData.setPaymentDate(RS.getString(11));
			    	 bookingData.setCancelDate(RS.getString(12));
			    	 ul.add(bookingData);
			    }
		   }catch (Exception e) {
			   // TODO: handle exception
			   e.printStackTrace();
		   }finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
	       }  
		 return ul;
	}
	
	public void updateStatusBookingInfo(int bookingId,String paymentDate) {
		Statement statement = null;
		String sql = null;
		
		try {
			statement = database.getConnection().createStatement();
			sql = "update bookinginfo set status= 2,paymentDate='" + paymentDate + "' where id=" + bookingId;
			statement.executeUpdate(sql);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
       }
	}
	
	public void newUpdateStatusBookingInfo(int bookingId) {
		Statement statement = null;
		String sql = null;
		
		try {
			statement = database.getConnection().createStatement();
			sql = "update bookinginfo set status= 1 where id=" + bookingId;
			statement.executeUpdate(sql);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
       }
	}
	
	public List<BookingData> getBooking(){
		 List<BookingData> ul = new ArrayList<BookingData>();
		 ResultSet RS = null;
		 Statement statement = null;
		 String sql = null;
		 try {
			    statement = database.getConnection().createStatement();
			    sql = "select bookinginfo.*,hotelinfo.hotelName,hotelinfo.address from bookinginfo left outer join hotelinfo on bookinginfo.hotelId = hotelinfo.id where bookinginfo.status = 2 and bookinginfo.paymentDate != 'null' order by bookinginfo.paymentDate desc;";
			    RS = statement.executeQuery(sql);
			    while(RS.next()) {
			    	 ResultSet RS2 = null;
					 Statement statement2 = null;
					 String sql2 = null;
					 int totalRoom = 0;
					 int bookingId = Integer.parseInt(RS.getString(1));
			    	 BookingData bookingData = new BookingData();
			    	 bookingData.setId(bookingId);
			    	 bookingData.setHotelId(Integer.parseInt(RS.getString(2)));
			    	 bookingData.setBookedBy(Integer.parseInt(RS.getString(3)));
			    	 bookingData.setCheckIn(RS.getString(4));
			    	 bookingData.setCheckOut(RS.getString(5));
			    	 try {
						    statement2 = database.getConnection().createStatement();
						    sql2 = "select * from bookingdetailsinfo where bookingId = " + bookingId;
						    RS2 = statement2.executeQuery(sql2);
						    while(RS2.next()) {
						    	totalRoom++;
						    }
						    
			    	 }catch (Exception e) {
						// TODO: handle exception
			    		 e.printStackTrace();
					}finally {
						try {
							if(statement2 != null) statement2.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						try {
							if(RS2 != null) RS2.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
			       }  
			    	 bookingData.setTotalRoom(totalRoom);
			    	 bookingData.setAdults(Integer.parseInt(RS.getString(6)));
			    	 bookingData.setKids(Integer.parseInt(RS.getString(7)));
			    	 bookingData.setInvoiceNo(Integer.parseInt(RS.getString(8)));
			    	 bookingData.setStatus(Integer.parseInt(RS.getString(9)));
			    	 bookingData.setBookingDate(RS.getString(10));
			    	 bookingData.setPaymentDate(RS.getString(11));
			    	 bookingData.setCancelDate(RS.getString(12));
			    	 bookingData.setHotelName(RS.getString(13));
			    	 bookingData.setHotelAddress(RS.getString(14));
			    	 ul.add(bookingData);
			    }
		   }catch (Exception e) {
			   // TODO: handle exception
			   e.printStackTrace();
		   }finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
	       }  
		 return ul;
	}
	
	public void deleteBooking(int bookingId) {
    	Statement statement = null;
  		String sql = null;
  		try {
  			statement = database.getConnection().createStatement();
  			sql = "delete from bookinginfo where id = " + bookingId;
  			statement.executeUpdate(sql);
  		}catch (Exception e) {
  			// TODO: handle exception
  			e.printStackTrace();
  		}finally {
  			try {
  				if(statement != null) {
  					statement.close();
  				}
  			}catch (Exception e) {
  				// TODO: handle exception
  				e.printStackTrace();
  			}
  		}
     }
}
