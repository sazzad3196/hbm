package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.HotelData;
import com.hbm.servlet.RoomData;

public class RoomDao {
	 Database database;
	 public RoomDao(Database db) {
		 database = db;
	 }
	 
	 public List<RoomData> getAllRoomInfo(){
		 List<RoomData> ul = new ArrayList<RoomData>();
		 String sql = null;
		 Statement statement = null;
		 ResultSet RS = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " select roominfo.*,roomtypeinfo.roomType,hotelinfo.hotelName from roominfo left outer join roomtypeinfo on roominfo.roomTypeId = roomtypeinfo.id left outer join hotelinfo on roominfo.hotelId = hotelinfo.id  ";
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 RoomData roomData = new RoomData();
				 roomData.setId(Integer.parseInt(RS.getString(1)));
				 roomData.setRoomNo(RS.getString(2));
				 roomData.setRent(Double.parseDouble(RS.getString(3)));
				 roomData.setRoomTypeId(Integer.parseInt(RS.getString(4)));
				 roomData.setHotelId(Integer.parseInt(RS.getString(5)));
				 roomData.setImage1(RS.getString(6));
				 roomData.setImage2(RS.getString(7));
				 roomData.setImage3(RS.getString(8));
				 roomData.setImage4(RS.getString(9));
				 roomData.setRoomType(RS.getString(10));
				 roomData.setHotelName(RS.getString(11));
				 ul.add(roomData);
			 }
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		  }
		 return ul;
	 }
	 
	 public void createRoom(String roomNo,double rent,int roomTypeId,int hotelId,String image1,String image2,String image3,String image4) {
		 String sql = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "insert into roominfo(roomNo,rent,roomTypeId,hotelId,image1,image2,image3,image4) values('" + roomNo + "'," + rent + "," + roomTypeId + "," + hotelId + ",'" + image1 + "','" + image2 + "','" + image3 + "','" + image4 + "')";
			 statement.executeUpdate(sql);
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		  }
	 }
	 
	 public void deleteRoom(int id) {
		 String sql = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " delete from roominfo where id = " + id;
			 statement.executeUpdate(sql);
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		  }
	 }
	 
	 public RoomData getRoom(int id){
		 RoomData roomData = new RoomData();
		 String sql = null;
		 Statement statement = null;
		 ResultSet RS = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " select * from roominfo where id = " + id;
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 roomData.setId(Integer.parseInt(RS.getString(1)));
				 roomData.setRoomNo(RS.getString(2));
				 roomData.setRent(Double.parseDouble(RS.getString(3)));
				 roomData.setRoomTypeId(Integer.parseInt(RS.getString(4)));
				 roomData.setHotelId(Integer.parseInt(RS.getString(5)));
				 roomData.setImage1(RS.getString(6));
				 roomData.setImage2(RS.getString(7));
				 roomData.setImage3(RS.getString(8));
				 roomData.setImage4(RS.getString(9));
			 }
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		  }
		 return roomData;
	 }
	 
	 public void updateRoom(int id,String roomNo,double rent,int roomTypeId,int hotelId,String image1,String image2,String image3,String image4) {
		 String sql = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " update roominfo set roomNo ='" + roomNo + "',rent =" + rent + ",roomTypeId =" + roomTypeId + ",hotelId = " + hotelId + ",image1 ='" + image1 + "',image2 ='" + image2 + "',image3 ='" + image3 + "',image4 ='" + image4 + "' where id = " + id ;
			 statement.executeUpdate(sql);
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		  }
	 }
	 
	 public List<RoomData> getRoomInfo(){
			List<RoomData> ul = new ArrayList<RoomData>();
			Statement statement = null;
			String sql = null;
			ResultSet RS = null;
			try {
				statement = database.getConnection().createStatement();
				sql = "select id,roomNo from roominfo";
				RS = statement.executeQuery(sql);
				while(RS.next()) {
					RoomData roomData = new RoomData();
					roomData.setId(Integer.parseInt(RS.getString(1)));
					roomData.setRoomNo(RS.getString(2));
					ul.add(roomData);
				}
			}catch(Exception e) {
				 e.printStackTrace();
			}finally {
					 try {
						if(RS != null) RS.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						try {
							if(statement != null) statement.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
			 }
			 
			return ul;
	 }
	 
	public List<RoomData> selectRoom(int hotelId){
		List<RoomData> ul = new ArrayList<RoomData>();
		Statement statement = null;
		String sql = null;
		ResultSet RS = null;
		try {
			statement = database.getConnection().createStatement();
			sql = "select roominfo.*,roomtypeinfo.roomType from roominfo left outer join roomtypeinfo on roominfo.roomTypeId = roomtypeinfo.id where roominfo.hotelId = " + hotelId;
			RS = statement.executeQuery(sql);
			while(RS.next()) {
				RoomData roomData = new RoomData();
				roomData.setId(Integer.parseInt(RS.getString(1)));
				roomData.setRoomNo(RS.getString(2));
				roomData.setRent(RS.getDouble(3));
				roomData.setRoomTypeId(Integer.parseInt(RS.getString(4)));
				roomData.setHotelId(Integer.parseInt(RS.getString(5)));
				roomData.setImage1(RS.getString(6));
				roomData.setImage2(RS.getString(7));
				roomData.setImage3(RS.getString(8));
				roomData.setImage4(RS.getString(9));
				roomData.setRoomType(RS.getString(10));
				ul.add(roomData);
			}
		}catch(Exception e) {
			 e.printStackTrace();
		}finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		 }
			 
		 return ul;
    }
}
