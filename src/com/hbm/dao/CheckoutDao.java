package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.BookingData;
import com.hbm.servlet.BookingDetailsData;
import com.hbm.servlet.CheckoutData;
import com.hbm.servlet.CountryData;
import com.hbm.servlet.RoomData;

public class CheckoutDao {
	Database database;
	
	public CheckoutDao(Database db) {
		 database = db;
	}
	
	public List<CheckoutData> getAllCheckoutInfo(int clientId){
		List<CheckoutData> ul = new ArrayList<CheckoutData>();
		String sql = null;
		Statement statement = null;
		ResultSet RS = null;
		try {
			statement = database.getConnection().createStatement();
			sql = "select bookinginfo.*,hotelinfo.hotelName,hotelinfo.address from bookinginfo left outer join hotelinfo on bookinginfo.hotelId = hotelinfo.id where bookinginfo.clientId=" + clientId + " and bookinginfo.status=1";
			RS = statement.executeQuery(sql);
			while(RS.next()) {
				 CheckoutData checkoutData = new CheckoutData();
				 int bookingId = Integer.parseInt(RS.getString(1));
				 
				 List<BookingDetailsData> ul2 = new ArrayList<BookingDetailsData>();
				 List<RoomData> ul3 = new ArrayList<RoomData>();
				 String sql2 = null;
				 ResultSet RS2 = null;
				 Statement statement2 = null;
				 try {
					 statement2 = database.getConnection().createStatement();
					 sql2 = "select * from bookingdetailsinfo where bookingId=" + bookingId;
					 RS2 = statement2.executeQuery(sql2);	 
					 while(RS2.next()) {
						 BookingDetailsData data = new BookingDetailsData();
						 data.setId(Integer.parseInt(RS2.getString(1)));
						 int roomId = Integer.parseInt(RS2.getString(2));
						 data.setRoomId(roomId);
						 RoomData roomData = new RoomData();
						 String sql3 = null;
						 ResultSet RS3 = null;
						 Statement statement3 = null;
						 
						 try {
							 statement3 = database.getConnection().createStatement();
							 sql3 = "select roominfo.*,roomtypeinfo.roomType from roominfo left outer join roomtypeinfo on roominfo.roomTypeId = roomtypeinfo.id where roominfo.id=" + roomId;
							 RS3 = statement3.executeQuery(sql3);
							 while(RS3.next()) {
								 roomData.setId(Integer.parseInt(RS3.getString(1)));
								 roomData.setRoomNo(RS3.getString(2));
								 roomData.setRent(Double.parseDouble(RS3.getString(3)));
								 roomData.setRoomTypeId(Integer.parseInt(RS3.getString(4)));
								 roomData.setHotelId(Integer.parseInt(RS3.getString(5)));
								 roomData.setImage1(RS3.getString(6));
								 roomData.setImage2(RS3.getString(7));
								 roomData.setImage3(RS3.getString(8));
								 roomData.setImage4(RS3.getString(9));
								 roomData.setRoomType(RS3.getString(10));
							 }
							 
						 }catch (Exception e) {
							// TODO: handle exception
							 e.printStackTrace();
						 }finally {
								try {
									if(RS3 != null) RS3.close();
									}catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
									try {
										if(statement3 != null) statement3.close();
									}catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
						 }
						 data.setRoomData(roomData);
						 data.setBookingId(Integer.parseInt(RS2.getString(3)));
						 data.setRent(Double.parseDouble(RS2.getString(4)));
						 ul2.add(data);
					 }
				 }catch (Exception e) {
					// TODO: handle exception
					 e.printStackTrace();
				 }finally {
						try {
							if(RS2 != null) RS2.close();
							}catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							try {
								if(statement2 != null) statement2.close();
							}catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
				}
				 checkoutData.setBookingDetailsData(ul2);
				 checkoutData.setBookingId(bookingId);
				 checkoutData.setHotelId(Integer.parseInt(RS.getString(2)));
				 checkoutData.setBookedBy(Integer.parseInt(RS.getString(3)));
				 checkoutData.setCheckIn(RS.getString(4));
				 checkoutData.setCheckOut(RS.getString(5));
				 checkoutData.setAdults(Integer.parseInt(RS.getString(6)));
				 checkoutData.setKids(Integer.parseInt(RS.getString(7)));
				 checkoutData.setInvoiceNo(Integer.parseInt(RS.getString(8)));
				 checkoutData.setStatus(Integer.parseInt(RS.getString(9)));
				 checkoutData.setBookingDate(RS.getString(10));
				 checkoutData.setPaymentDate(RS.getString(11));
				 checkoutData.setCancelDate(RS.getString(12));
				 checkoutData.setHotelName(RS.getString(13));
				 checkoutData.setHotelAddress(RS.getString(14));
				 
				 ul.add(checkoutData);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		
		return ul;
	}
	
	public List<CheckoutData> getCheckoutInfo(int bookingId){
		List<CheckoutData> ul = new ArrayList<CheckoutData>();
		String sql = null;
		Statement statement = null;
		ResultSet RS = null;
		try {
			statement = database.getConnection().createStatement();
			sql = "select bookinginfo.*,hotelinfo.hotelName,hotelinfo.address from bookinginfo left outer join hotelinfo on bookinginfo.hotelId = hotelinfo.id where bookinginfo.id=" + bookingId;
			RS = statement.executeQuery(sql);
			while(RS.next()) {
				 CheckoutData checkoutData = new CheckoutData();
				 bookingId = Integer.parseInt(RS.getString(1));
				 
				 List<BookingDetailsData> ul2 = new ArrayList<BookingDetailsData>();
				 List<RoomData> ul3 = new ArrayList<RoomData>();
				 String sql2 = null;
				 ResultSet RS2 = null;
				 Statement statement2 = null;
				 try {
					 statement2 = database.getConnection().createStatement();
					 sql2 = "select * from bookingdetailsinfo where bookingId=" + bookingId;
					 RS2 = statement2.executeQuery(sql2);	 
					 while(RS2.next()) {
						 BookingDetailsData data = new BookingDetailsData();
						 data.setId(Integer.parseInt(RS2.getString(1)));
						 int roomId = Integer.parseInt(RS2.getString(2));
						 data.setRoomId(roomId);
						 RoomData roomData = new RoomData();
						 String sql3 = null;
						 ResultSet RS3 = null;
						 Statement statement3 = null;
						 
						 try {
							 statement3 = database.getConnection().createStatement();
							 sql3 = "select roominfo.*,roomtypeinfo.roomType from roominfo left outer join roomtypeinfo on roominfo.roomTypeId = roomtypeinfo.id where roominfo.id=" + roomId;
							 RS3 = statement3.executeQuery(sql3);
							 while(RS3.next()) {
								 roomData.setId(Integer.parseInt(RS3.getString(1)));
								 roomData.setRoomNo(RS3.getString(2));
								 roomData.setRent(Double.parseDouble(RS3.getString(3)));
								 roomData.setRoomTypeId(Integer.parseInt(RS3.getString(4)));
								 roomData.setHotelId(Integer.parseInt(RS3.getString(5)));
								 roomData.setImage1(RS3.getString(6));
								 roomData.setImage2(RS3.getString(7));
								 roomData.setImage3(RS3.getString(8));
								 roomData.setImage4(RS3.getString(9));
								 roomData.setRoomType(RS3.getString(10));
							 }
							 
						 }catch (Exception e) {
							// TODO: handle exception
							 e.printStackTrace();
						 }finally {
								try {
									if(RS3 != null) RS3.close();
									}catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
									try {
										if(statement3 != null) statement3.close();
									}catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
						 }
						 data.setRoomData(roomData);
						 data.setBookingId(Integer.parseInt(RS2.getString(3)));
						 data.setRent(Double.parseDouble(RS2.getString(4)));
						 ul2.add(data);
					 }
				 }catch (Exception e) {
					// TODO: handle exception
					 e.printStackTrace();
				 }finally {
						try {
							if(RS2 != null) RS2.close();
							}catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							try {
								if(statement2 != null) statement2.close();
							}catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
				}
				 checkoutData.setBookingDetailsData(ul2);
				 checkoutData.setBookingId(bookingId);
				 checkoutData.setHotelId(Integer.parseInt(RS.getString(2)));
				 checkoutData.setBookedBy(Integer.parseInt(RS.getString(3)));
				 checkoutData.setCheckIn(RS.getString(4));
				 checkoutData.setCheckOut(RS.getString(5));
				 checkoutData.setAdults(Integer.parseInt(RS.getString(6)));
				 checkoutData.setKids(Integer.parseInt(RS.getString(7)));
				 checkoutData.setInvoiceNo(Integer.parseInt(RS.getString(8)));
				 checkoutData.setStatus(Integer.parseInt(RS.getString(9)));
				 checkoutData.setBookingDate(RS.getString(10));
				 checkoutData.setPaymentDate(RS.getString(11));
				 checkoutData.setCancelDate(RS.getString(12));
				 checkoutData.setHotelName(RS.getString(13));
				 checkoutData.setHotelAddress(RS.getString(14));
				 
				 ul.add(checkoutData);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		
		return ul;
	}
}
