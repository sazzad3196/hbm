package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.CountryData;

public class CountryDao {
	Database database = null;
	public CountryDao(Database db) {
		database = db;
	}
	public List<CountryData> getAllCountrys(){
		String sql = null;
		Statement statement = null;
		ResultSet RS = null;
		List <CountryData> ul = new ArrayList<CountryData>();
		try {
			statement = database.getConnection().createStatement();
			sql = "select * from countryinfo";
			RS = statement.executeQuery(sql);
			while(RS.next()) {
				int id = Integer.parseInt(RS.getString(1));
				CountryData countryData = new CountryData();
				countryData.setId(id);
				countryData.setName(RS.getString(2));
				ul.add(countryData);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
		}
		return ul;
	}
	
	public void createCountry(String name) {
		 String sql = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "insert into countryinfo(name) values ('" + name + "')";
			 statement.executeUpdate(sql);
		 }catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}finally {
				try {
					if(statement != null) {
						statement.close();
					}
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
	}
	
	public void updateCountry(String id,String name) {
		 String sql = null;
		 Statement statement = null;
		 ResultSet resultRS = null;
		 int Id = Integer.parseInt(id);
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select * from countryinfo";
			 resultRS = statement.executeQuery(sql);
			 if(resultRS.next()) {
				 String sql1 = "update countryinfo set name = '" + name + "' where id = "+Id;
				 statement.executeUpdate(sql1);
			 }
		 }catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}finally {
				try {
					if(statement != null) {
						statement.close();
					}
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
	}
	
	public CountryData getCountry(String id) {
		CountryData countryData = new CountryData();
		int Id = Integer.parseInt(id);
		Statement statement = null;
		String sql = null;
		ResultSet rs = null;
		try {
			statement = database.getConnection().createStatement();
			sql = "select * from countryinfo where id ="+ Id;
			rs = statement.executeQuery(sql);
			while(rs.next()) {
				countryData.setName(rs.getString(2));
				countryData.setId(Id);
			}
		}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			} finally {
				try {
					if(statement != null) {
						statement.close();
					}
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(rs != null) {
						rs.close();
					}
				}
				catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
			
		return countryData;
	}
	
	public void countryDelete(int id) {
		Statement statement = null;
		String sql = null;
		try {
		    statement = database.getConnection().createStatement();
		    sql = " delete from countryinfo where id = " + id;
		    statement.executeUpdate(sql); 
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(statement != null) {
					statement.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
		
}
