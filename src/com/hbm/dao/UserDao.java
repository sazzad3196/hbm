package com.hbm.dao;

import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.hbm.servlet.UserData;

public class UserDao {
	
	Database database = null;
	
	public UserDao(Database database) {
		this.database = database;
	}

	public List<UserData> getAllUsers(){	
		List<UserData> ul=new ArrayList<UserData>();
		String sql=null;
		ResultSet resultRS=null;
		Statement statement = null;
		try{	
		   statement=this.database.getConnection().createStatement();
		   sql="select Id,Name,Email,PhoneNo,UserType,IsActive from userinfo";
		   resultRS=statement.executeQuery(sql);
		   while(resultRS.next()) {
			   int Id=Integer.parseInt(resultRS.getString(1));
			   UserData userData=new UserData();
			   userData.setId(Id);
			   userData.setName(resultRS.getString(2));
			   userData.setEmail(resultRS.getString(3));
			   userData.setPhoneNo(resultRS.getString(4));
			   userData.setUserType(Integer.parseInt(resultRS.getString(5)));
			   userData.setActive(true);
			   ul.add(userData);
		   }
		}
		catch(Exception e){
			e.printStackTrace();
		}finally {
			try {
				if(resultRS != null) resultRS.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		return ul;
	}
	
	public UserData getUser(String id) {
		String sql=null;
		Statement statement = null;
		ResultSet resultRS=null;
		UserData userData=new UserData();
		try {
		   statement=this.database.getConnection().createStatement();
		   sql="select Name,Email,Password,PhoneNo from userinfo where Id="+id;
		   int Id=Integer.parseInt(id);
		   resultRS =statement.executeQuery(sql);
		   if(resultRS.next()) {
			   userData.setId(Id);
			   userData.setName(resultRS.getString(1));
			   userData.setEmail(resultRS.getString(2));
			   userData.setPassword(resultRS.getString(3));
			   userData.setPhoneNo(resultRS.getString(4));
		   }
		   
		}
		catch(Exception e){
			e.printStackTrace();
		} finally {
			try {
				if(resultRS != null) resultRS.next();
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		return userData;
	}
	
	public void deleteUser(String id) {
		String sql=null;
		Statement statement = null;
		try {
		    statement=this.database.getConnection().createStatement();
		    sql="delete from userinfo where Id="+id;
		    statement.executeUpdate(sql);
		}
		catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	
	public void updateUser(int id , String name , String email , String password , String phoneNo) {
		
		String sql=null;
		Statement statement = null;
		ResultSet resultRS =null;
		try {
			sql="select * from userinfo";
		    statement=this.database.getConnection().createStatement();
		    resultRS=statement.executeQuery(sql);
		    while(resultRS.next()) {
		    	if(id == resultRS.getInt(1)) {
		    		String sq="update userinfo set Name='"+name+"',Email='"+email+"',Password='"+password+"',PhoneNo='"+phoneNo+"' where Id="+id;
		    		statement.executeUpdate(sq);
		    	}
		    }
		}
		catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(resultRS != null) resultRS.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	public void createUser(String name , String email , String password , String phoneNo) {
		
		 String sql=null;
		 Statement statement = null;
		 sql="insert into userinfo(Name, Email, PhoneNo, Password, UserType, IsActive) "
		 		+ "values ('" +name+ "','" +email+ "','" +phoneNo+ "','" +password+ "', 1, 1)";
		 try {
			 statement=this.database.getConnection().createStatement();
		     statement.executeUpdate(sql);
		 }
		 catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		  }  finally {
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		  }	
	}
	
	public UserData adminLogin(String userName , String password) {
		String sql;
		Statement statement = null;
		UserData userData = new UserData();
		sql=" select * from userinfo where Name='" + userName + "' and Password='"+ password +"' ";
		try {
			 statement=this.database.getConnection().createStatement();
		     ResultSet resultRS=statement.executeQuery(sql);
		     if(resultRS.next()) {
		    	 userData.setId(resultRS.getInt(1));
		    	 userData.setName(resultRS.getString(2));
		    	 userData.setEmail(resultRS.getString(3));
		    	 userData.setPhoneNo(resultRS.getString(4));
		    	 userData.setPassword(resultRS.getString(5));
		    	 userData.setUserType(resultRS.getInt(6));
		    	 userData.setActive(resultRS.getBoolean(7));
		     } else {
		    	 userData = null;
		     }
		}
		catch (Exception e) {
			// TODO: handle exceptio
			userData = null;
			e.printStackTrace();
		}
		
   	    return userData;
	}
	

}
