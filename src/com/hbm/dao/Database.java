package com.hbm.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class Database {

	private Connection connection = null;
	private String databaseDriver = null;
	private String databaseName = null;
	private String username = null;
	private String password = null;
	
	public Database(String databaseDriver , String databaseName , String username , String password) {
		this.databaseDriver = databaseDriver;
		this.databaseName = databaseName;
		this.username = username;
		this.password = password;
	}
	
	public void open() {
		try {
			 Class.forName(databaseDriver);
		     connection = DriverManager.getConnection(databaseName,username,password);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			if(connection != null) connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Connection getConnection() {
		return connection;
	}
	
}
