package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.RoomTypeData;

public class RoomTypeDao {
	 Database database;
	 public RoomTypeDao(Database db) {
		 database = db;
	 }
	 
	 public List<RoomTypeData> getAllRoomType(){
		 List<RoomTypeData> ul = new ArrayList<RoomTypeData>();
		 String sql = null;
		 ResultSet RS = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select * from roomtypeinfo";
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 RoomTypeData roomTypeData = new RoomTypeData();
				 roomTypeData.setId(Integer.parseInt(RS.getString(1)));
				 roomTypeData.setRoomType(RS.getString(2));
				 ul.add(roomTypeData);
			 }
		 }catch(Exception e){
				e.printStackTrace();
			}finally {
				try {
					if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		 
		 return ul;
	 }
	 
	 public void createRoomType(String roomType) {
		 String sql = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " insert into roomtypeinfo(roomType) values('" + roomType + "') ";
			 statement.executeUpdate(sql);
		 }catch(Exception e){
				e.printStackTrace();
			}finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
	 }
	 
	 public RoomTypeData getRoomType(int id) {
		 RoomTypeData roomTypeData = new RoomTypeData();
		 String sql = null;
		 ResultSet RS = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select * from roomtypeinfo where id = " + id;
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 roomTypeData.setId(Integer.parseInt(RS.getString(1)));
				 roomTypeData.setRoomType(RS.getString(2));
			 } 
		 }catch(Exception e){
				e.printStackTrace();
			}finally {
				try {
					if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		 
		 return roomTypeData;
	 }
	 
	 public void updateRoomType(int id,String roomType) {
		 String sql = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " update roomtypeinfo set roomType='" + roomType + "' where id=" + id;
			 statement.executeUpdate(sql);
		 }catch(Exception e){
				e.printStackTrace();
			}finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
	 }
	 
	 public void deleteRoomType(int id) {
		 String sql = null;
		 Statement statement = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "delete from roomtypeinfo where id = " + id ;
			 statement.executeUpdate(sql);
		 }catch(Exception e){
				e.printStackTrace();
			}finally {
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
	 }
}
