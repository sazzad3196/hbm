package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.HotelData;
import com.hbm.servlet.RoomWiseFacilityData;

public class RoomWiseFacilityDao {
	  Database database;
	  public RoomWiseFacilityDao(Database db) {
		   database = db;
	  }
	  
	  public List<RoomWiseFacilityData> getAllRoomWiseFacility() {
			 List<RoomWiseFacilityData> ul = new ArrayList<RoomWiseFacilityData>();
			 String sql = null;
			 Statement statement = null;
			 ResultSet RS = null;
			 try {
				 statement = database.getConnection().createStatement();
				 sql = " select roomwisefacilityinfo.*,roominfo.roomNo,roomfacilityinfo.roomFacility,hotelinfo.hotelName from roomwisefacilityinfo left outer join roominfo on roomwisefacilityinfo.roomId = roominfo.id"
				 		+ " left outer join roomfacilityinfo on roomwisefacilityinfo.roomFacilityId = roomfacilityinfo.id left outer join hotelinfo on roomwisefacilityinfo.hotelId = hotelinfo.id";
				 RS = statement.executeQuery(sql);
				 while(RS.next()) {
					 RoomWiseFacilityData room = new RoomWiseFacilityData();
					 room.setId(Integer.parseInt(RS.getString(1)));
					 room.setRoomId(Integer.parseInt(RS.getString(2)));
					 room.setRoomFacilityId(Integer.parseInt(RS.getString(3)));
					 room.setHotelId(Integer.parseInt(RS.getString(4)));
					 room.setRoomNumber(RS.getString(5));
					 room.setRoomFacility(RS.getString(6));
					 room.setHotelName(RS.getString(7));
					 ul.add(room);
				 }
			 }catch(Exception e) {
				 e.printStackTrace();
			}finally {
					 try {
						if(RS != null) RS.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						try {
							if(statement != null) statement.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
			 }
			 
			 return ul;
		}
	  
	   public void createRoomWiseFacility(int hotelId,int roomId,int roomFacilityId) {
		   String sql = null;
		   Statement statement = null;
		   
		   try {
			   statement = database.getConnection().createStatement();
			   sql = " insert into roomwisefacilityinfo(roomId,hotelId,roomFacilityId) values(" + roomId + "," + hotelId + "," + roomFacilityId + ")";
			   statement.executeUpdate(sql);
		   }catch(Exception e) {
				 e.printStackTrace();
			}finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
			 }
	   }
	   
	   public void deleteRoomWiseFacility(int id) {
		   String sql = null;
		   Statement statement = null;
		   
		   try {
			   statement = database.getConnection().createStatement();
			   sql = " delete from roomwisefacilityinfo where id = " + id;
			   statement.executeUpdate(sql);
		   }catch(Exception e) {
				 e.printStackTrace();
			}finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
			 }
	   }
	   
	   public void updateRoomWiseFacility(int id,int hotelId,int roomId,int roomFacilityId) {
		   String sql = null;
		   Statement statement = null;
		   
		   try {
			   statement = database.getConnection().createStatement();
			   sql = " update roomwisefacilityinfo set hotelId= " + hotelId + ",roomId= " + roomId + ",roomFacilityId =" + roomFacilityId + " where id = " + id;
			   statement.executeUpdate(sql);
		   }catch(Exception e) {
				 e.printStackTrace();
			}finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
			 }
	   }
	   
	   public RoomWiseFacilityData getRoomWiseFacility(int id) {
		     RoomWiseFacilityData room = new RoomWiseFacilityData();
		     String sql = null;
		     Statement statement = null;
			 ResultSet RS = null;
			 try {
				 statement = database.getConnection().createStatement();
				 sql = "select * from roomwisefacilityinfo where id = " + id;
				 RS = statement.executeQuery(sql);
				 while(RS.next()) {
					 room.setId(Integer.parseInt(RS.getString(1)));
					 room.setRoomId(Integer.parseInt(RS.getString(2)));
					 room.setRoomFacilityId(Integer.parseInt(RS.getString(3)));
					 room.setHotelId(Integer.parseInt(RS.getString(4)));
				 }
			 }catch(Exception e) {
				 e.printStackTrace();
			}finally {
					 try {
						if(RS != null) RS.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						try {
							if(statement != null) statement.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
			 }
		   
		   return room;
	   }
	   
	   
}
