package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.CityData;
import com.hbm.servlet.CountryData;
import com.hbm.servlet.HotelData;

public class HotelDao {
	Database database = null;
	public HotelDao(Database db) {
		 database = db;
	}
	
	public List<HotelData> getAllHotels() {
		 List<HotelData> ul = new ArrayList<HotelData>();
		 String sql = null;
		 Statement statement = null;
		 ResultSet RS = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " select hotelinfo.*,cityinfo.Name,countryinfo.name from hotelinfo left outer join cityinfo on hotelinfo.city = cityinfo.Id"
			 		+ " left outer join countryinfo on hotelinfo.country = countryinfo.id ";
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 HotelData hotelData = new HotelData();
				 hotelData.setId(Integer.parseInt(RS.getString(1)));
				 hotelData.setHotelName(RS.getString(2));
				 hotelData.setEmail(RS.getString(3));
				 hotelData.setPhoneNo(RS.getString(4));
				 hotelData.setAddress(RS.getString(5));
				 hotelData.setCheckIn(RS.getString(6));
				 hotelData.setCheckOut(RS.getString(7));
				 hotelData.setCountryId(Integer.parseInt(RS.getString(9)));
				 hotelData.setCityId(Integer.parseInt(RS.getString(8)));
				 hotelData.setStarRating(Integer.parseInt(RS.getString(10)));
				 hotelData.setImage1(RS.getString(11));
				 hotelData.setImage2(RS.getString(12));
				 hotelData.setImage3(RS.getString(13));
				 hotelData.setImage4(RS.getString(14));
				 hotelData.setCity(RS.getString(15));
				 hotelData.setCountry(RS.getString(16));
				 ul.add(hotelData);
			 }
		 }catch(Exception e) {
			 e.printStackTrace();
		}finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		 }
		 
		 return ul;
	}
	public List<CityData> getCity(int countryId){
		List<CityData> ul = new ArrayList<CityData>();
		String sql = null;
		Statement statement = null;
		ResultSet RS = null;
		try {
			statement = database.getConnection().createStatement();
			sql = "select Id,Name from cityinfo where CountryId = " + countryId;
			RS = statement.executeQuery(sql);
			while(RS.next()) {
				CityData cityData = new CityData();
				cityData.setId(Integer.parseInt(RS.getString(1)));
				cityData.setName(RS.getString(2));
				ul.add(cityData);
			}
		}catch(Exception e) {
			 e.printStackTrace();
		}finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		 }
		 
		return ul;
	}
	
	public void hotelDelete(int id) {
		 Statement statement = null;
		 String sql = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " delete from hotelinfo where id = '" + id + "'";
			 statement.executeUpdate(sql);
		 }catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(statement != null) statement.close();
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
	
	public void createHotel(int countryId,int cityId,int starRating,String hotelName,String email,String phoneNo,String address,String checkIn,String checkOut,String image1,String image2,String image3,String image4) {
			Statement statement = null;
			String sql = null;
			try {
				statement = database.getConnection().createStatement();
	            sql = "Insert into hotelinfo(hotelName,email,phoneNo,address,country,city,starRating,checkIn,checkOut,image1,image2,image3,image4) values ('" + hotelName +
	                  "','" + email +  "','" + phoneNo + "','" + address + "','" + countryId + "','" + cityId + "','" + starRating + "','" + checkIn + "','" + checkOut + "','" + image1 + "','" + image2 + "','" + image3 + "','" + image4 + "')";
	            statement.executeUpdate(sql);
			}catch(Exception e) {
				 e.printStackTrace();
			}finally {
						try {
							if(statement != null) statement.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
			 }
			 
	}
	
	public HotelData getHotel(int id) {
		Statement statement = null;
		String sql = null;
		ResultSet RS = null;
		HotelData hotelData = new HotelData();
		try {
			statement = database.getConnection().createStatement();
			sql = "select * from hotelinfo where id = " + id;
			RS = statement.executeQuery(sql);
			while(RS.next()) {
				 hotelData.setId(Integer.parseInt(RS.getString(1)));
				 hotelData.setHotelName(RS.getString(2));
				 hotelData.setEmail(RS.getString(3));
				 hotelData.setPhoneNo(RS.getString(4));
				 hotelData.setAddress(RS.getString(5));
				 hotelData.setCheckIn(RS.getString(6));
				 hotelData.setCheckOut(RS.getString(7));
				 hotelData.setCountryId(Integer.parseInt(RS.getString(9)));
				 hotelData.setCityId(Integer.parseInt(RS.getString(8)));
				 hotelData.setStarRating(Integer.parseInt(RS.getString(10)));
				 hotelData.setImage1(RS.getString(11));
				 hotelData.setImage2(RS.getString(12));
				 hotelData.setImage3(RS.getString(13));
				 hotelData.setImage4(RS.getString(14));
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return hotelData;
	}
	
	
	public void updateHotel(int id,int countryId,int cityId,int starRating,String hotelName,String email,String phoneNo,String address,String checkIn,String checkOut,String image1,String image2,String image3,String image4) {
		Statement statement = null;
		String sql = null;
		try {
			statement = database.getConnection().createStatement();
			sql = " update hotelinfo set hotelName = '" + hotelName + "', email = '" + email + "', phoneNo = '" + phoneNo + "', address = '" + address + "', checkIn = '" + checkIn + "', checkOut ='" + checkOut + "', city =" + cityId + ", country = " + countryId + ", starRating = " + starRating + ", image1 = '" + image1 + "', image2 = '" + image2 + "', image3 = '" + image3 + "', image4 = '" + image4 + "' where id =" + id;
			statement.executeUpdate(sql);
		}catch(Exception e) {
			 e.printStackTrace();
		}finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		 }
	}
	
	public List<CountryData> getCountry(){
		List<CountryData> ul = new ArrayList<CountryData>();
		Statement statement = null;
		String sql = null;
		ResultSet RS = null;
		try {
			statement = database.getConnection().createStatement();
			sql = "select * from countryinfo";
			RS = statement.executeQuery(sql);
			while(RS.next()) {
				CountryData countryData = new CountryData();
				countryData.setId(Integer.parseInt(RS.getString(1)));
				countryData.setName(RS.getString(2));
				ul.add(countryData);
			}
		}catch(Exception e) {
			 e.printStackTrace();
		}finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		 }
		
		return ul;
	}
	
	public List<HotelData> getHotelInfo(){
		List<HotelData> ul = new ArrayList<HotelData>();
		Statement statement = null;
		String sql = null;
		ResultSet RS = null;
		try {
			statement = database.getConnection().createStatement();
			sql = "select id,hotelName from hotelinfo";
			RS = statement.executeQuery(sql);
			while(RS.next()) {
				HotelData hotelData = new HotelData();
				hotelData.setId(Integer.parseInt(RS.getString(1)));
				hotelData.setHotelName(RS.getString(2));
				ul.add(hotelData);
			}
		}catch(Exception e) {
			 e.printStackTrace();
		}finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		 }
		
		return ul;
	}
	
	  public List<HotelData> selectHotelName(int countryId,int cityId){
		    List<HotelData> ul = new ArrayList<HotelData>();
		    Statement statement = null;
			String sql = null;
			ResultSet RS = null;
			try {
				statement = database.getConnection().createStatement();
				sql = "select id,hotelName from hotelinfo where country =" + countryId + " and city=" + cityId;
				RS = statement.executeQuery(sql);
				while(RS.next()) {
					HotelData hotelData = new HotelData();
					hotelData.setId(Integer.parseInt(RS.getString(1)));
					hotelData.setHotelName(RS.getString(2));
					ul.add(hotelData);
				}
			}catch(Exception e) {
				 e.printStackTrace();
			}finally {
					 try {
						if(RS != null) RS.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						try {
							if(statement != null) statement.close();
						}catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
			 }
			
		 return ul;
	  }

	
	
}
