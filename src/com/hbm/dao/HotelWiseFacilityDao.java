package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.HotelWiseFacilityData;
import com.hbm.servlet.RoomWiseFacilityData;

public class HotelWiseFacilityDao {
	 Database database;
	 
	 public HotelWiseFacilityDao(Database db) {
		  database = db;
	 }
	 
	 public List<HotelWiseFacilityData> getAllHotelWiseFacility() {
		 List<HotelWiseFacilityData> ul = new ArrayList<HotelWiseFacilityData>();
		 String sql = null;
		 Statement statement = null;
		 ResultSet RS = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = " select hotelwisefacilityinfo.*,hotelinfo.hotelName,hotelfacilityinfo.hotelFacility from hotelwisefacilityinfo left outer join hotelinfo on hotelwisefacilityinfo.hotelId = hotelinfo.id"
			 		+ " left outer join hotelfacilityinfo on hotelwisefacilityinfo.hotelFacilityId = hotelfacilityinfo.id ";
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 HotelWiseFacilityData hotel = new HotelWiseFacilityData();
				 hotel.setId(Integer.parseInt(RS.getString(1)));
				 hotel.setHotelId(Integer.parseInt(RS.getString(3)));
				 hotel.setHotelFacilityId(Integer.parseInt(RS.getString(2)));
				 hotel.setHotelName(RS.getString(4));
				 hotel.setHotelFacility(RS.getString(5)); 
				 ul.add(hotel);
			 }
		 }catch(Exception e) {
			 e.printStackTrace();
		}finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		 }
		 
		 return ul;
	}
	 
	 public void createHotelWiseFacility(int hotelId,int hotelFacilityId) {
		   String sql = null;
		   Statement statement = null;
		   
		   try {
			   statement = database.getConnection().createStatement();
			   sql = " insert into hotelwisefacilityinfo(hotelId,hotelFacilityId) values(" + hotelId + "," + hotelFacilityId + ")";
			   statement.executeUpdate(sql);
		   }catch(Exception e) {
				 e.printStackTrace();
			}finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
			 }
	   }
	 
	 public void deleteHotelWiseFacility(int id) {
		   String sql = null;
		   Statement statement = null;
		   
		   try {
			   statement = database.getConnection().createStatement();
			   sql = " delete from hotelwisefacilityinfo where id = " + id;
			   statement.executeUpdate(sql);
		   }catch(Exception e) {
				 e.printStackTrace();
			}finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
			 }
	   }
	 
	 public HotelWiseFacilityData getHotelWiseFacility(int id) {
		 HotelWiseFacilityData hotel = new HotelWiseFacilityData();
	     String sql = null;
	     Statement statement = null;
		 ResultSet RS = null;
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select * from hotelwisefacilityinfo where id = " + id;
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 hotel.setId(Integer.parseInt(RS.getString(1)));
				 hotel.setHotelId(Integer.parseInt(RS.getString(3)));
				 hotel.setHotelFacilityId(Integer.parseInt(RS.getString(2)));
			 }
		 }catch(Exception e) {
			 e.printStackTrace();
		}finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		 }
	   
	   return hotel;
   }
	 
	 
	 public void updateHotelWiseFacility(int id,int hotelId,int hotelFacilityId) {
		   String sql = null;
		   Statement statement = null;
		   
		   try {
			   statement = database.getConnection().createStatement();
			   sql = " update hotelwisefacilityinfo set hotelId= " + hotelId + ",hotelFacilityId =" + hotelFacilityId + " where id = " + id;
			   statement.executeUpdate(sql);
		   }catch(Exception e) {
				 e.printStackTrace();
			}finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
			 }
	   }
	 
	   public List<HotelWiseFacilityData> HotelWiseFacility(int hotelId){
		   List<HotelWiseFacilityData> ul = new ArrayList<HotelWiseFacilityData>();
		   Statement statement = null;
		   String sql = null;
		   ResultSet RS = null;
		   try {
			    statement = database.getConnection().createStatement();
			    sql = " select * from hotelwisefacilityinfo where hotelId = " + hotelId;
			    RS = statement.executeQuery(sql);
			    while(RS.next()) {
			    	 HotelWiseFacilityData hotelWiseFacilityData = new HotelWiseFacilityData();
			    	 hotelWiseFacilityData.setId(Integer.parseInt(RS.getString(1)));
			    	 hotelWiseFacilityData.setHotelFacilityId(Integer.parseInt(RS.getString(2)));
			    	 ul.add(hotelWiseFacilityData);
			    }
		   }catch (Exception e) {
			// TODO: handle exception
			   e.printStackTrace();
		   }finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		 }
		   return ul;
	   }
	 
}
