package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.HotelFacilityData;
import com.hbm.servlet.RoomFacilityData;

public class HotelFacilityDao {
	  Database database;
	  public HotelFacilityDao(Database db) {
		   database = db;
	  }
	  
	  public List<HotelFacilityData> getAllHotelFacility(){
		  List<HotelFacilityData> ul = new ArrayList<HotelFacilityData>();
		  String sql = null;
		  Statement statement = null;
		  ResultSet RS = null;
		  try {
			 statement = database.getConnection().createStatement();
			 sql = " select * from hotelfacilityinfo ";
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 HotelFacilityData hotelData = new HotelFacilityData();
				 hotelData.setId(Integer.parseInt(RS.getString(1)));
				 hotelData.setHotelFacility(RS.getString(2));
				 ul.add(hotelData);
			 }
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		  }
		  return ul;
	 }
	  
	  public void createHotelFacility(String hotelFacility) {
			 String sql = null;
			 Statement statement = null;
			 try {
				 statement = database.getConnection().createStatement();
				 sql = " insert into hotelfacilityinfo(hotelFacility) values ('" + hotelFacility + "')";
				 statement.executeUpdate(sql);
			 }catch(Exception e) {
				 e.printStackTrace();
			  }finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
			  }
		 }
	  
	  public void deleteHotelFacility(int id) {
			 String sql = null;
			 Statement statement = null;
			 try {
				 statement = database.getConnection().createStatement();
				 sql = " delete from hotelfacilityinfo where id= " + id;
				 statement.executeUpdate(sql);
			 }catch(Exception e) {
				 e.printStackTrace();
			  }finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
			  }
		 }
	  
	  public HotelFacilityData getHotelFacility(int id) {
		  String sql = null;
		  Statement statement = null;
		  ResultSet RS = null;
		  HotelFacilityData hotelData = new HotelFacilityData();
		  try {
			 statement = database.getConnection().createStatement();
			 sql = " select * from hotelfacilityinfo where id = " + id;
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 hotelData.setId(Integer.parseInt(RS.getString(1)));
				 hotelData.setHotelFacility(RS.getString(2));
			 }
		 }catch(Exception e) {
			 e.printStackTrace();
		  }finally {
				 try {
					if(RS != null) RS.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		  }
		  
		  return hotelData;
	 }
	  
	  public void updateHotelFacility(int id,String hotelFacility) {
			 String sql = null;
			 Statement statement = null;
			 try {
				 statement = database.getConnection().createStatement();
				 sql = " update hotelfacilityinfo set hotelFacility='" + hotelFacility + "' where id= " + id;
				 statement.executeUpdate(sql);
			 }catch(Exception e) {
				 e.printStackTrace();
			  }finally {
					try {
						if(statement != null) statement.close();
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
			  }
	  }
}
