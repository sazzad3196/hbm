package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;

import com.hbm.servlet.ClientData;

public class ClientLoginDao {
	Database database; 
	public ClientLoginDao(Database db) {
		database = db;
	}
	
	public ClientData doSingIn(String userName , String password) {
		 Statement statement = null;
		 ResultSet RS = null;
		 String sql = null;
		 ClientData clientLoginData = null;
		 //System.out.println("userName2: " + userName);
		 try {
			 statement = database.getConnection().createStatement();
			 sql = "select * from clientinfo where name='" + userName + "' and password='" + password + "'";
			 RS = statement.executeQuery(sql);
			 while(RS.next()) {
				 clientLoginData = new ClientData();
				 clientLoginData.setId(Integer.parseInt(RS.getString(1)));
				 clientLoginData.setName(RS.getString(2));
				 clientLoginData.setEmail(RS.getString(3));
				 clientLoginData.setPassword(RS.getString(4));
				 clientLoginData.setGender(RS.getString(5));
				 clientLoginData.setAge(Integer.parseInt(RS.getString(6)));
				 clientLoginData.setPhoneNo(RS.getString(7));	 
			 }
			 
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		} finally {
			try {
				if(statement != null) {
					statement.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			try {
				if(RS != null) {
					RS.close();
				}
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		 
		return clientLoginData; 
	}
	
	public void doRegister(String userName,String password,String phoneNo,String email,String gender,int age) {
		Statement statement = null;
		String sql = null;
		
		try {
			 statement = database.getConnection().createStatement();
			 sql = "insert into clientinfo (name,email,phoneNo,password,gender,age) values('" + userName + "','" + email + "','" + phoneNo + "','" + password + "','" + gender + "'," + age + ")";
			 statement.executeUpdate(sql);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			try {
				if(statement != null) {
					statement.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
