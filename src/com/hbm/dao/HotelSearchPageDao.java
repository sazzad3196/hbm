package com.hbm.dao;

import java.util.Date;

import com.hbm.servlet.HotelSearchPageData;

public class HotelSearchPageDao {
	  Database database;
	  public HotelSearchPageDao(Database db) {
		  database = db;
	  }
	  
	  public HotelSearchPageData setHotelSearchInfo(String checkInDate,String checkOutDate,int cityId,int adults,int kids) {
		   HotelSearchPageData page = new HotelSearchPageData();
		   page.setCheckInData(checkInDate);
		   page.setCheckOutData(checkOutDate);
		   page.setCityId(cityId);
		   page.setAdults(adults);
		   page.setKids(kids);
		   return page;
	  }
}
