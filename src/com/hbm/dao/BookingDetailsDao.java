package com.hbm.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hbm.servlet.BookingDetailsData;

public class BookingDetailsDao {
      Database database = null;
      
      public BookingDetailsDao(Database db){
    	  database = db;
      }
      
     public void deleteBookingDetails(int bookingDetailsId) {
    	Statement statement = null;
  		String sql = null;
  		try {
  			statement = database.getConnection().createStatement();
  			sql = "delete from bookingdetailsinfo where id = " + bookingDetailsId;
  			statement.executeUpdate(sql);
  		}catch (Exception e) {
  			// TODO: handle exception
  			e.printStackTrace();
  		}finally {
  			try {
  				if(statement != null) {
  					statement.close();
  				}
  			}catch (Exception e) {
  				// TODO: handle exception
  				e.printStackTrace();
  			}
  		}
     }
     
     public List<BookingDetailsData> getBookingDetails(int bookingId){
    	 Statement statement = null;
   		 String sql = null;
   		 ResultSet RS = null;
   		 int count = 0; 
   		 List<BookingDetailsData> ul = new ArrayList<BookingDetailsData>();
   		 try {
   			statement = database.getConnection().createStatement();
  			sql = "select * from bookingdetailsinfo where bookingId= " + bookingId;
  			RS = statement.executeQuery(sql);
  			if(RS == null) {
  				 return ul = null;
  			}else {
  			while(RS.next()) {
  				 BookingDetailsData bookingDetailsData = new BookingDetailsData();
  				 bookingDetailsData.setId(Integer.parseInt(RS.getString(1)));
  				 bookingDetailsData.setRoomId(Integer.parseInt(RS.getString(2)));
  				 bookingDetailsData.setBookingId(Integer.parseInt(RS.getString(3)));
  				 bookingDetailsData.setRent(Double.parseDouble(RS.getString(4)));
  				 ul.add(bookingDetailsData);
  				 count++;
  			}}
   		 }catch (Exception e) {
			// TODO: handle exception
   			 e.printStackTrace();
		}finally {
			try {
				if(RS != null) RS.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				try {
					if(statement != null) statement.close();
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
	    }
   		
   		if(count == 0) {
   			return ul = null;
   		}
   		else {
   		   return ul; 
   		}
     }
}
