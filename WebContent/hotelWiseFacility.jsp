<%@page import="com.hbm.servlet.HotelFacilityData"%>
<%@page import="com.hbm.servlet.HotelData"%>
<%@page import="com.hbm.servlet.HotelWiseFacilityData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>HotelWise Facility</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body id="wrapper">
	<!-- Start Header -->
    	  <%@include file="include/header.jsp" %>
    <!-- End Header -->
    <div class="container">
	  			<br><br>
<%
				if(request.getParameter("id") == null && request.getParameter("action") == null) {
					List<HotelWiseFacilityData> ul = (List)request.getAttribute("data");
%>
				<div style="float:right;">
		  			<form action="/HBM/hotelWiseFacility" method="get" >
		  				 <button type="submit" name="action" value="createHotelWiseFacility">Create Hotel Wise Facility</button>
		  			</form>
	  			</div>
	  			
		   		<div>
		   			<table class="table table-striped">
		   				<thead class="thead-dark">
			   				<tr>
			   					<th>ID</th>
			   					<th>HotelFacility</th>
			   					<th>HotelName</th>
			   					<th>Action</th>
			   				</tr>
			   			</thead>
		   				<% 
		   					for(HotelWiseFacilityData hotel:ul)
		   					{
		   				%>
		   				<tr>
		   					<td><%= hotel.getId() %></td>
		   					<td><%= hotel.getHotelFacility() %></td>
		   					<td><%= hotel.getHotelName() %></td>
		   					<td>
		   						<a class="button" href="/HBM/hotelWiseFacility?id=<%= hotel.getId() %>&action=edit">Edit</a> <!-- /HBM/cityadmin -->
		   						<a class="button" href="/HBM/hotelWiseFacility?id=<%= hotel.getId() %>&action=delete">Delete</a>  <!-- /HBM/delete -->
		   					</td>
		   				</tr>
		   				<%
		   					} 
		   				%>
		   			</table>
		   		</div>	<!-- admin-inf-div -->
<%
				} else if(request.getParameter("action").equals("edit") || request.getParameter("action").equals("createHotelWiseFacility")) {
					  int id = 0;
					  int hotelFacilityId = 0;
					  int hotelId = 0;
			          if(request.getParameter("action").equals("edit")){
			        	   HotelWiseFacilityData hotel = (HotelWiseFacilityData)request.getAttribute("data");
			        	   id = hotel.getId();
			        	   hotelFacilityId = hotel.getHotelFacilityId();
			        	   hotelId = hotel.getHotelId();
			          }
 %>
		        <form action="/HBM/hotelWiseFacility" method="post">
		   			<% 
		   				if(request.getParameter("action").equals("edit")) {
		  
		   			%>
		   			<input type="hidden" name="action" value="edit" />
		   			<input type="hidden" name="id" value="<%= id %>" />
		   			<% 
		   				} else {
		   			%>
		   			<input type="hidden" name="action" value="create" />
		   			<%
		   				}
		   			%>
		   				
	   		        <div class="form-group">
	   		        		<label for="hotelId">HotelName</label>
						    <select class="form-control" id="hotelId" name="hotelId">
						    	  <option value="0">--Please select--</option>
						      	  <%
	   		        					List<HotelData> ul = (List)request.getAttribute("data1");
						    			for(HotelData hotel:ul){
						    				if(hotel.getId() == hotelId){
						   			    
			   			           %>	
			   			           <option value="<%= hotel.getId() %>" selected="selected" > <%= hotel.getHotelName() %> </option>
			   			           <%
			   			                    }else{
			   			           %>
							      <option value="<%= hotel.getId() %>"> <%= hotel.getHotelName() %> </option>
							      <%
						    			    }	
						    			}
							      %>
						    </select>
					</div>
					<div class="form-group">
	   		        		<label for="hotelFacilityId">Room Facility</label>
						    <select class="form-control" id="hotelFacilityId" name="hotelFacilityId">
						    	  <option value="0">--Please select--</option>
						      	  <%
	   		        					List<HotelFacilityData> ul2 = (List)request.getAttribute("data2");
						    			for(HotelFacilityData hotelFacility:ul2){
						    				if(hotelFacility.getId() == hotelFacilityId){
						   			    
			   			           %>	
			   			           <option value="<%= hotelFacility.getId() %>" selected="selected" > <%= hotelFacility.getHotelFacility() %> </option>
			   			           <%
			   			                    }else{
			   			           %>
							      <option value="<%= hotelFacility.getId() %>"> <%= hotelFacility.getHotelFacility() %> </option>
							      <%
						    			    }	
						    			}
							      %>
						    </select>
					</div>
		   			<input type="submit" value="submit" onclick="return hotel.onFormSubmit();" />
		   		</form>
<% 
		   	 }
%>
       </div>

	    <!-- Start Footer -->
	    	 <%@include file="include/footer.jsp" %> 
	    <!-- End Footer -->
	    <!-- Start Scripts -->
	    	 <%@include file="include/scripts.jsp" %>
	    <!-- End Scripts -->
</body>
</html>