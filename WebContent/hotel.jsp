<%@page import="com.hbm.servlet.HotelData"%>
<%@page import="com.hbm.servlet.CountryData"%>
<%@page import="com.hbm.servlet.CityData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.hbm.servlet.UserData" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.hbm.dao.Database" %>
<html>
<head>
	<title>Hotel</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body id="wrapper">
      <!-- Start Header -->
    		<%@include file="include/header.jsp" %>
      <!-- End Header -->

	   <div class="container">
	  			<br><br>
<%
				if(request.getParameter("id") == null && request.getParameter("action") == null) {
			 		List<HotelData> ul = (List)request.getAttribute("data");
%>
				<div style="float:right;">
		  			<form action="/HBM/hotel" method="get" >
		  				 <button type="submit" name="action" value="createHotel">CreateHotel</button>
		  			</form>
	  			</div>
	  			
		   		<div>
		   			<table class="table table-striped">
		   				<thead class="thead-dark">
			   				<tr>
			   					<th>ID</th>
			   					<th>HotelName</th>
			   					<th>Email</th>
			   					<th>PhoneNo</th>
			   					<th>Address</th>
			   					<th>Country</th>
			   					<th>City</th>
			   					<th>CheckIn</th>
			   					<th>CheckOut</th>
			   					<th>Star Rating</th>
			   					<th>Action</th>
			   				</tr>
			   			</thead>
		   				<% 
		   					for(HotelData hotel:ul)
		   					{
		   				%>
		   				<tr>
		   					<td><%= hotel.getId() %></td>
		   					<td><%= hotel.getHotelName() %></td>
		   					<td><%= hotel.getEmail() %></td>
		   					<td><%= hotel.getPhoneNo() %></td>
		   					<td><%= hotel.getAddress() %></td>
		   					<td><%= hotel.getCountry() %></td>
		   					<td><%= hotel.getCity() %></td>
		   					<td><%= hotel.getCheckIn() %></td>
		   					<td><%= hotel.getCheckOut() %></td>
		   					<td><%= hotel.getStarRating() %></td>
		   					<td>
		   						<a class="button" href="/HBM/hotel?id=<%= hotel.getId() %>&action=edit">Edit</a> <!-- /HBM/cityadmin -->
		   						<a class="button" href="/HBM/hotel?id=<%= hotel.getId() %>&action=delete">Delete</a>  <!-- /HBM/delete -->
		   					</td>
		   				</tr>
		   				<%
		   					} 
		   				%>
		   			</table>
		   		</div>	<!-- admin-inf-div -->
<%
				} else if(request.getParameter("action").equals("edit") || request.getParameter("action").equals("createHotel")) {
					  String hotelName = "";
					  String phoneNo = "";
					  String address = "";
					  String email = "";
					  String checkIn = "";
					  String checkOut = "";
					  String image1 = "";
					  String image2 = "";
					  String image3 = "";
					  String image4 = "";  
					  int id = 0;
					  int starRating = 0;
			          int cityId = 0; 
			          int countryId = 0;
			          if(request.getParameter("action").equals("edit")){
			   				HotelData hotelData = (HotelData)request.getAttribute("data2");
			   				id = hotelData.getId();
			   				hotelName = hotelData.getHotelName();
			   				email = hotelData.getEmail();
			   				address = hotelData.getAddress();
			   				phoneNo = hotelData.getPhoneNo();
			   				cityId = hotelData.getCityId();
			   				countryId = hotelData.getCountryId();
			   				starRating = hotelData.getStarRating();
			   				checkIn = hotelData.getCheckIn();
			   				checkOut = hotelData.getCheckOut();
			   				image1 =  hotelData.getImage1();
				        	image2 =  hotelData.getImage2();
				        	image3 =  hotelData.getImage3();
				        	image4 =  hotelData.getImage4();
			          }
 %>
		        <form action="/HBM/hotel"  method="post">
		   			<% 
		   				if(request.getParameter("action").equals("edit")) {
		  
		   			%>
		   			<input type="hidden" name="action" value="edit" />
		   			<input type="hidden" name="id" value="<%= id %>" />
		   			<% 
		   				} else {
		   			%>
		   			<input type="hidden" name="action" value="create" />
		   			<%
		   				}
		   			%>
		   			<div class="form-group">
			   			<label for="hotelName">HotelName</label>
			   			<input class="form-control" onchange="getCity();" type="text" id="hotelName" name="hotelName" value="<%= hotelName %>" placeholder="Hotel Name.." />
			   		</div>
			   		
			   		<div class="form-group">
			   			<label for="email">Email</label>
			   			<input class="form-control" onchange="getCity();" type="text" id="email" name="email" value="<%= email %>" placeholder="Email.." />
			   		</div>
			   		
			   		<div class="form-group">
			   			<label for="phoneNo">PhoneNo</label>
			   			<input class="form-control" onchange="getCity();" type="text" id="phoneNo" name="phoneNo" value="<%= phoneNo %>" placeholder="Phone Number.." />
			   		</div>
			   			
			   		<div class="form-group">
			   			<label for="address">Address</label>
			   			<input class="form-control" onchange="getCity();" type="text" id="address" name="address" value="<%= address %>" placeholder="Address.." />
			   		</div>	
			   		
			   		<div class="form-group">
						    <label for="countryId">Country</label>
						    
						    <select class="form-control" id="countryId" name="countryId" onchange="getCity();">
						    	  <option value="0">--Please select--</option>
						      	  <%
						   				List<CountryData> ul1 = (List)request.getAttribute("data1");
						    			for(CountryData country:ul1){
						    				if(country.getId() == countryId){
						   			    
			   			           %>	
			   			           <option value="<%= country.getId() %>" selected="selected"> <%= country.getName() %> </option>
			   			           <%
			   			                    }else{
			   			           %>
							      <option value="<%= country.getId() %>"> <%= country.getName() %> </option>
							      <%
						    			    }	
						    			}
							      %>
						    </select>
					</div>
						
			   		<div class="form-group">
						    <label for="cityId">City</label>
						    
						    <select class="form-control" id="cityId" name="cityId">
						    	  <option value="0">--Please select--</option>
						      	  <%
						   				List<CityData> ul = (List)request.getAttribute("data");
						    			for(CityData city:ul){
						    				if(city.getId() == cityId){
						   			    
			   			           %>	
			   			           <option value="<%= city.getId() %>" selected="selected"> <%= city.getName() %> </option>
			   			           <%
			   			                    }else{
			   			           %>
							      <option value="<%= city.getId() %>"> <%= city.getName() %> </option>
							      <%
						    			    }	
						    			}
							      %>
						    </select>
					</div>
					
					<div class="form-group">
						<label for="CheckIn">CheckIn</label>
			   			<input class="form-control" onchange="getCity" type="text" id="checkIn" name="checkIn" value="<%= checkIn %>" placeholder="CheckIn.." />
			   		</div>
			   		
			   		<div class="form-group">
			   			<label for="CheckOut">CheckOut</label>
			   			<input class="form-control" onchange="getCity" type="text" id="checkOut" name="checkOut" value="<%= checkOut %>" placeholder="CheckOut.." />
			   		</div>
			   		
			   		<div class="form-group">
			   			<label for="starRating">Star Rating</label>
			   			<input class="form-control" onchange="getCity" type="text" id="starRating" name="starRating" value="<%= starRating %>" placeholder="Star Rating.." />
					</div>	
					<div class="form-group">
						 <label for="rent">Image1</label>
						 <input class="form-control" type="hidden" id="image1" name="image1" value="<%= image1 %>" placeholder="Image 1 .." />
						 <img alt="alt" id="img1" src="images/<%= image1 %>" width="300" height="300">
						 <button type="button" class="btn btn-info btn-lg" onclick="imageUpload(1)" data-toggle="modal" data-target="#myModal">Image Upload</button>
		   		    </div>
	   		        <br>
	   		        <div class="form-group">
	   			         <label for="rent">Image2</label>
	   			         <input class="form-control" type="hidden" id="image2" name="image2" value="<%= image2 %>" placeholder="Image 2 .." />
	   			         <img alt="alt" id="img2" src="images/<%= image2 %>" width="300" height="300">
	   			         <button type="button" class="btn btn-info btn-lg" onclick="imageUpload(2)" data-toggle="modal" data-target="#myModal">Image Upload</button>
	   		        </div>
	   		        <br>
	   		        <div class="form-group">
	   			         <label for="rent">Image3</label>
	   			         <input class="form-control" type="hidden" id="image3" name="image3" value="<%= image3 %>" placeholder="Image 3 .." />
	   			         <img alt="alt" id="img3" src="images/<%= image3 %>" width="300" height="300">
	   			         <button type="button" class="btn btn-info btn-lg" onclick="imageUpload(3)" data-toggle="modal" data-target="#myModal">Image Upload</button>
	   		        </div>
	   		        <br>
	   		        <div class="form-group">
	   			         <label for="rent">Image4</label>
	   			         <input class="form-control" type="hidden" id="image4" name="image4" value="<%= image4 %>" placeholder="Image 4 .." />
	   			         <img alt="alt" id="img4" src="images/<%= image4 %>" width="300" height="300">
	   			         <button type="button" class="btn btn-info btn-lg" onclick="imageUpload(4)" data-toggle="modal" data-target="#myModal">Image Upload</button>
	   		        </div>
	   		        <br>
	   		        <br>
		   			<input type="submit" value="submit" onclick="return hotel.onFormSubmit();" />
		   		</form>
<%
				}              	 
%>   
       </div>

	    <!-- Start Footer -->
	    	 <%@include file="include/footer.jsp" %> 
	    <!-- End Footer -->
	    <!-- Start Scripts -->
	    	 <%@include file="include/scripts.jsp" %>
	    <!-- End Scripts -->
	    
	    <script type="text/javascript">
		    function getCity(){
		    	var countryId = $("#countryId").val();
		    	$.get(
		    		    "/HBM/cityList?countryId=" + countryId, function(d , s){
		    			var x = " ",i;
		    			var option = " <option value=\"0\">--Please select--</option> ";
		    			if(s == "success"){
		    				 for(i in d){
		    					 option += "<option value='" + d[i].id + "'>" + d[i].name + "</option>";
		    				 }
		    				 $("#cityId").html(option);
		    			}
		    			else{
		    			    alert("Welcome!!!!!");
		    			}
		    		}
		    	)
		    }
		</script>
		
		<div class="modal fade" id="myModal" role="dialog">
		 	 <div class="modal-dialog">
			    <div class="modal-content">
			
			      <!-- Modal Header -->
			      <div class="modal-header">
			        <h4 class="modal-title">Image Uploading</h4>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			      </div>
			
			      <!-- Modal body -->
			      <div class="modal-body">
			        	<form action="/HBM/image" id="upload" method="post" enctype="multipart/form-data">
			        		 <div class="form-group">
			        		     <input type="file" class="form-control" id="image1" name="image1" placeholder="Select Image" />
			        		 </div>
			        		 <br>
			        		  <div class="form-group">
			        		       <button type="submit" class="btn btn-info btn-lg">Upload</button>
			        		  </div>
			        	</form>
			      </div>
			
			      <!-- Modal footer -->
			      <div class="modal-footer">
			      		
			      </div>
			
			    </div>
			  </div>
 		 </div>
 		 
 		 <script type="text/javascript">
 		       var value = 0;   
 		       function imageUpload(val) {
					 value = val
				}
 		        
		 		$(function() {
		 	        $('#upload').ajaxForm({
		 	            success: function(msg) {
							if(value == 1){
								$('#image1').val(msg);
								$('#img1').attr("src","images/" + msg);
							}
							else if (value == 2){
								$('#image2').val(msg);
								$('#img2').attr("src","images/" + msg);
							}
							else if (value == 3){
								$('#image3').val(msg);
								$('#img3').attr("src","images/" + msg);
							}
							else{
								$('#image4').val(msg);
								$('#img4').attr("src","images/" + msg);
							}
		 	            	$('#myModal').modal('toggle');
		 	            },
		 	            error: function(msg) {
		 	            	alert(msg);
		 	            }
		 	        });
		 	    });
 		 </script>
	
  
</body>
</html>