<%@page import="java.awt.Desktop.Action"%>
<%@page import="com.hbm.servlet.RoomTypeData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Room Type</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body id="wrapper">
	  <!-- Start Header -->
    	  <%@include file="include/header.jsp" %>
      <!-- End Header -->

	   <div class="container">
	  			<br><br>
<%
				if(request.getParameter("id") == null && request.getParameter("action") == null) {
					List<RoomTypeData> ul = (List)request.getAttribute("data");
%>
				<div style="float:right;">
		  			<form action="/HBM/roomType" method="get" >
		  				 <button type="submit" name="action" value="createRoomType">CreateRoomType</button>
		  			</form>
	  			</div>
	  			
		   		<div>
		   			<table class="table table-striped">
		   				<thead class="thead-dark">
			   				<tr>
			   					<th>ID</th>
			   					<th>RoomType</th>
			   					<th>Action</th>
			   				</tr>
			   			</thead>
		   				<% 
		   					for(RoomTypeData room:ul)
		   					{
		   				%>
		   				<tr>
		   					<td><%= room.getId() %></td>
		   					<td><%= room.getRoomType() %></td>
		   					<td>
		   						<a class="button" href="/HBM/roomType?id=<%= room.getId() %>&action=edit">Edit</a> <!-- /HBM/cityadmin -->
		   						<a class="button" href="/HBM/roomType?id=<%= room.getId() %>&action=delete">Delete</a>  <!-- /HBM/delete -->
		   					</td>
		   				</tr>
		   				<%
		   					} 
		   				%>
		   			</table>
		   		</div>	<!-- admin-inf-div -->
<%
				} else if(request.getParameter("action").equals("edit") || request.getParameter("action").equals("createRoomType")) {
					  int id = 0;
					  String roomType = "";
			          if(request.getParameter("action").equals("edit")){
			        	   RoomTypeData roomTypeData = (RoomTypeData)request.getAttribute("data1");
			        	   id = roomTypeData.getId();
			        	   roomType = roomTypeData.getRoomType();
			          }
 %>
		        <form action="/HBM/roomType" method="post">
		   			<% 
		   				if(request.getParameter("action").equals("edit")) {
		  
		   			%>
		   			<input type="hidden" name="action" value="edit" />
		   			<input type="hidden" name="id" value="<%= id %>" />
		   			<% 
		   				} else {
		   			%>
		   			<input type="hidden" name="action" value="create" />
		   			<%
		   				}
		   			%>
		   				
			   		<div class="form-group">
						    <label for="roomType">RoomType</label>
						    <input class="form-control" type="text" id="roomType" name="roomType"  value="<%= roomType%>" placeholder="Room Type.." />	    
					</div>
		   			<input type="submit" value="submit" onclick="return hotel.onFormSubmit();" />
		   		</form>
<%
				}              	 
%>   
       </div>

	    <!-- Start Footer -->
	    	 <%@include file="include/footer.jsp" %> 
	    <!-- End Footer -->
	    <!-- Start Scripts -->
	    	 <%@include file="include/scripts.jsp" %>
	    <!-- End Scripts -->
</body>
</html>