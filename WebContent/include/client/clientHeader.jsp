<%@page import="java.util.List"%>
<%@page import="com.hbm.dao.BookingDao"%>
<%@page import="com.hbm.servlet.BookingData"%>
<%@page import="com.hbm.servlet.ClientData"%>
<%@page import="com.hbm.servlet.HotelSearchPageData"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.hbm.servlet.UserData" %>    

<%
        String clientName = null;
		ClientData clientData = (ClientData)session.getAttribute("client_data");
		HotelSearchPageData hotelSearchPage = (HotelSearchPageData)session.getAttribute("search_data");
		if(clientData != null){
			 clientName = clientData.getName();
		}
        	
%>
    
<header>
      <div class="top">
        <div class="container">
          <div class="row">
            <div class="span6">
              <ul class="topmenu">
                <li><a href="#">Home</a> &#47;</li>
                <li><a href="#">Terms</a> &#47;</li>
                <li><a href="#">Privacy policy</a></li>
              </ul>
            </div>
            <div class="span6">

              <ul class="social-network">
                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-circled icon-bglight icon-facebook"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-circled icon-bglight icon-twitter"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-circled icon-linkedin icon-bglight"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-circled icon-pinterest  icon-bglight"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Google +"><i class="icon-circled icon-google-plus icon-bglight"></i></a></li>
              </ul>

            </div>
          </div>
        </div>
      </div>
      <div class="container">


        <div class="row nomargin">
          <div class="span4">
            	<div class="logo">
                     <h1><a href="#username"><i class="icon-tint"></i><%= clientName != null ? clientName : "" %></a></h1>
                </div>
          </div>
          <div class="span8">
            <div class="navbar navbar-static-top">
              <div class="navigation">
                <nav>
                  <ul class="nav topnav">
                    <li class="active">
                      <a href="/HBM/hotelHomePage">Home</a>
                    </li>
                    <li class="dropdown">
                      <a href="#">Hotel <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="/HBM/hotel">Hotel</a></li>
                        <li><a href="/HBM/room">Room</a></li>
                        <li><a href="/HBM/roomWiseFacility">Room Wise Facility</a></li>
                        <li><a href="/HBM/hotelWiseFacility">Hotel Wise Facility</a></li>
                      </ul>
                    </li>
                    <%
                          if(clientData == null){
                    %>
                    <li>
                        <a href="clientLogin.jsp">Sign In</a>
                    </li>
                    <%
                          }else{
                        	    List<BookingData> ul = (List)session.getAttribute("booking_data");
                        	    Boolean value = false;
                        	    for(BookingData booking:ul){
                        	    	 if(clientData.getId() == booking.getBookedBy() && booking.getStatus() == 1){
                        	    		    value = true;
                        	    	 }
                        	    }
                        	    
                        	    if(value == true){
                    %>
                    <li> 
                        <a href="/HBM/checkout?clientId=<%= clientData.getId()%>">Check Out</a>
                    </li>
                    <%
                        	    }
                    %>
                    <li> 
                        <a href="/HBM/clientLogout">Sign Out</a>
                    </li>
                    <%
                          }
                    %>
                  </ul>
                </nav>
              </div>
              <!-- end navigation -->
            </div>
          </div>
        </div>
      </div>
</header>
