<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.hbm.servlet.UserData" %>    
<%
	UserData userData=(UserData)session.getAttribute("USER_DATA");
	if(userData == null) {
		response.sendRedirect("adminLogin.jsp");
		return;
	}
%>
    
<header>
      <div class="top">
        <div class="container">
          <div class="row">
            <div class="span6">
              <ul class="topmenu">
                <li><a href="#">Home</a> &#47;</li>
                <li><a href="#">Terms</a> &#47;</li>
                <li><a href="#">Privacy policy</a></li>
              </ul>
            </div>
            <div class="span6">

              <ul class="social-network">
                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-circled icon-bglight icon-facebook"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-circled icon-bglight icon-twitter"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-circled icon-linkedin icon-bglight"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-circled icon-pinterest  icon-bglight"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Google +"><i class="icon-circled icon-google-plus icon-bglight"></i></a></li>
              </ul>

            </div>
          </div>
        </div>
      </div>
      <div class="container">


        <div class="row nomargin">
          <div class="span4">
            <div class="logo">
              <h1><a href="#username"><i class="icon-tint"></i> <%= userData.getName() %></a></h1>
            </div>
          </div>
          <div class="span8">
            <div class="navbar navbar-static-top">
              <div class="navigation">
                <nav>
                  <ul class="nav topnav">
                    <li class="active">
                      <a href="adminHome.jsp">Home</a>
                    </li>
                    <li class="dropdown">
                      <a href="#">User Management <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="/HBM/adminUser">Admin</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#">Hotel Management <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="/HBM/hotel">Hotel</a></li>
                        <li><a href="/HBM/room">Room</a></li>
                        <li><a href="/HBM/viewBooking">View Booking</a></li>
                        <li><a href="/HBM/roomWiseFacility">Room Wise Facility</a></li>
                        <li><a href="/HBM/hotelWiseFacility">Hotel Wise Facility</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#">Setup <i class="icon-angle-down"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="/HBM/cityUser">City</a></li>
                        <li><a href="/HBM/countryUser">Country</a></li>
                        <li><a href="/HBM/roomType">RoomType</a></li>
                        <li><a href="/HBM/roomFacility">Room Facility</a></li>
                        <li><a href="/HBM/hotelFacility">Hotel Facility</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="adminLogin.jsp">Sign out </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <!-- end navigation -->
            </div>
          </div>
        </div>
      </div>
    </header>