<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.hbm.servlet.UserData" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Page</title>
    <!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>

<body id="wrapper">
	  <!-- Start Header -->
    		<%@include file="include/header.jsp" %>
      <!-- End Header -->
	  <div class="container">
                <%
					if(request.getParameter("id") == null && request.getParameter("action") == null) {
				 		List<UserData> ul = (List)request.getAttribute("data");
                %>
		   		<br><br>
		   		<form action="/HBM/adminUser" method="get" >
		  				 <button type="submit" name="action" value="createAdmin">CreateAdmin</button>
		  		</form>
		   		<div>
		   			<table class="table table-striped">
		   				<thead class="thead-dark">
			   				<tr>
			   					<th>ID</th>
			   					<th>Name</th>
			   					<th>Email</th>
			   					<th>PhoneNo</th>
			   					<th>UserType</th>
			   					<th>IsActive</th>
			   					<th>Active</th>
			   				</tr>
		   				</thead>
		   				<% 
		   					for(UserData user:ul)
		   					{
		   				%>
		   				<tr>
		   					<td><%= user.getId() %></td>
		   					<td><%= user.getName() %></td>
		   					<td><%= user.getEmail() %></td>
		   					<td><%= user.getPhoneNo() %></td>
		   					<td><%= user.getUserType() %></td>
		   					<td><%= user.isActive() %></td>
		   					<td>
		   						<a class="button" href="/HBM/adminUser?id=<%= user.getId() %>&action=edit">Edit</a> <!-- /HBM/editadmin -->
		   						<a class="button" href="/HBM/adminUser?id=<%= user.getId() %>&action=delete">Delete</a>  <!-- /HBM/delete -->
		   					</td>
		   				</tr>
		   				<%
		   					} 
		   				%>
		   			</table>
		   		</div>	<!-- admin-inf-div -->
<%
				} else if(request.getParameter("action").equals("edit") || request.getParameter("action").equals("createAdmin")) {
			          String name="";
			          String email="";
			          String password="";
			          String phoneNo="";
			          if(request.getParameter("action").equals("edit")){
			     	     UserData adminData = (UserData)request.getAttribute("data");
			     	     name=adminData.getName();
			     	     email=adminData.getEmail();
			     	     password=adminData.getPassword();
			     	     phoneNo=adminData.getPhoneNo() ;
			          }
 %>
		        <form action="/HBM/adminUser" method="post" autocomplete="off">
		        	<br><br>
		   			<% 
		   				if(request.getParameter("action").equals("edit")) {
		   					int id = Integer.parseInt(request.getParameter("id"));
		   			%>
		   			<input type="hidden" name="action" value="edit" />
		   			<input type="hidden" name="id" value="<%= id %>" />
		   			<% 
		   				} else {
		   						System.out.println("Hello");
		   						System.out.println(email);
		   						System.out.println(password);
		   						System.out.println("Sazzad11");
		   			%>
		   			<input type="hidden" name="action" value="create" />
		   			<%
		   				}
		   			%>
		   			<div class="form-group">
			   			<label for="name">Name</label>
			   			<input class="form-control" type="text" id="name" name="name" value="<%= name %>" placeholder="Your name ...." />
			   		</div>
			   		<div class="form-group">
			   			<label for="email">Email</label>
			   			<input class="form-control" type="text" id="email" name="email" autocomplete="off" value="<%= email %>" placeholder="Your email ...." />
		   			</div>
		   			<div class="form-group">
			   			<label for="password">Password</label>
			   			<input class="form-control" type="password" id="password" name="password" autocomplete="off" value="<%= password %>" placeholder="Your password ...." />
			   		</div>
			   		<div class="form-group">
			   			<label for="confirmPassword">Confirm Password</label>
			   			<input class="form-control" type="password" id="confirmPassword" placeholder="Confirm password ...." />
		   			</div>
		   			<div class="form-group">
			   			<label for="phoneNo">PhoneNo</label>
			   			<input class="form-control" type="text" id="phoneNo" name="phoneNo" value="<%= phoneNo %>" placeholder="Your phoneNo ...." />
			   		</div>	
		   			<input type="submit" value="submit" onclick="return adminUser.onFormSubmit();" />	
		   		</form>
<%
				}                     	 
%>   
	  </div>
 	  <!-- Start Footer -->
	    	<%@include file="include/footer.jsp" %> 
	  <!-- End Footer -->
	  <!-- Start Scripts -->
	    	<%@include file="include/scripts.jsp" %>
	  <!-- End Scripts -->
  
</body>
</html>