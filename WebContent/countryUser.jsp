<%@page import="com.hbm.servlet.CountryData"%>
<%@page import="com.hbm.servlet.CityData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.hbm.servlet.UserData" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head>
<title>HBM - Admin Home</title>
    <!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>

<body id="wrapper">

    	<!-- Start Header -->
    		<%@include file="include/header.jsp" %>
        <!-- End Header -->
	   <div class="container">
	  			<br><br>			
<%				
				if(request.getParameter("id") == null && request.getParameter("action") == null) {
			 		List<CountryData> ul = (List)request.getAttribute("data");
%>
				<div style="float:right;">
		  			<form action="/HBM/countryUser" method="get" >
		  				 <button type="submit" name="action" value="createCountry">CreateCountry</button>
		  			</form>
	  			</div>
		   		<div>
		   			<table class="table table-striped">
		   				<thead class="thead-dark">
			   				<tr>
			   					<th>ID</th>
			   					<th>Name</th>
			   					<th>Action</th>
			   				</tr>
			   			</thead>
		   				<% 
		   					for(CountryData country:ul)
		   					{
		   				%>
		   				<tr>
		   					<td><%= country.getId() %></td>
		   					<td><%= country.getName() %></td>
		   					<td>
		   						<a class="button" href="/HBM/countryUser?id=<%= country.getId() %>&action=edit">Edit</a> <!-- /HBM/cityadmin -->
		   						<a class="button" href="/HBM/countryUser?id=<%= country.getId() %>&action=delete">Delete</a>  <!-- /HBM/delete -->
		   					</td>
		   				</tr>
		   				<%
		   					} 
		   				%>
		   			</table>
		   		</div>	<!-- admin-inf-div -->
<%
				} else if(request.getParameter("action").equals("edit") || request.getParameter("action").equals("createCountry")) {
			          String name="";
			          if(request.getParameter("action").equals("edit")){
			     	     CountryData country = (CountryData)request.getAttribute("data");
			     	     name = country.getName();
			          }
 %>
		        <form action="/HBM/countryUser" method="post">
		   			<% 
		   				if(request.getParameter("action").equals("edit")) {
		   					int id = Integer.parseInt(request.getParameter("id"));
		   			%>
		   			<input type="hidden" name="action" value="edit" />
		   			<input type="hidden" name="id" value="<%= id %>" />
		   			<% 
		   				} else {
		   			%>
		   			<input type="hidden" name="action" value="create" />
		   			<%
		   				}
		   			%>
		   			<div class="form-group">
			   			<label for="name">Name</label>
			   			<input class="form-control" type="text" id="name" name="name" value="<%= name %>" placeholder="Your name.." />
			   		</div>
		   			<input type="submit" value="submit" onclick="return adminUser.onFormSubmit();" />
		   		</form>
<%
				}                     	 
%>   
		        
	  	   
       </div>
       <!-- Start Footer -->
	    	 <%@include file="include/footer.jsp" %> 
	    <!-- End Footer -->
	    <!-- Start Scripts -->
	    	 <%@include file="include/scripts.jsp" %>
	    <!-- End Scripts -->
</body>
</html>