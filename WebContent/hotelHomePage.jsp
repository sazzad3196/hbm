<%@page import="com.hbm.servlet.RoomData"%>
<%@page import="com.hbm.servlet.HotelData"%>
<%@page import="com.hbm.servlet.CityData"%>
<%@page import="com.hbm.servlet.CountryData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Hotel HomePage</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body id="wrapper">
    <!-- start header -->
		<%@include file="include/client/clientHeader.jsp" %>
    <!-- end header -->

      	<div class="container">
      	 			
      	 	    <div class="row">
                     <br>
      				 <form action="/HBM/hotelSearchPage" method="post">
      				 		<div class="form-group">
	   			                 <label for="checkIn">CHECK IN</label>
	   			                 <input class="form-control" type="date" id="checkIn" name="checkIn" value="" placeholder="Check In .." />
	   			            </div>
	   			            <div class="form-group">
	   			                 <label for="checkOut">CHECK OUT</label>
	   			                 <input class="form-control" type="date" id="checkOut" name="checkOut" value="" placeholder="Check Out .." />
	   		                </div>
	   		                <div class="form-group">
	   			                 <label for="cityId">CITY</label>
	   			                 <select class="form-control" id="cityId" name="cityId" onchange="getCity(2);">
	   			                 <option value="0">--Please select--</option>
	   			                 <%
	   			                		List<CityData> ul1 = (List)request.getAttribute("data1");
	   			                		for(CityData city:ul1){
	   			                 %>
	   			                 <option value="<%= city.getId()%>" ><%= city.getName() %>,<%= city.getCountryName() %></option>
	   			                 <%
	   			                		}
	   			                 %>
	   			                 </select>
	   		                </div>
	   		                <div class="form-group">
	   			                 <label for="adults">ADULTS</label>
	   			                  <select class="form-control" id="adults" name="adults">
	   			                      <option value="1">01</option>
	   			                      <option value="2">02</option>
	   			                      <option value="3">03</option>
	   			                      <option value="4">04</option>
	   			                  </select>
	   			            </div>
	   			            <div class="form-group">
	   			                 <label for="kids">KIDS</label>
	   			                  <select class="form-control" id="kids" name="kids">
	   			                      <option value="1">01</option>
	   			                      <option value="2">02</option>
	   			                      <option value="3">03</option>
	   			                      <option value="4">04</option>
	   			                  </select>
	   			            </div>
	   			            <br><button name="submit" value="submit" onclick="if(onSubmit()) { return true; } else { return false; }"class="btn btn-info btn-lg">Search</button>
      				 </form>
                </div>				   	 			
      	 </div>
    <!-- Start Footer -->
	    <%@include file="include/footer.jsp" %> 
	<!-- End Footer -->

	<!-- Start Scripts -->
	    <%@include file="include/scripts.jsp" %>
	<!-- End Scripts -->
	
	<script type="text/javascript">
    	  function onSubmit() {
    		  var checkIn = $("#checkIn").val().trim();
    		  var checkOut = $("#checkOut").val().trim();
    		  var adults = $("#adults").val().trim();
    		  var kids = $("#kids").val().trim();
    		  var cityId = $("#cityId").val().trim();
    		  var len1 = checkIn.length;
    		  var len2 = checkOut.length;
    		  var index1 = checkIn.lastIndexOf('-') + 1;
    		  var index2 = checkOut.lastIndexOf('-') + 1;
    		  var yearResult =  checkOut.substring(0, 4) - checkIn.substring(0, 4);
    		  var monthResult =  checkOut.substring(5, index2-1) - checkIn.substring(5, index1-1);
    		  var dayResult =  checkOut.substring(index2, len2) - checkIn.substring(index1, len1);
    		  if(checkIn == ''){
  				  alert("Please, Enter CheckIn.");
  				  return false;
    		  }
    		  else if(checkOut == ''){
    			  alert("Please, Enter CheckOut.");
      			  return false;
        	  }
    		  else if(adults == ''){
    			  alert("Please, Enter Adults.");
      			  return false;
        	  }
    		  else if(kids == ''){
    			  alert("Please, Enter Kids.");
      			  return false;
        	  }
    		  else if(cityId == '0'){
    			  alert("Please, Enter CityId.");
      			  return false;
        	  }
    		  else if(dayResult <= 0 || monthResult < 0 || yearResult < 0){
    			  alert("Please, Enter Correct CheckIn and CheckOut.");
    			  return false;
    		  }
    		  return true;
		  }
    </script>
	
</body>
</html>
