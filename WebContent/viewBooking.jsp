<%@page import="com.hbm.servlet.BookingData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>View Booking</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body id="wrapper">
       <!-- Start Header -->
    	  <%@include file="include/header.jsp" %>
       <!-- End Header -->
       
       <div class="container">
	  			<br><br>
	  			<h3 style="text-align: center;color: red;">All Booking</h3>
	  			<div>
		   			<table class="table table-striped">
		   				<thead class="thead-dark">
			   				<tr>
			   					<th>Invoice No</th>
			   					<th>Hotel Name</th>
			   					<th>Hotel Address</th>
			   					<th>Total Room</th>
			   					<th>Check In</th>
			   					<th>Check Out</th>
			   					<th>PaymentDate</th>
			   					<th>Status</th>
			   					<th>Action</th>
			   				</tr>
			   			</thead>
			   			<%
	  			           List<BookingData> ul = (List)request.getAttribute("data");
			   			   for(BookingData bookingData:ul){
	  			        %>
	  			        <tr>
	  			               <td><%= bookingData.getInvoiceNo() %></td>
	  			               <td><%= bookingData.getHotelName() %></td>
	  			               <td><%= bookingData.getHotelAddress() %></td>
	  			               <td><%= bookingData.getTotalRoom() %></td>
	  			               <td><%= bookingData.getCheckIn() %></td>
	  			               <td><%= bookingData.getCheckOut() %></td>
	  			               <td><%= bookingData.getPaymentDate() %></td>
	  			               <td><%= bookingData.getStatus() %></td>
	  			               <td><a href="/HBM/viewMoreBooking?bookingId=<%= bookingData.getId()%>"><button>View More</button></a></td>
	  			        </tr>
	  			        <%
			   			   }
	  			        %>
			   		</table>
			   	</div>
	   </div>
	   
	   <!-- Start Footer -->
	    	 <%@include file="include/footer.jsp" %> 
	    <!-- End Footer -->
	    <!-- Start Scripts -->
	    	 <%@include file="include/scripts.jsp" %>
	    <!-- End Scripts -->

</body>
</html>