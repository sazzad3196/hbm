<%@page import="com.hbm.servlet.RoomFacilityData"%>
<%@page import="com.hbm.servlet.RoomData"%>
<%@page import="com.hbm.servlet.HotelData"%>
<%@page import="com.hbm.servlet.RoomWiseFacilityData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>RoomWise Facility</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body id="wrapper">
	<!-- Start Header -->
    	  <%@include file="include/header.jsp" %>
    <!-- End Header -->
    <div class="container">
	  			<br><br>
<%
				if(request.getParameter("id") == null && request.getParameter("action") == null) {
					List<RoomWiseFacilityData> ul = (List)request.getAttribute("data");
%>
				<div style="float:right;">
		  			<form action="/HBM/roomWiseFacility" method="get" >
		  				 <button type="submit" name="action" value="createRoomWiseFacility">Create Room Wise Facility</button>
		  			</form>
	  			</div>
	  			
		   		<div>
		   			<table class="table table-striped">
		   				<thead class="thead-dark">
			   				<tr>
			   					<th>ID</th>
			   					<th>RoomNo</th>
			   					<th>RoomFacility</th>
			   					<th>HotelName</th>
			   					<th>Action</th>
			   				</tr>
			   			</thead>
		   				<% 
		   					for(RoomWiseFacilityData room:ul)
		   					{
		   				%>
		   				<tr>
		   					<td><%= room.getId() %></td>
		   					<td><%= room.getRoomNumber() %></td>
		   					<td><%= room.getRoomFacility() %></td>
		   					<td><%= room.getHotelName() %></td>
		   					<td>
		   						<a class="button" href="/HBM/roomWiseFacility?id=<%= room.getId() %>&action=edit">Edit</a> <!-- /HBM/cityadmin -->
		   						<a class="button" href="/HBM/roomWiseFacility?id=<%= room.getId() %>&action=delete">Delete</a>  <!-- /HBM/delete -->
		   					</td>
		   				</tr>
		   				<%
		   					} 
		   				%>
		   			</table>
		   		</div>	<!-- admin-inf-div -->
<%
				} else if(request.getParameter("action").equals("edit") || request.getParameter("action").equals("createRoomWiseFacility")) {
					  int id = 0;
					  int roomFacilityId = 0;
					  int hotelId = 0;
					  int roomId = 0;
			          if(request.getParameter("action").equals("edit")){
			        	   RoomWiseFacilityData room = (RoomWiseFacilityData)request.getAttribute("data");
			        	   id = room.getId();
			        	   roomFacilityId = room.getRoomFacilityId();
			        	   hotelId = room.getHotelId();
			        	   roomId = room.getRoomId();
			          }
 %>
		        <form action="/HBM/roomWiseFacility" method="post">
		   			<% 
		   				if(request.getParameter("action").equals("edit")) {
		  
		   			%>
		   			<input type="hidden" name="action" value="edit" />
		   			<input type="hidden" name="id" value="<%= id %>" />
		   			<% 
		   				} else {
		   			%>
		   			<input type="hidden" name="action" value="create" />
		   			<%
		   				}
		   			%>
		   				
	   		        <div class="form-group">
	   		        		<label for="hotelId">HotelName</label>
						    <select class="form-control" id="hotelId" name="hotelId">
						    	  <option value="0">--Please select--</option>
						      	  <%
	   		        					List<HotelData> ul = (List)request.getAttribute("data1");
						    			for(HotelData hotel:ul){
						    				if(hotel.getId() == hotelId){
						   			    
			   			           %>	
			   			           <option value="<%= hotel.getId() %>" selected="selected" > <%= hotel.getHotelName() %> </option>
			   			           <%
			   			                    }else{
			   			           %>
							      <option value="<%= hotel.getId() %>"> <%= hotel.getHotelName() %> </option>
							      <%
						    			    }	
						    			}
							      %>
						    </select>
					</div>
					<div class="form-group">
	   		        		<label for="roomId">RoomNo</label>
						    <select class="form-control" id="roomId" name="roomId">
						    	  <option value="0">--Please select--</option>
						      	  <%
	   		        					List<RoomData> ul1 = (List)request.getAttribute("data2");
						    			for(RoomData room:ul1){
						    				if(room.getId() == roomId){
						   			    
			   			           %>	
			   			           <option value="<%= room.getId() %>" selected="selected" > <%= room.getRoomNo() %> </option>
			   			           <%
			   			                    }else{
			   			           %>
							      <option value="<%= room.getId() %>"> <%= room.getRoomNo() %> </option>
							      <%
						    			    }	
						    			}
							      %>
						    </select>
					</div>
					<div class="form-group">
	   		        		<label for="roomFacilityId">Room Facility</label>
						    <select class="form-control" id="roomFacilityId" name="roomFacilityId">
						    	  <option value="0">--Please select--</option>
						      	  <%
	   		        					List<RoomFacilityData> ul2 = (List)request.getAttribute("data3");
						    			for(RoomFacilityData roomFacility:ul2){
						    				if(roomFacility.getId() == roomFacilityId){
						   			    
			   			           %>	
			   			           <option value="<%= roomFacility.getId() %>" selected="selected" > <%= roomFacility.getFacilityName() %> </option>
			   			           <%
			   			                    }else{
			   			           %>
							      <option value="<%= roomFacility.getId() %>"> <%= roomFacility.getFacilityName() %> </option>
							      <%
						    			    }	
						    			}
							      %>
						    </select>
					</div>
		   			<input type="submit" value="submit" onclick="return hotel.onFormSubmit();" />
		   		</form>
<% 
		   	 }
%>
       </div>

	    <!-- Start Footer -->
	    	 <%@include file="include/footer.jsp" %> 
	    <!-- End Footer -->
	    <!-- Start Scripts -->
	    	 <%@include file="include/scripts.jsp" %>
	    <!-- End Scripts -->
</body>
</html>