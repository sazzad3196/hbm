<%@page import="com.hbm.servlet.RoomData"%>
<%@page import="com.hbm.servlet.BookingDetailsData"%>
<%@page import="com.hbm.servlet.CheckoutData"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>ConfirmBooking Page</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body>
	<!-- start header -->
		<%@include file="include/client/clientHeader.jsp" %>
    <!-- end header -->
    
    
    <div class="container">
    	 <%
    	      List<CheckoutData> ul = (List)request.getAttribute("data");
    	 	  for(CheckoutData checkoutData:ul){
    	 %>
    	 		    <div style="background-color: #ddd">
    	 		    	  <br>
    	 		          <div style="margin-left: 180px;">
    	 		               <h2>Successfully!!This booking is completed.</h2>
    	 		          </div>
    	 				  <div style="margin-left: 480px;">
    	 				  	   <br>
    	 				  	   <div class="row">
    	 				  	       <div class="col-sm-3">
		    	 				       Hotel name: <%= checkoutData.getHotelName() %><br>
		    	 				       Hotel address: <%= checkoutData.getHotelAddress() %><br>
		    	 				       Invoice No: <%= checkoutData.getInvoiceNo() %><br>
		    	 				   </div>
		    	 				   <div class="col-sm-1">
		    	 				   </div>
		    	 				   <div class="col-sm-3">
		    	 				       Check In: <%= checkoutData.getCheckIn() %><br>
		    	 				       Check Out: <%= checkoutData.getCheckOut() %><br>
		    	 				       Booking Date: <%= checkoutData.getBookingDate() %><br>
		    	 				   </div> 
    	 				  </div>
    	 				  <br>
    	 				  <div style="margin-left: 180px;">
					   			<table class="table table-striped">
					   				<thead class="thead-dark">
						   				<tr>
						   					<th>RoomNo</th>
						   					<th>RoomType</th>
						   					<th>Rent</th>
						   				</tr>
						   			</thead>
						   			<%
						   			   List<BookingDetailsData> ul2 = checkoutData.getBookingDetailsData();
									   for(BookingDetailsData bookingDetailsData:ul2) {
										   RoomData roomData = bookingDetailsData.getRoomData();
						   			%>
						   			<tr>
						   			    <td><%= roomData.getRoomNo() %></td>
						   			    <td><%= roomData.getRoomType() %></td>
						   			    <td><%= bookingDetailsData.getRent() %></td>
						   			    <td><img alt="alt" src="images/<%= roomData.getImage1()%>" width="150" height="100"></td>
						   			    <td><img alt="alt" src="images/<%= roomData.getImage2()%>" width="150" height="100"></td>
						   			</tr>
						   			<%
									   }
						   			%>
						   	    </table>
						  </div>
						  <br>
    	 		    </div><br>
    	 <% 
    	 	  }
    	 %>
    </div>
    
    
      
    <!-- Start Footer -->
	    <%@include file="include/footer.jsp" %> 
	<!-- End Footer -->

	<!-- Start Scripts -->
	    <%@include file="include/scripts.jsp" %>
	<!-- End Scripts -->  
</body>	

</html>