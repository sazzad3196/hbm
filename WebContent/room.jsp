<%@page import="com.hbm.servlet.HotelData"%>
<%@page import="com.hbm.servlet.RoomTypeData"%>
<%@page import="com.hbm.servlet.RoomData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Room Type</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body id="wrapper">
	  <!-- Start Header -->
    	  <%@include file="include/header.jsp" %>
      <!-- End Header -->

	   <div class="container">
	  			<br><br>
<%
				if(request.getParameter("id") == null && request.getParameter("action") == null) {
					List<RoomData> ul = (List)request.getAttribute("data");
%>
				<div style="float:right;">
		  			<form action="/HBM/room" method="get" >
		  				 <button type="submit" name="action" value="createRoom">CreateRoom</button>
		  			</form>
	  			</div>
	  			
		   		<div>
		   			<table class="table table-striped">
		   				<thead class="thead-dark">
			   				<tr>
			   					<th>ID</th>
			   					<th>RoomNo</th>
			   					<th>Rent</th>
			   					<th>RoomType</th>
			   					<th>HotelName</th>
			   					<th>Action</th>
			   				</tr>
			   			</thead>
		   				<% 
		   					for(RoomData room:ul)
		   					{
		   				%>
		   				<tr>
		   					<td><%= room.getId() %></td>
		   					<td><%= room.getRoomNo() %></td>
		   					<td><%= room.getRent() %></td>
		   					<td><%= room.getRoomType() %></td>
		   					<td><%= room.getHotelName() %></td>
		   					<td>
		   						<a class="button" href="/HBM/room?id=<%= room.getId() %>&action=edit">Edit</a> <!-- /HBM/cityadmin -->
		   						<a class="button" href="/HBM/room?id=<%= room.getId() %>&action=delete">Delete</a>  <!-- /HBM/delete -->
		   					</td>
		   				</tr>
		   				<%
		   					} 
		   				%>
		   			</table>
		   		</div>	<!-- admin-inf-div -->
<%
				} else if(request.getParameter("action").equals("edit") || request.getParameter("action").equals("createRoom")) {
					  int id = 0;
					  double rent = 0;
					  int roomTypeId = 0;
					  int hotelId = 0;
					  String roomNo = "";
					  String image1 = "";
					  String image2 = "";
					  String image3 = "";
					  String image4 = "";
			          if(request.getParameter("action").equals("edit")){
			        	   RoomData roomData = (RoomData)request.getAttribute("data2");
			        	   id = roomData.getId();
			        	   rent = roomData.getRent();
			        	   roomNo = roomData.getRoomNo();
			        	   roomTypeId = roomData.getRoomTypeId();
			        	   hotelId = roomData.getHotelId();
			        	   image1 = roomData.getImage1();
			        	   image2 = roomData.getImage2();
			        	   image3 = roomData.getImage3();
			        	   image4 = roomData.getImage4();
			        	   
			          }
 %>
		        <form action="/HBM/room" method="post">
		   			<% 
		   				if(request.getParameter("action").equals("edit")) {
		  
		   			%>
		   			<input type="hidden" name="action" value="edit" />
		   			<input type="hidden" name="id" value="<%= id %>" />
		   			<% 
		   				} else {
		   			%>
		   			<input type="hidden" name="action" value="create" />
		   			<%
		   				}
		   			%>
		   				
				    <div class="form-group">
	   			         <label for="roomNo">RoomNo</label>
	   			         <input class="form-control" type="text" id="roomNo" name="roomNo" value="<%= roomNo %>" placeholder="Room Number .." />
	   		        </div>
	   		        <div class="form-group">
	   			         <label for="rent">Rent</label>
	   			         <input class="form-control" type="text" id="rent" name="rent" value="<%= rent %>" placeholder="Rent .." />
	   		        </div>
	   		        <div class="form-group">
	   		        		<label for="roomTypeId">RoomType</label>
						    <select class="form-control" id="roomTypeId" name="roomTypeId">
						    	  <option value="0">--Please select--</option>
						      	  <%
						   				List<RoomTypeData> ul = (List)request.getAttribute("data");
						    			for(RoomTypeData room:ul){
						    				if(room.getId() == roomTypeId){
						   			    
			   			           %>	
			   			           <option value="<%= room.getId() %>" selected="selected" > <%= room.getRoomType() %> </option>
			   			           <%
			   			                    }else{
			   			           %>
							      <option value="<%= room.getId() %>"> <%= room.getRoomType() %> </option>
							      <%
						    			    }	
						    			}
							      %>
						    </select>
					</div>
					<div class="form-group">
	   		        		<label for="hotelId">HotelName</label>
						    <select class="form-control" id="hotelId" name="hotelId">
						    	  <option value="0">--Please select--</option>
						      	  <%
						   				List<HotelData> ul1 = (List)request.getAttribute("data1");
						    			for(HotelData hotel:ul1){
						    				if(hotel.getId() == hotelId){
						   			    
			   			           %>	
			   			           <option value="<%= hotel.getId() %>" selected="selected" > <%= hotel.getHotelName() %> </option>
			   			           <%
			   			                    }else{
			   			           %>
							      <option value="<%= hotel.getId() %>"> <%= hotel.getHotelName() %> </option>
							      <%
						    			    }	
						    			}
							      %>
						    </select>
					</div>
					<br>
					<div class="form-group">
						 <label for="rent">Image1</label>
						 <input class="form-control" type="hidden" id="image1" name="image1" value="<%= image1 %>" placeholder="Image 1 .." />
						 <img alt="alt" id="img1" src="images/<%= image1 %>" width="300" height="300">
						 <button type="button" class="btn btn-info btn-lg" onclick="imageUpload(1)" data-toggle="modal" data-target="#myModal">Image Upload</button>
		   		    </div>
	   		        <br>
	   		        <div class="form-group">
	   			         <label for="rent">Image2</label>
	   			         <input class="form-control" type="hidden" id="image2" name="image2" value="<%= image2 %>" placeholder="Image 2 .." />
	   			         <img alt="alt" id="img2" src="images/<%= image2 %>" width="300" height="300">
	   			         <button type="button" class="btn btn-info btn-lg" onclick="imageUpload(2)" data-toggle="modal" data-target="#myModal">Image Upload</button>
	   		        </div>
	   		        <br>
	   		        <div class="form-group">
	   			         <label for="rent">Image3</label>
	   			         <input class="form-control" type="hidden" id="image3" name="image3" value="<%= image3 %>" placeholder="Image 3 .." />
	   			         <img alt="alt" id="img3" src="images/<%= image3 %>" width="300" height="300">
	   			         <button type="button" class="btn btn-info btn-lg" onclick="imageUpload(3)" data-toggle="modal" data-target="#myModal">Image Upload</button>
	   		        </div>
	   		        <br>
	   		        <div class="form-group">
	   			         <label for="rent">Image4</label>
	   			         <input class="form-control" type="hidden" id="image4" name="image4" value="<%= image4 %>" placeholder="Image 4 .." />
	   			         <img alt="alt" id="img4" src="images/<%= image4 %>" width="300" height="300">
	   			         <button type="button" class="btn btn-info btn-lg" onclick="imageUpload(4)" data-toggle="modal" data-target="#myModal">Image Upload</button>
	   		        </div>
	   		        <br><br>
		   			<input type="submit" class="btn btn-info btn-lg" value="submit" onclick="return hotel.onFormSubmit();" />
		   		</form>
<% 
		   	 }
%>
       </div>

	    <!-- Start Footer -->
	    	 <%@include file="include/footer.jsp" %> 
	    <!-- End Footer -->
	    <!-- Start Scripts -->
	    	 <%@include file="include/scripts.jsp" %>
	    <!-- End Scripts -->
	    
	    <div class="modal fade" id="myModal" role="dialog">
		 	 <div class="modal-dialog">
			    <div class="modal-content">
			
			      <!-- Modal Header -->
			      <div class="modal-header">
			        <h4 class="modal-title">Image Uploading</h4>
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			      </div>
			
			      <!-- Modal body -->
			      <div class="modal-body">
			        	<form action="/HBM/image" id="upload" method="post" enctype="multipart/form-data">
			        		 <div class="form-group">
			        		     <input type="file" class="form-control" id="image1" name="image1" placeholder="Select Image" />
			        		 </div>
			        		 <br>
			        		  <div class="form-group">
			        		       <button type="submit" class="btn btn-info btn-lg">Upload</button>
			        		  </div>
			        	</form>
			      </div>
			
			      <!-- Modal footer -->
			      <div class="modal-footer">
			      		
			      </div>
			
			    </div>
			  </div>
 		 </div>
 		 
 		 <script type="text/javascript">
 		       var value = 0;   
 		       function imageUpload(val) {
					 value = val
				}
 		        
		 		$(function() {
		 	        $('#upload').ajaxForm({
		 	            success: function(msg) {
							if(value == 1){
								$('#image1').val(msg);
								$('#img1').attr("src" , "images/" + msg);
							}
							else if (value == 2){
								$('#image2').val(msg);
								$('#img2').attr("src" , "images/" + msg);
							}
							else if (value == 3){
								$('#image3').val(msg);
								$('#img3').attr("src" , "images/" + msg);
							}
							else{
								$('#image4').val(msg);
								$('#img4').attr("src" , "images/" + msg);
							}
		 	            	$('#myModal').modal('toggle');
		 	            },
		 	            error: function(msg) {
		 	            	alert(msg);
		 	            }
		 	        });
		 	    });
 		 </script>
</body>
</html>