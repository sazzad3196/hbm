<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Client Login</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body>
	<!-- start header -->
		<%@include file="include/client/clientHeader.jsp" %>
    <!-- end header -->
    <div class="container">
    	  <br><br><br>
    	  <div class="row">
    	       <div class="col-sm-3">
    	       </div>
    	       <div class="col-sm-5" style="background-color: #ddd">
    	            <h3 style="text-align:center">Client Login Page</h3><br>
    	            <form action="/HBM/clientLogin" method="post">
    	            	  <input type="hidden" name="action" value="clientLogin">
    	            	  <div class="form-group" style="padding-left: 120px;">
	   			               <label for="userName">User Name</label>
	   			               <input class="form-control" type="text" id="userName" name="userName"  placeholder="UserName .." />
	   		              </div>
	   		              <div class="form-group" style="padding-left: 120px;">
	   			               <label for="password">Password</label>
	   			               <input class="form-control" type="password" id="password" name="password"  placeholder="Password .." />
	   		              </div>
	   		              <div style="padding-left: 120px;">
	   		                   <a href="clientRegister.jsp">create a account</a>
	   		              </div>
	   		              <br>
	   		              <div style="padding-left: 120px;">
	   		                  <button  type="submit" class="btn btn-success" id="submit" value="submit" onclick="if(onSubmit()) { return true; } else { return false; }">Submit</button>
	   		              </div>
    	            </form>
    	       </div>
    	       
    	  </div>
    </div>

	<div id="dropDownSelect1"></div>
	
	<!-- Start Footer -->
	    <%@include file="include/footer.jsp" %> 
	<!-- End Footer -->
	
	<script type="text/javascript">
		function onSubmit() {
			var userName = $("#userName").val().trim();
			var password = $("#password").val().trim();
			if(userName == '' && password == ''){
				alert("Please, enter username and password.");
				return false;
			} else if(userName == ''){
				alert("Please, enter username.");
				return false;
			} else if(password == ''){
				alert("Please, enter password.");
				return false;
			}
			
			return true;
		}
	</script>
	<!-- Start Scripts -->
	    <%@include file="include/scripts.jsp" %>
	<!-- End Scripts -->

</body>
</html>