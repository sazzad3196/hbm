<%@page import="com.hbm.servlet.HotelFacilityData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Hotel Facilities</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body id="wrapper">
	  <!-- Start Header -->
    	  <%@include file="include/header.jsp" %>
      <!-- End Header -->
      <div class="container">
	  			<br><br>
<%
				if(request.getParameter("id") == null && request.getParameter("action") == null) {
					 List<HotelFacilityData> ul = (List)request.getAttribute("data");
%>
				<div style="float:right;">
		  			<form action="/HBM/hotelFacility" method="get" >
		  				 <button type="submit" name="action" value="createHotel">Create Hotel Facility</button>
		  			</form>
	  			</div>
	  			
		   		<div>
		   			<table class="table table-striped">
		   				<thead class="thead-dark">
			   				<tr>
			   					<th>ID</th>
			   					<th>HotelFacility</th>
			   					<th>Action</th>
			   				</tr>
			   			</thead>
		   				<% 
		   					for(HotelFacilityData facility:ul)
		   					{
		   				%>
		   				<tr>
		   					<td><%= facility.getId() %></td>
		   					<td><%= facility.getHotelFacility() %></td>
		   					<td>
		   						<a class="button" href="/HBM/hotelFacility?id=<%= facility.getId() %>&action=edit">Edit</a> <!-- /HBM/cityadmin -->
		   						<a class="button" href="/HBM/hotelFacility?id=<%= facility.getId() %>&action=delete">Delete</a>  <!-- /HBM/delete -->
		   					</td>
		   				</tr>
		   				<%
		   					} 
		   				%>
		   			</table>
		   		</div>	<!-- admin-inf-div -->
<%
				} else if(request.getParameter("action").equals("edit") || request.getParameter("action").equals("createHotel")) {
					  int id = 0;
					  String hotelFacility = "";
			          if(request.getParameter("action").equals("edit")){
			        	  HotelFacilityData hotelData = (HotelFacilityData)request.getAttribute("data");
			        	   id = hotelData.getId();
			        	   hotelFacility = hotelData.getHotelFacility();
			        	   
			          }
 %>
		        <form action="/HBM/hotelFacility" method="post">
		   			<% 
		   				if(request.getParameter("action").equals("edit")) {
		  
		   			%>
		   			<input type="hidden" name="action" value="edit" />
		   			<input type="hidden" name="id" value="<%= id %>" />
		   			<% 
		   				} else {
		   			%>
		   			<input type="hidden" name="action" value="create" />
		   			<%
		   				}
		   			%>
		   				
				    <div class="form-group">
	   			         <label for="hotelFacility">HotelFacility</label>
	   			         <input class="form-control" type="text" id="hotelFacility" name="hotelFacility" value="<%= hotelFacility %>" placeholder="Hotel Facility .." />
	   		        </div>  
		   			<input type="submit" value="submit" onclick="return hotel.onFormSubmit();" />
		   		</form>
<% 
		   	 }
%>
       </div>

	    <!-- Start Footer -->
	    	 <%@include file="include/footer.jsp" %> 
	    <!-- End Footer -->
	    <!-- Start Scripts -->
	    	 <%@include file="include/scripts.jsp" %>
	    <!-- End Scripts -->
</body>
</html>