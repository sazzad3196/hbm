<%@page import="com.hbm.servlet.RoomWiseFacilityData"%>
<%@page import="java.util.List"%>
<%@page import="com.hbm.servlet.RoomData"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Room Details</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    	
    <!-- End Styles -->
</head>
<body id="wrapper">
	<!-- start header -->
		<%@include file="include/client/clientHeader.jsp" %>
    <!-- end header -->
    
    <div class="container">
    	  <br><br><br>
    	  <div class="row">
    	  
    	  		<div class="col-sm-3">
    	  			 <h1>Room Details Description</h1>
    	  		</div>
    	  		<div class="col-sm-8">
    	  			 <%
    	  			 		RoomData roomData = (RoomData)request.getAttribute("data1");
    	  			 		String image1 = roomData.getImage1();
    	  			 		String image2 = roomData.getImage2();
    	  			 		String image3 = roomData.getImage3();
    	  			 		String image4 = roomData.getImage4();
    	  			 		int roomId = roomData.getId();
    	  			 		int hotelId = roomData.getHotelId();
    	  			 		double rent = roomData.getRent();
    	  			 		
    	  			 %>
    	  			 <div class="overflow-hidden" style="height:500px">
						  <span onclick="this.parentElement.style.display='none'" class="closebtn">&times;</span>
						  <img src="images/<%= image1 %>" id="expandedImg" width="900" height="900">
                     </div>
    	  			 <!-- <img alt="alt" src="images/<%= image1 %>" width="900" height="900">  -->
    	  			 <div class="overflow-hidden">
   	  			 		 	<br><br>
   	  			 		 	<div style="height:100px;background-color:#ddd">
   	  			 		 		<table>
   	  			 		 			<tr>
   	  			 		 				<td style="width: 22.75%;align: top">
   	  			 		 					<img src="images/<%= image1 %>" alt="Nature" onclick="myFunction(this);">
   	  			 		 				</td>
   	  			 		 				<td style="width: 3%"></td>
   	  			 		 				<td style="width: 22.75%;align: top">
   	  			 		 					<img src="images/<%= image2 %>" alt="Nature" onclick="myFunction(this);">
   	  			 		 				</td>
   	  			 		 				<td style="width: 3%"></td>
   	  			 		 				<td style="width: 22.75%;align: top;">
   	  			 		 					<img src="images/<%= image3 %>" alt="Nature" onclick="myFunction(this);">
   	  			 		 				</td>
   	  			 		 				<td style="width: 3%"></td>
   	  			 		 				<td style="width: 22.75%;align: top;">
   	  			 		 					<img src="images/<%= image4 %>" alt="Nature" onclick="myFunction(this);">
   	  			 		 				</td>
   	  			 		 			</tr>
   	  			 		 		</table>
	                    	</div>
		             </div>
		             <br><br>
		             
		             <div class="room-info-container overflow-hidden" style="height:230px;background-color: #ddd">
		             	<div class="row">
		             	 		<div class="col-sm-4">
		             	 		     <img alt="alt" src="images/<%= image1%>" width="900" height="500">
		             	 		</div>
		             	 		<div class="col-sm-7">
		             	 			 <br><h3>Room Overview</h3><br>
		             	 			 Rent: <%= rent %><br>
		             	 			 <%
		             	 			        int i = 0;
		             	 			 		List<RoomWiseFacilityData> ul2 = (List)request.getAttribute("data2");	
		             	 			 		for(RoomWiseFacilityData roomWiseFacility:ul2){
		             	 			 			  if(roomId == roomWiseFacility.getRoomId() && hotelId == roomWiseFacility.getHotelId()){
		             	 			 					   String roomFacility = roomWiseFacility.getRoomFacility();
		             	 			 					   if(i > 0){
		             	 			 				  	            i++;
		             	 			 %>
		             	 			 ,<%= roomFacility %>
		             	 			 <%
		             	 			 					   }else{
		             	 			 						     i++;
		             	 			 %>
		             	 			 <%= roomFacility %>
		             	 			 <%
		             	 			 					   }
		             	 			 				  }
		             	 			 			}
		             	 			 %>
		             	 			 .<br><br>
		             	 			 <div class="row">
		             	 			 		 <div class="col-sm-7">
		             	 			 		 </div>
               		    		             <div class="col-sm-3">
              		    		                        <input type="hidden" class="roomId" value="<%= roomId%>"/>
              		    		                        <input type="hidden" class="hotelId" value="<%= hotelId%>"/>
              		    		                        <input type="hidden" class="rent" value="<%= rent%>"/>
              		    		                        
              		    		                        <% 
              		    		                              //System.out.println("Rent1: " + rent);
              		    		                        	  if(clientData == null) {
              		    		                        %>
              		    		                        <button  type="button" class="btn btn-success" onclick="openModal(this)">Book now</button>
              		    		                        <%
              		    		                        	  } else {
              		    		                        %>
              		    		             		    <button type="button" class="btn btn-success" onclick="bookingButton(this);">Book now</button>
              		    		             		    <%
              		    		                        	  }
              		    		             		    %>
               		    		             </div>
             		    		      </div>
		             	 		</div>
		             	</div>	  
		             </div>
		                    
    	  		</div><!-- end col-sm-8 -->
    	  			
    	  </div>	<!-- end row -->
    </div>
    
    
    <div class="modal hide" id="myModal" role="dialog">
	 	 <div class="modal-dialog">
		    <div class="modal-content">
		
		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title" style="text-align:center">Client Login Page</h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		
		      <!-- Modal body -->
		      <div class="modal-body">
		        	<form action="/HBM/booking" id="login" method="get">
		        		  <input type="hidden" id="action" name="action" value="login"/>
		        		  <input type="hidden" id="roomId" name="roomId" value=""/>
		               	  <input type="hidden" id="hotelId" name="hotelId" value=""/>
		               	  <input type="hidden" id="rent" name="rent" value=""/>
    	            	  <div class="form-group" style="padding-left: 120px;">
	   			               <label for="userName">User Name</label>
	   			               <input class="form-control" type="text" id="userName" name="userName"  placeholder="UserName .." />
	   		              </div>
	   		              <div class="form-group" style="padding-left: 120px;">
	   			               <label for="password">Password</label>
	   			               <input class="form-control" type="password" id="password" name="password"  placeholder="Password .." />
	   		              </div>
	   		              <div style="padding-left: 120px;">
	   		                   <a href="clientRegister.jsp" target="_blank">Create a account</a>
	   		              </div>
	   		              <br>
	   		              <div style="padding-left: 120px;">
	   		                  <button  type="submit" class="btn btn-success" id="submit" value="submit" onclick="if(onSubmit()) { return true; } else { return false; }">Submit</button>
	   		              </div>
    	            </form>
		      </div>
		
		      <!-- Modal footer -->
		      <div class="modal-footer"></div>
		
		   </div>
		</div>
	</div>
	  
	<!-- Start Footer -->
	    <%@include file="include/footer.jsp" %> 
	<!-- End Footer -->

	<!-- Start Scripts -->
	    <%@include file="include/scripts.jsp" %>
	<!-- End Scripts -->
	
    <script type="text/javascript">
			function openModal(obj) {
				var hotelId = $(obj).closest(".room-info-container").find(".hotelId").val();
				var roomId = $(obj).closest(".room-info-container").find(".roomId").val();
				var rent =  $(obj).closest(".room-info-container").find(".rent").val();
				$("#hotelId").val(hotelId);
			    $("#roomId").val(roomId);
			    $("#rent").val(roomId);
				$("#myModal").modal('toggle');
			}
			
			function onSubmit() {
				var userName = $("#userName").val().trim();
				var password = $("#password").val().trim();
				if(userName == '' && password == ''){
					alert("Please, enter username and password.");
					return false;
				} else if(userName == ''){
					alert("Please, enter username.");
					return false;
				} else if(password == ''){
					alert("Please, enter password.");
					return false;
				}
				
				return true;
			}
		 	  
			function bookingButton(obj) {
				var hotelId = $(obj).closest(".room-info-container").find(".hotelId").val();
				var roomId = $(obj).closest(".room-info-container").find(".roomId").val();
				var rent =  $(obj).closest(".room-info-container").find(".rent").val();
				$.get(
						 "/HBM/booking?hotelId=" + hotelId + "&roomId=" + roomId + "&action=anotherLogin&rent=" + rent , function(d , s) {
								if(s == "success"){
									 alert(d);
								}
								else{
									alert("Not Successfully!!!");
								}
						}
				      )
			}
			
		 	 $(function() {    
		 		   $("#login").ajaxForm({
		 			    success: function(msg) {
		 			    	if(msg == "Please enter correct Username and Password"){
		 			    		alert("Please enter correct Username and Password.");
		 			    	}
		 			    	else if(msg == "Successfully Login and Successfully you have done booking this room" || msg == "Successfully Login but Already you had done booking this room" || msg == "Successfully Login but you have done booking this room"){
		 			    		//$(".button-info").attr("disabled",true);
		 			    		alert(msg);
		 			    		window.location.reload(true);
							    $("#myModal").modal('toggle');
		 			    	}
						},
						error: function(msg) {
							alert(msg);
						}
		 		   });
			   });
			
			  function myFunction(imgs) {
				  var expandImg = document.getElementById("expandedImg");
				  var imgText = document.getElementById("imgtext");
				  expandImg.src = imgs.src;
				  expandImg.parentElement.style.display = "block";
		      }
	  </script>

</body>
</html>