<%@page import="com.hbm.servlet.HotelWiseFacilityData"%>
<%@page import="com.hbm.servlet.HotelData"%>
<%@page import="com.hbm.servlet.RoomFacilityData"%>
<%@page import="com.hbm.servlet.HotelFacilityData"%>
<%@page import="java.util.Date"%>
<%@page import="com.hbm.servlet.HotelSearchPageData"%>
<%@page import="com.hbm.servlet.CityData"%>
<%@page import="com.hbm.servlet.CountryData"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Hotel SearchPage</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>
<body id="wrapper">
    <!-- start header -->
		<%@include file="include/client/clientHeader.jsp" %>
    <!-- end header -->

      	  <div class="container">
      	 			
                 <br>
       			 <% 
       			 		HotelSearchPageData search = (HotelSearchPageData)request.getAttribute("data2");
       			 		String checkInDate = search.getCheckInData();
       			 		String checkOutDate = search.getCheckOutData();
       			 		int cityId = search.getCityId();
       			 		int adults = search.getAdults();
       			 		int kids = search.getKids();
       			 %>
           		<div class="row">
           			 	<div class="col-sm-2">
           			 		<div class="form-group">
	   			                 <label for="checkIn">CHECK IN</label>
	   			                 <input class="form-control" type="date" id="checkIn" name="checkIn" value="<%= checkInDate %>" placeholder="Check In .." />
	   			            </div>
           			 	</div>
           			 	<div class="col-sm-2">
           			 		<div class="form-group">
	   			                 <label for="checkOut">CHECK OUT</label>
	   			                 <input class="form-control" type="date" id="checkOut" name="checkOut" value="<%= checkOutDate %>" placeholder="Check Out .." />
	   		                </div>
           			 	</div>
           			 	<div class="col-sm-2">
           			 		<label for="cityId">CITY</label>
	   			                 <select class="form-control" id="cityId" name="cityId" onchange="getCity(2);">
	   			                 <%
	   			                		List<CityData> ul1 = (List)request.getAttribute("data1");
	   			                		for(CityData city:ul1){
	   			                		    if(city.getId() == cityId){
	   			                 %>
	   			                 <option selected value="<%= city.getId()%>" ><%= city.getName() %>,<%= city.getCountryName() %></option>
	   			                 <%
	   			                		    }else{
	   			                  %>
	   			                  <option value="<%= city.getId()%>" ><%= city.getName() %>,<%= city.getCountryName() %></option>
	   			                  <%
	   			                		    }
	   			                		}
	   			                  %>
	   			            </select>
           			 	</div>
           			 	<div class="col-sm-2">
           			 		<label for="adults">ADULTS</label>
  			                <select class="form-control" id="adults" name="adults">
  			                     <option value="1" <%= (adults == 1? "selected" : "") %>> 01</option>
  			                     <option value="2" <%= (adults == 2? "selected" : "") %>> 02 </option>
  			                     <option value="3" <%= (adults == 3? "selected" : "") %> > 03 </option>
  			                     <option value="4" <%= (adults == 4? "selected" : "") %>> 04 </option>
  			                </select>
           			 	</div>
           			 	<div class="col-sm-2">
           			 		<label for="kids">KIDS</label>
	   			            <select class="form-control" id="kids" name="kids">
	   			                  <option value="1" <%= (kids==1? "selected" : "") %>>01</option>
	   			                  <option value="2" <%= (kids==2? "selected" : "") %>>02</option>
	   			                  <option value="3" <%= (kids==3? "selected" : "") %>>03</option>
	   			                  <option value="4" <%= (kids==4? "selected" : "") %>>04</option>
	   			            </select>
           			 	</div>
           		 </div> 
           		 <div class="row">
           		 		<div class="col-sm-4">
           		 			<h4>Hotel Facility</h4>
           		 			<%
           		 				  int id = 0;
           		 				  List<HotelFacilityData> ul3 = (List)request.getAttribute("data3");
           		 				  for(HotelFacilityData facility:ul3){
           		 			%>
           		 			<div class="custom-control custom-checkbox mb-3">
                                 <input type="checkbox" class="custom-control-input" id="hotelFacilityId<%= id %>" name="hotelFacilityId">
                                 <label class="custom-control-label" for="hotelFacilityId<%= id++ %>"><%= facility.getHotelFacility() %></label>
                            </div>
                            <%
           		 				  }
                            %>
                            <br><h4>Room Facility</h4>
                            <%
           		 				  int id1 = 0;
           		 				  List<RoomFacilityData> ul4 = (List)request.getAttribute("data4");
           		 				  for(RoomFacilityData room:ul4){
           		 			%>
           		 			<div class="custom-control custom-checkbox mb-3">
                                 <input type="checkbox" class="custom-control-input" id="roomFacilityId<%= id1 %>" name="roomFacilityId">
                                 <label class="custom-control-label" for="roomFacilityId<%= id1++ %>"><%= room.getFacilityName() %></label>
                            </div>
                            <%
           		 				  }
                            %>
                            <br>
                            <h4>Rating</h4>
                            <%
           		 				  for(int i=1;i<=5;i++){
           		 			%>
           		 			<div class="custom-control custom-checkbox mb-3">
                                 <input type="checkbox" class="custom-control-input" id="starRating<%= i %>" name="starRating">
                                 <label class="custom-control-label" for="starRating<%= i %>"><%= i %> Star</label>
                            </div>
                            <%
           		 				  }
                            %>
                            <br><h4>Price Range</h4>
                            <div id="slider-range" class="price-filter-range" name="rangeInput">
								<input type="number" min=0 max="9900" oninput="validity.valid||(value='0');" 
									id="min_price" class="price-range-field" />
								<input type="number" min=0 max="10000" oninput="validity.valid||(value='10000');" 
									id="max_price" class="price-range-field" />
							</div><br>
							
                      </div>
                      <div class="col-sm-7">
                      		<%
                      			 List<HotelData> ul5 = (List)request.getAttribute("data5");
                      			 for(HotelData hotel:ul5){
                      				  int hotelId = hotel.getId();
                      				  String image1 = hotel.getImage1();
                      				  String country = hotel.getCountry();
                      				  int starRating = hotel.getStarRating();
                      				  String hotelName = hotel.getHotelName();
                      				  String city = hotel.getCity();
                      				  //System.out.println("Image1: " + image1);
                      			 
                      		%>
                      		<div class="overflow-hidden">
                      		    <div style="height:200px;background-color:#ddd">
                      		    		<div class="row">
                      		    		   <div class="col-sm-5">
                      		    		       <img alt="alt" id="img1" src="images/<%= image1 %>" width="300" height="300">
                      		    		   </div>
                      		    		   <div class="col-sm-6">
                      		    		   	   <br><p>
                      		    		   	         <h4 class="font-weight-bold"><%= hotelName %>,<%= starRating %> Star</h4><br>
                      		    		             From: <%= country %>,<%= city %>
                      		    		           </p>
                      		    		           <% 
                      		    		                List<HotelWiseFacilityData> ul6 = (List)request.getAttribute("data6");
                      		    		           		int j = 0;
                      		    		           		for(HotelWiseFacilityData hotelWise:ul6){
                      		    		           			if(hotelId == hotelWise.getHotelId()){
                      		    		           				String hotelFacility = hotelWise.getHotelFacility();
                      		    		           				if(j > 0){
                      		    		           %>
                      		    		                        ,<%= hotelFacility %>
                      		    		                        
                      		    		           <%      
                      		    		           				j++;
                      		    		           				}else{
                      		    		           %>
                      		    		           				<%= hotelFacility %>
                      		    		           <%
                      		    		           				j++;
                      		    		           				}
                      		    		           %>
                      		    		           <%
                      		    		           			}
                      		    		           		}
                      		    		           %>
                      		    		           .<br>
                      		    		           <form action="/HBM/hotelDetails" method="get">
                      		    		           		<input type="hidden" name="hotelId" value="<%= hotelId%>">
                      		    		           		<button type="submit" class="btn btn-success">view more</button>
                      		    		           </form>
                      		    		           
                      		    		   </div>
                      		    		</div>
                      		            
                      		    </div>
                      		</div><br>
                      		<%
                      			 }
                      		%>
                      </div>
              	 </div>      
					                           
                  

             </div>		
             		   	 			
      	    
    <!-- Start Footer -->
	    <%@include file="include/footer.jsp" %> 
	<!-- End Footer -->

	<!-- Start Scripts -->
	    <%@include file="include/scripts.jsp" %>
	<!-- End Scripts -->
	<script type="text/javascript">
		$("#slider-range").slider({
			  range: true,
			  orientation: "horizontal",
			  min: 0,
			  max: 10000,
			  values: [0, 10000],
			  step: 100,
	
			  slide: function (event, ui) {
			    if (ui.values[0] == ui.values[1]) {
			      return false;
			    }
			    
			    $("#min_price").val(ui.values[0]);
			    $("#max_price").val(ui.values[1]);
			  }
		});
	</script>
</body>
</html>