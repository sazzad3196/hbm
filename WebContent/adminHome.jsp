<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.hbm.servlet.UserData" %>
<html>
<head>

<title>Admin Home Page</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
</head>

<body>

  <div id="wrapper">
    <!-- start header -->
		<%@include file="include/header.jsp" %>
    <!-- end header -->

    <!-- section intro -->
    <section id="intro">
      <div class="intro-content">
        <h2>Welcome to Remember!</h2>
        <h3>Lorem ipsum dolor sit amet, elit persecuti efficiendi</h3>
        <div>
          <a href="#content" class="btn-get-started scrollto">Get Started</a>
        </div>
      </div>
    </section>
    <!-- /section intro -->
  
    <!-- Start Footer -->
	    <%@include file="include/footer.jsp" %> 
	<!-- End Footer -->
  </div>

	<!-- Start Scripts -->
	    <%@include file="include/scripts.jsp" %>
	<!-- End Scripts -->

</body>
</html>