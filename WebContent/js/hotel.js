var hotel = {
	onFormSubmit : function(){
		var hotelName = $("#hotelName").val().trim();
		var email = $("#email").val().trim();
		var phoneNo = $("#phoneNo").val().trim();
		var address = $("#address").val().trim();
		var cityId = $("#cityId").val().trim();
		var countryId = $("#countryId").val().trim();
		var starRating = $("#starRating").val().trim();
		var checkIn = $("#checkIn").val().trim();
		var checkOut = $("#checkOut").val().trim();
		
		if(hotelName == ''){
			return false;
		}
		else if(email == ''){
			return false;
		}
		else if(phoneNo == ''){
			return false;
		}
		else if(address == ''){
			return false;
		}
		else if(cityId == ''){
			return false;
		}
		else if(countryId == ''){
			return false;
		}
		else if(starRating == ''){
			return false;
		}
		else if(checkIn == ''){
			return false;
		}
		else if(checkOut == ''){
			return false;
		}
		
		return true;
	}
}