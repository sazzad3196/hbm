<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Client Register</title>
	<!-- Start Styles -->
    	<%@include file="include/styles.jsp" %>
    <!-- End Styles -->
	<script type="text/javascript">
		function onSubmit() {
			var userName = $("#userName").val().trim();
			var password = $("#password").val().trim();
			var conPassword = $("#conPassword").val().trim();
			var email = $("#email").val().trim();
			var phoneNo = $("#phoneNo").val().trim();
			var gender = $("#gender").val().trim();
			var age = $("#age").val().trim();
			
		    if(userName == ''){
				alert("Please, enter username.");
				return false;
			}
		    else if(email == ''){
				alert("Please, enter email.");
				return false;
			}
		    else if(password == ''){
				alert("Please, enter password.");
				return false;
			}
		    else if(conPassword == ''){
				alert("Please, enter confirm password.");
				return false;
			}
		    else if(phoneNo == ''){
				alert("Please, enter phone number.");
				return false;
			}
		    else if(gender == ''){
				alert("Please, enter gender.");
				return false;
			}
		    else if(age == ''){
				alert("Please, enter age.");
				return false;
			}
		    else if(password != conPassword){
		    	alert("Please, enter correct password and confirm password.");
				return false;
		    }
			
			return true;
		}
	</script>
</head>
<body>
	  <!-- start header -->
		<%@include file="include/client/clientHeader.jsp" %>
    <!-- end header -->
    <div class="container">
    	  <br><br><br>
    	  <div class="row">
    	       <div class="col-sm-3">
    	       </div>
    	       <div class="col-sm-5" style="background-color: #ddd">
    	            <h3 style="text-align:center">Client Register Page</h3><br>
    	            <form action="/HBM/clientRegister" method="post">
    	            	  <input type="hidden" name="action" value="clientRegister">
    	            	  <div class="form-group" style="padding-left: 120px;">
	   			               <label for="userName">User Name</label>
	   			               <input class="form-control" type="text" id="userName" name="userName"  placeholder="UserName .." />
	   		              </div>
	   		              <div class="form-group" style="padding-left: 120px;">
	   			               <label for="email">Email</label>
	   			               <input class="form-control" type="text" id="email" name="email"  placeholder="Email .." />
	   		              </div>
	   		              <div class="form-group" style="padding-left: 120px;">
	   			               <label for="phoneNo">Phone Number</label>
	   			               <input class="form-control" type="text" id="phoneNo" name="phoneNo"  placeholder="Phone Number .." />
	   		              </div>
	   		              <div class="form-group" style="padding-left: 120px;">
	   			               <label for="password">Password</label>
	   			               <input class="form-control" type="password" id="password" name="password"  placeholder="Password .." />
	   		              </div>
	   		              <div class="form-group" style="padding-left: 120px;">
	   			               <label for="conPassword">Confirm Password</label>
	   			               <input class="form-control" type="password" id="conPassword" name="conPassword"  placeholder="Confirm Password .." />
	   		              </div>
	   		              <div class="form-group" style="padding-left: 120px;">
	   			               <label for="conPassword">Gender</label>
	   			               <input type="radio" id="gender" name="gender" value="male"> Male  
	   			               <input type="radio" id="gender" name="gender" value="female"> Female  
	   			               <input type="radio" id="gender" name="gender" value="other"> Other
	   		              </div>
	   		              <div class="form-group" style="padding-left: 120px;">
	   			               <label for="age">Age</label>
	   			               <input class="form-control" type="text" id="age" name="age"  placeholder="Age .." />
	   		              </div>
	   		              <br>
	   		              <div style="padding-left: 120px;">
	   		                  <button  type="submit" class="btn btn-success" id="submit" value="submit" onclick="if(onSubmit()) { return true; } else { return false; }">Submit</button>
	   		              </div>
    	            </form>
    	       </div>
    	       
    	  </div>
    </div>

	<div id="dropDownSelect1"></div>
	
	<!-- Start Footer -->
	    <%@include file="include/footer.jsp" %> 
	<!-- End Footer -->

	<!-- Start Scripts -->
	    <%@include file="include/scripts.jsp" %>
	<!-- End Scripts -->
	  
</body>
</html>