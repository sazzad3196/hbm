CREATE DATABASE  IF NOT EXISTS `dbinfo` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dbinfo`;
-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: dbinfo
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cityinfo`
--

DROP TABLE IF EXISTS `cityinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cityinfo` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `CountryId` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cityinfo`
--

LOCK TABLES `cityinfo` WRITE;
/*!40000 ALTER TABLE `cityinfo` DISABLE KEYS */;
INSERT INTO `cityinfo` VALUES (9,'Dhaka',1),(10,'Delli',2),(11,'Mumbai',2);
/*!40000 ALTER TABLE `cityinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countryinfo`
--

DROP TABLE IF EXISTS `countryinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countryinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countryinfo`
--

LOCK TABLES `countryinfo` WRITE;
/*!40000 ALTER TABLE `countryinfo` DISABLE KEYS */;
INSERT INTO `countryinfo` VALUES (1,'Bangladesh'),(2,'India'),(3,'China'),(4,'Nepal');
/*!40000 ALTER TABLE `countryinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotelfacilityinfo`
--

DROP TABLE IF EXISTS `hotelfacilityinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotelfacilityinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelFacility` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotelfacilityinfo`
--

LOCK TABLES `hotelfacilityinfo` WRITE;
/*!40000 ALTER TABLE `hotelfacilityinfo` DISABLE KEYS */;
INSERT INTO `hotelfacilityinfo` VALUES (1,'Fresh water Swimming pool with sun-loungers'),(2,'Pool bar offering drinks & light snacks'),(3,'Room service'),(5,'Free Wi-Fi internet access throughout the hotel'),(6,'Daily newspaper with breakfast'),(7,'Doctor on call'),(8,'All rooms are smoke free'),(9,'Car & ATV rental'),(10,'Private and Exclusive Canopy Candlelight dinners'),(11,'Reception desk open 24 hours a day');
/*!40000 ALTER TABLE `hotelfacilityinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotelinfo`
--

DROP TABLE IF EXISTS `hotelinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotelinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelName` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `phoneNo` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `checkIn` varchar(45) NOT NULL,
  `checkOut` varchar(45) NOT NULL,
  `city` int(11) NOT NULL,
  `country` int(11) NOT NULL,
  `starRating` int(11) NOT NULL,
  `image1` varchar(45) NOT NULL,
  `image2` varchar(45) NOT NULL,
  `image3` varchar(45) NOT NULL,
  `image4` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotelinfo`
--

LOCK TABLES `hotelinfo` WRITE;
/*!40000 ALTER TABLE `hotelinfo` DISABLE KEYS */;
INSERT INTO `hotelinfo` VALUES (1,'Hotel Radisson','Radison@gmail.com','01881486111','Airport,Dhaka','1100','1300',9,1,5,'1556096561955.jpg','1556005110799.jpg','1556005121359.jpg','1556005132883.jpg'),(2,'Taj Mahal Hotel','TajMahalHotel@gmail.com','01944147017','Mumbai','1200','1400',11,2,5,'1556005187266.jpg','1556005193425.jpg','1556005200683.jpg','1556044420249.jpg'),(3,'Royal Plaza Hotel','Royal@gmail.com','0192211110','New, Delli','1200','1500',10,2,5,'1556005232797.jpg','1556005243258.jpg','1556005251399.jpg','1556005265474.jpeg');
/*!40000 ALTER TABLE `hotelinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotelwisefacilityinfo`
--

DROP TABLE IF EXISTS `hotelwisefacilityinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotelwisefacilityinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotelFacilityId` int(11) NOT NULL,
  `hotelId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotelwisefacilityinfo`
--

LOCK TABLES `hotelwisefacilityinfo` WRITE;
/*!40000 ALTER TABLE `hotelwisefacilityinfo` DISABLE KEYS */;
INSERT INTO `hotelwisefacilityinfo` VALUES (1,1,1),(2,2,1),(3,3,2),(4,11,2);
/*!40000 ALTER TABLE `hotelwisefacilityinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomfacilityinfo`
--

DROP TABLE IF EXISTS `roomfacilityinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roomfacilityinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomFacility` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomfacilityinfo`
--

LOCK TABLES `roomfacilityinfo` WRITE;
/*!40000 ALTER TABLE `roomfacilityinfo` DISABLE KEYS */;
INSERT INTO `roomfacilityinfo` VALUES (1,'Free Wi-Fi'),(2,'Aircondition'),(4,'TV'),(5,'Telephone'),(6,'Coffee and tea facilities'),(7,'Iron with ironing board');
/*!40000 ALTER TABLE `roomfacilityinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roominfo`
--

DROP TABLE IF EXISTS `roominfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roominfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomNo` varchar(45) NOT NULL,
  `rent` double(18,3) NOT NULL,
  `roomTypeId` int(11) NOT NULL,
  `hotelId` int(11) NOT NULL,
  `image1` varchar(45) NOT NULL,
  `image2` varchar(45) NOT NULL,
  `image3` varchar(45) NOT NULL,
  `image4` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roominfo`
--

LOCK TABLES `roominfo` WRITE;
/*!40000 ALTER TABLE `roominfo` DISABLE KEYS */;
INSERT INTO `roominfo` VALUES (1,'101G',2500.000,2,1,'1556045831158.jpg','1556045841393.jpg','1556045852857.jpg','1556045862764.jpg'),(3,'105A',3000.500,3,1,'1556046417521.jpg','1556045994936.jpg','1556046005039.jpg','1556046014852.jpg'),(4,'106B',2000.000,2,2,'1556046066350.jpg','1556046074745.jpg','1556046087872.jpg','1556046097381.jpg');
/*!40000 ALTER TABLE `roominfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomtypeinfo`
--

DROP TABLE IF EXISTS `roomtypeinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roomtypeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomType` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomtypeinfo`
--

LOCK TABLES `roomtypeinfo` WRITE;
/*!40000 ALTER TABLE `roomtypeinfo` DISABLE KEYS */;
INSERT INTO `roomtypeinfo` VALUES (1,'Suite Room'),(2,'Family Room'),(3,'Duplex Room');
/*!40000 ALTER TABLE `roomtypeinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomwisefacilityinfo`
--

DROP TABLE IF EXISTS `roomwisefacilityinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roomwisefacilityinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomId` int(11) NOT NULL,
  `roomFacilityId` int(11) NOT NULL,
  `hotelId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomwisefacilityinfo`
--

LOCK TABLES `roomwisefacilityinfo` WRITE;
/*!40000 ALTER TABLE `roomwisefacilityinfo` DISABLE KEYS */;
INSERT INTO `roomwisefacilityinfo` VALUES (1,1,1,1),(2,1,2,2),(3,1,7,1);
/*!40000 ALTER TABLE `roomwisefacilityinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userinfo`
--

DROP TABLE IF EXISTS `userinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userinfo` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  `Email` varchar(45) NOT NULL,
  `PhoneNo` varchar(45) DEFAULT NULL,
  `Password` varchar(45) NOT NULL,
  `UserType` tinyint(1) NOT NULL DEFAULT '0',
  `IsActive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userinfo`
--

LOCK TABLES `userinfo` WRITE;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;
INSERT INTO `userinfo` VALUES (1,'admin','admin@gmail.com','13133333','123',1,1),(2,'superadmin','superadmin@gmail.com','1212121','12',1,1),(3,'sazzad','sazzad.hossen@gmail.com','01881486111','1',1,1),(4,'pritam','pritom@gmail.com','01911111112','123',1,1),(7,'bbb','bb','bbbbbbbbbbb','1',1,1);
/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'dbinfo'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-24 22:59:02
